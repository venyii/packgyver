<div id="wrapper">
    <div id="pg-special-panel" class="content-shadow pg-login">
        <div class="textcenter marginb10">
			<a href="/">
				<img src="/static/img/logo.png" alt="logo" />
			</a>
        </div>

		{if $data.error}
		<div class="alert alert-error">{$data.error}</div>
		{/if}

		{if auth('authEnabledFor', array('googleoauth'))}
			<a class="login-button google marginb10" href="{auth('getGoogleAuthLink')}"></a>
		{/if}

        <form id="pg-auth-form" action="/authenticate/service/basic" method="post">
            <div class="marginb10">
                <label for="panelEmail">E-Mail</label>
                <input type="email" placeholder="your.name@company.com" name="panelEmail" value="{if $data.credentials.email}{$data.credentials.email}{/if}" required="required"  />
            </div>
            <div>
                <label for="panelPassword">Passwort</label>
                <input type="password" placeholder="*********" name="panelPassword" value="" required="required" />
            </div>
            <input type="hidden" name="redirect" value="{$redirect}" />
            <button type="submit" id="submit"><i class="icon-arrow-right"></i>Anmelden</button>
        </form>
    </div>
</div>
<script>
    $(function() {
        $('html, body').addClass('pg-special');
        centerPanel();
    });

    $(window).resize(function() {
        centerPanel();
    });
</script>