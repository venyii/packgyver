<!DOCTYPE html>
<html>
    <head>
        <title>{if $pgPageTitle}{$pgPageTitle} | {/if}PackGyver Foosball</title>
        <meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow" />

		<link type="text/plain" rel="author" href="/static/humans.txt" />
        <link rel="SHORTCUT ICON" href="/static/img/icons/favicon.ico"/>
		<link rel="apple-touch-icon" href="/static/img/app/apple/touch-icon-iphone.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="/static/img/app/apple/touch-icon-ipad.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="/static/img/app/apple/touch-icon-iphone4.png" />

        {include '../inc/statics.tpl'}

    </head>
    <body>
        {$pgMainContent}
    </body>
</html>