<div class="content-first content-shadow grid4">
    {include 'inc/sidebar/mostMonsterPacksAlltime.tpl'}
</div>
<div class="content-shadow grid4">
    {include 'inc/sidebar/mostMonsterPacksOwnedAlltime.tpl'}
</div>
<div class="content-shadow grid4">
    {include 'inc/sidebar/longestWinStreakAlltimeRanking.tpl'}
</div>

<div class="content-first content-shadow grid4">
    {include 'inc/sidebar/mostPacksAlltime.tpl'}
</div>
<div class="content-shadow grid4">
    {include 'inc/sidebar/mostMatchesPlayerAlltimeRanking.tpl'}
</div>
<div class="content-shadow grid4">
    {include 'inc/sidebar/longestLossStreakAlltimeRanking.tpl'}
</div>

<div class="content-first content-shadow grid6">
    {include 'inc/sidebar/mostSeasonPointsPlus.tpl'}
</div>
<div class="content-shadow grid6">
    {include 'inc/sidebar/mostSeasonPointsMinus.tpl'}
</div>

<div class="clear"></div>
<div id="content" class="content-first content-shadow">
    <h4 class="headline">
        <span>Alltime Tabelle</span>
        <span class="headline-info fr">Die Aktualisierung erfolgt täglich um 00:00Uhr</span>
    </h4>
    <table id="ranking-alltime" class="ranking ranking-big">
        <thead>
            <tr>
                <th class="textcenter" style="width: 40px;">Platz</th>
                <th>Name</th>
                <th class="textcenter">Ratio</th>
                <th class="textcenter tipsy-tt" title="Anzahl gespielter Saisons">Saisons</th>
                <th class="textcenter">Spiele</th>
                <th class="textcenter">Gew.</th>
                <th class="textcenter">Verl.</th>
                <th class="textcenter">Tore</th>
                <th class="textcenter">Diff.</th>
				<th class="textcenter tipsy-tt" title="Bester Win Streak">+ Str</th>
				<th class="textcenter tipsy-tt" title="Längster Loss Streak">- Str</th>
                <th class="textcenter last-h">Punkte</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data.alltimeRanking rank player}
                {if $player.player.stats.alltime.countMatches > 0}
		    {if $player.player.stats.alltime.streaks.bestWinStreak > 0}
                        {$streakWin=' posTrend'}
                    {else}
                        {$streakWin=''}
                    {/if}

                    {if $player.player.stats.alltime.streaks.bestLossStreak > 0}
                        {$streakLoss=' negTrend'}
                    {else}
                        {$streakLoss=''}
                    {/if}

                    {if $dwoo.foreach.default.last}
                        {$isLastV=' last-v'}
                    {/if}

                    <tr class="{cycle values=array('','odd')}">
                        <td class="big textcenter ranking-rank{$isLastV}">{$rank + 1}.</td>
                        <td class="{$isLastV}">
                            <span class="pgStatus{if $player.player.checkedIn} checkedIn{else} checkedOut{/if}"></span>
                            <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                            <span class="ranking-player-name"><a href="/player/{$player.player.uid}">{$player.player.firstname} {$player.player.lastname}</a></span>
                        </td>
                        <td class="textcenter{$isLastV}">{$player.player.stats.alltime.matchWinRatio}%</td>
                        <td class="textcenter{$isLastV}">{$player.seasonCount}</td>
                        <td class="textcenter{$isLastV}">{$player.player.stats.alltime.countMatches}</td>
                        <td class="textcenter{$isLastV}">{$player.player.stats.alltime.countWins}</td>
                        <td class="textcenter{$isLastV}">{$player.player.stats.alltime.countLosses}</td>
                        <td class="textcenter{$isLastV}">{$player.player.stats.alltime.playerGoals}</td>
                        <td class="textcenter{$isLastV}">{$player.player.stats.alltime.playerGoals-$player.player.stats.alltime.enemyGoals}</td>
						<td class="textcenter{$isLastV}{$streakWin}">+{$player.player.stats.alltime.streaks.bestWinStreak}</td>
                        <td class="textcenter{$isLastV}{$streakLoss}">-{$player.player.stats.alltime.streaks.bestLossStreak}</td>
                        <td class="textcenter bold last-h{$isLastV}">{$player.player.stats.alltime.points}</td>
                    </tr>
                {/if}
            {/foreach}
        </tbody>
    </table>
</div>