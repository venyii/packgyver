<div id="sidebar" class="content-first grid4">
    <div class="content-shadow">
        {include 'inc/sidebar/awardRanking.tpl'}
    </div>
    <div class="content-shadow">
        {include 'inc/sidebar/awardsGiven.tpl'}
    </div>
</div>
<div id="content" class="grid8 content-shadow">
    <h4 class="headline"><span>Awards</span></h4>
    <p>

    {if $error}

    <h2>{$error}</h2>

    {else}

        <div class="posRel">
            {foreach $awards award name=awardList}
            {$awardId = $award.id}
            <div class="award-container fl tipsy-tt tipsy-s textcenter" title="{$award.description}">
                <div class="award">
                    <img src="{$award.logo}" alt="">
                </div>
                <div class="bold margint10 nowrap">{$award.name}</div>
                <button class="facebox-tr margint10" facebox-type="fb-player-list" href="#awPlayers_{$award.id}"><i class="icon-user"></i>{count $pAwards.$awardId} Träger</button>
            </div>
            {/foreach}
            <div class="clearfix"></div>
        </div>

        <div class="hide">
            {foreach $pAwards pAwardId pAward}
            <div id="awPlayers_{$pAwardId}">
                <h1>Spieler mit diesem Award</h1>
                <div class="fb-player-list-content fb-player-list-awards">
                        {if count($pAward) > 0}
                            {foreach $pAward playerAward}
                            <div class="fb-player-list-content-item{if $dwoo.foreach.default.last} last{/if}">
                                <img src="{$playerAward.player.avatar}" alt="" class="avatar medium fl" />
                                <div class="fl player-name" style="margin-top: 3px;">
                                    <a href="/player/{$playerAward.player.uid}">
                                        {$playerAward.player.firstname} {$playerAward.player.lastname} ({$playerAward.awards|count}x)
				    </a>
				    <div style="font-size: 12px;">
					<ul>
					    {foreach $playerAward.awards playerAwardCollection}
					    <li>
						{if $playerAwardCollection.comment}
						    {$playerAwardCollection.comment}
						{else}
						    <em>n/a</em>
						{/if}
					    </li>
					    {/foreach}
					</ul>
				    </div>
                                </div>
				<div class="clearfix"></div>
                            </div>
                            {/foreach}
                        {else}
                            <div class="margin10">Bisher wurde keinem Spieler dieser Award verliehen.</div>
                        {/if}
                </div>
                <div class="fb-controls shadow textright">
                    <button class="fb-close-trigger">Schließen</button>
                </div>
            </div>
            {/foreach}
        </div>

   {/if}

</div>