<div class="content-first content-shadow paddingb0">

    {if $error}

    <h4 class="headline">
        <span>Matchmaking</span>
    </h4>
    <div class="alert alert-error">
        <h4 class="alert-heading">Ein Fehler ist aufgetreten!</h4> {$error}
    </div>

    {else}

    <h4 class="headline">
        <span>Neues Spiel anlegen</span>
    </h4>

    {template teamPlayer}
    <div class="team-spot-player">
        <div class="tsp-addon">
            <div class="tsp-addon-points"><span class="val"></span> Pt.</div>
            <div class="tsp-addon-streak"></div>
            <div class="tsp-addon-position"></div>
        </div>
        <div class="tsp-main">
            <img class="avatar big" src="" alt="" />
            <div class="tsp-player-name">
                <span class="firstname"></span><br />
                <span class="lastname"></span>
            </div>
        </div>
    </div>
    {/template}

    <div id="mmContainer">
        <div id="matchmaking">
            <div id="pregame">
                <div class="special-headline marginb15"><span>Spieler auswählen</span></div>
                <div id="alternative-pp">
                    <div id="mm-chosen-player-msg" class="marginb15 textcenter">
                        <a href="" class="mm-player-change-stati" status="in">Alle auswählen</a> | 
                        <span class="bold msg"><span class="val">{$players|count}</span> Spieler ausgewählt</span> | 
                        <a href="" class="mm-player-change-stati" status="out">Alle abwählen</a>
                    </div>
                    <div id="mm-choose-player">
                        <ul>
                            {foreach $players player}
                            <li class="fl">
                                <div class="mm-player" playerId="{$player.id}">
                                    <img src="{$player.avatar}" alt="" class="avatar big" />
                                    <div class="mm-player-credentials">
                                        <span class="bold">{$player.firstname} {$player.lastname}</span><br />
                                        <span class="mm-player-stats">R {$player.stats.season.matchWinRatio}% / S {$player.stats.season.countWins} / N {$player.stats.season.countLosses}</span>
                                    </div>
                                    <div class="player-mm-status">
                                        <button>&check;</button>
                                    </div>
                                </div>
                            </li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
                <div class="special-headline marginb15"><span>Optionen</span></div>
                <div class="textcenter">
                    <input type="radio" id="match-type-1on1" name="matchType" value="2"{if !$is1on1Active} disabled="disabled"{/if}{if !$is2on2Active || $players|count < 4} checked="checked"{/if} /> <label for="match-type-1on1">1 gegen 1</label>
                    <input type="radio" id="match-type-2on2" name="matchType" value="4"{if !$is2on2Active || $players|count < 4} disabled="disabled"{else} checked="checked"{/if} /> <label for="match-type-2on2">2 gegen 2</label><br /><br />
                    <input type="checkbox" id="mm-player-participate" /> <label for="mm-player-participate">Ich möchte mitspielen</label>
                    {*<input type="checkbox" id="mm-fair-shuffle" /> <label for="mm-fair-shuffle">Gerechte Team-Verteilung aktivieren</label>*}
                </div>
                <div class="margint25 textcenter">
                    <button id="mm-generate-match"><i class="icon-ok"></i><span>Spiel generieren</span></button>
                    <button class="mm-cancel"><i class="icon-remove"></i>Abbrechen</button>
                </div>
            </div>

            <div id="mm-setup-rdy" class="hide">
                <div id="table-area">

                    <!-- TEAM RED SPOTS -->
                    <div class="team-container team-red">
                        <div id="team-red-goal" class="team-spot goal" position2on2="TOR" position1on1="ALLE">
							<div class="panel">
								<div class="front">
									<img src="/static/img/matchmaking/position_goal.png" width="140" />
								</div>
								<div class="back">
									{teamPlayer}
								</div>
							</div>
                        </div>
                        <div id="team-red-offense" class="team-spot offense" position2on2="STURM">
                            <div class="panel">
								<div class="front">
									<img src="/static/img/matchmaking/position_offense.png" width="110" />
								</div>
								<div class="back">
									{teamPlayer}
								</div>
							</div>
                        </div>
                    </div>
                    <!-- /TEAM RED SPOTS -->

                    <!-- TABLE -->
                    <div id="matchmaking-table">
                        <div id="table-content" class="textcenter">
                            <!-- CONTROLS -->
                            <div id="mm-controls">
                            </div>
                            <!-- /CONTROLS -->
                            <form method="post" action="" autocomplete="off">
                                <input type="hidden" id="matchId" name="matchId" value="" />
                            </form>
                        </div>
                    </div>
                    <!-- /TABLE -->

                    <!-- SCORE-CENTER -->
                    <div id="score-center">
                        <div id="scoreboard" class="margin15">
                            <form method="post" action="" autocomplete="off">
                                <div id="range-red" team="red" class="score-range"></div>
                                <div class="score-red fl scorecard zero"></div>
                                <div class="fl" style="width: 16px; margin: 40px 15px;">
                                    <div style="width: 16px; line-height: 0px;">
                                        <span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span>
                                    </div>
                                    <div style="width: 16px; line-height: 0px; margin-top: 40px;">
                                        <span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span>
                                    </div>
                                </div>
                                <div class="score-yellow fl scorecard zero"></div>
                                <div id="range-yellow" class="score-range" team="yellow" style=""></div>
                                <div style="clear: both;"></div>
                                <input type="hidden" name="teamScoreYellow" class="teamScoreYellow" value="0" />
                                <input type="hidden" name="teamScoreRed" class="teamScoreRed" value="0" />
                                <input type="hidden" name="matchId" class="matchId" value="" />
                            </form>
                        </div>
                        <div id="current-match-ops" class="textcenter">
                            <div class="cm-ops-control-back">
                                <button class="save-cm save-generated"><i class="icon-ok"></i>Speichern</button>
                                <button class="cancel-cm"><i class="icon-remove"></i>Abbrechen</button>
                            </div>
                            <div class="cm-ops-control-front">
                                <button class="enter-score-cm" matchId="{$cMatch.id}"><i class="icon-pencil"></i>Ergebnis eintragen</button>
                            </div>
                        </div>
                    </div>
                    <!-- /SCORE-CENTER -->

                    <!-- TEAM YELLOW SPOTS -->
                    <div class="team-container team-yellow">
                        <div id="team-yellow-goal" class="team-spot goal" position2on2="TOR" position1on1="ALLE">
                            <div class="panel">
								<div class="front">
									<img src="/static/img/matchmaking/position_goal.png" width="140" />
								</div>
								<div class="back">
									{teamPlayer}
								</div>
							</div>
                        </div>
                        <div id="team-yellow-offense" class="team-spot offense" position2on2="STURM">
                            <div class="panel">
								<div class="front">
									<img src="/static/img/matchmaking/position_offense.png" width="110" />
								</div>
								<div class="back">
									{teamPlayer}
								</div>
							</div>
                        </div>
                    </div>
                    <!-- /TEAM YELLOW SPOTS -->
                </div>
            </div>
        </div>
    </div>
    <div class="clear marginb25"></div>
    <script>
        {foreach $players player}

        var player_{$player.id} = {
            id: {$player.id},
            firstname: '{$player.firstname}',
            lastname: '{$player.lastname}',
            avatar: '{$player.avatar}',
            stats: {
                points: {$player.stats.season.points},
                ratio: {$player.stats.season.matchWinRatio},
                matches: {$player.stats.season.countMatches},
                streak: {$player.stats.season.streaks.currentStreak}
            }
        };

        {if $player.id == $pgUser.id}
        $.matchMaker.player = player_{$player.id};
        {/if}
        $.matchMaker.appendPoolPlayer({$player.id}, player_{$player.id});

        {/foreach}
    </script>
    {/if}
</div>