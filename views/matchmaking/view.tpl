{if $pgIsDevMode}

<script type="text/javascript" src="/static/js/plugins/jquery.lionbars.js"></script>
<script type="text/javascript" src="/static/js/matchmaker.js"></script>
<script type="text/javascript" src="/static/js/matchmaking.js"></script>

{else}

<script type="text/javascript" src="/static/js/_min/packgyver.mm.008.min.js"></script>

{/if}

<div id="matchmaking-site">
    {if $function == 'showUnsaved'}
		{include 'matchOverview.tpl'}
    {elseif $function == 'createMatch'}
		{include 'createMatch.tpl'}
    {/if}
</div>
