<h4 class="headline"><span>Meiste Spiele Season</span></h4>
{$data = widget('getSeasonMostMatches')}

{if $data.player && $data.matchCount > 0}
<div class="fr textright">
    <div class="fl" style="padding: 0px 7px 0 0;">
        <div style="color: #ccc; font-size: 55px;">{$data.matchCount}</div>
        <a href="/player/{$data.player.uid}">
            {$data.player.shortName}
        </a>
    </div>
    <a href="/player/{$data.player.uid}">
        <img src="{$data.player.avatar}" class="avatar medium fl" alt="avatar" />
    </a>
</div>
<div class="clear"></div>
{else}
<span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}