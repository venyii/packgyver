{$isLastV=''}
<h4 class="headline"><span class="tipsy-tt" title="Loss Streaks der aktuellen Saison">Loss Streaks (S)</span></h4>
{$seasonLongestLossStreak = widget('getSeasonLongestLossStreak', array(3))}
{if $seasonLongestLossStreak|count > 0}
    <table class="ranking sidebar-player-ranking">
        <thead>
            <tr>
                <th class="textcenter bold">#</th>
                <th class="bold">Name</th>
                <th class="bold last-h tipsy-tt" title="Loss Streak">LS</th>
            </tr>
        </thead>
        <tbody>
            {foreach $seasonLongestLossStreak player}
                {if $dwoo.foreach.default.last}
                    {$isLastV=' last-v'}
                {/if}
                <tr class="{cycle values=array('','odd')}">
                    <td class="big textcenter ranking-rank{$isLastV}">{$dwoo.foreach.default.iteration}.</td>
                    <td class="{$isLastV}">
                        <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                        <span class="ranking-player-name"><a href="/player/{$player.player.uid}">{$player.player.shortName}</a></span>
                    </td>
                    <td class="textcenter last-h{$isLastV}"><span class="negTrend">-{$player.bestLossStreak}</span></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{else}
    <span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}