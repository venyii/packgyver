<script>
    var WS_SERVICE_ENABLED = {if $pgWSService.enabled}true{else}false{/if};
</script>

{if $pgIsDevMode}

    <!-- STYLES -->
    <link type="text/css" rel="stylesheet" href="/static/css/plugins/jquery-ui/smoothness/jquery-ui-1.11.1.custom.css" />
    <link type="text/css" rel="stylesheet" href="/static/css/plugins/jquery.dataTables.css" />
    <link type="text/css" rel="stylesheet" href="/static/css/plugins/font-awesome.css" />
    <link type="text/css" rel="stylesheet" href="/static/css/plugins/tipsy.css" />
    <link type="text/css" rel="stylesheet" href="/static/css/plugins/facebox.css" />
	<link type="text/css" rel="stylesheet" href="/static/css/plugins/select2.css" />
	<link type="text/css" rel="stylesheet" href="/static/css/plugins/picker/default.css" />
	<link type="text/css" rel="stylesheet" href="/static/css/plugins/picker/default.date.css" />
	{*<link type="text/css" rel="stylesheet" href="/static/css/plugins/picker/default.time.css" />*}
    <link type="text/css" rel="stylesheet" href="/static/css/styles.css" />
    <link type="text/css" rel="stylesheet" href="/static/css/matchmaking.css" />
    <link type="text/css" rel="stylesheet" href="/static/css/sidebar.css" />
    <!-- /STYLES -->

    <!-- SCRIPT -->
    <script src="/static/js/plugins/jquery-2.1.1.js"></script>
    <script src="/static/js/plugins/jquery-ui-1.11.1.custom.js"></script>
    <script src="/static/js/plugins/highcharts.src.js"></script>
    <script src="/static/js/plugins/jquery.base64.min.js"></script>
    <script src="/static/js/plugins/jquery.dataTables.js"></script>
    <script src="/static/js/plugins/jquery.tipsy.js"></script>
    <script src="/static/js/plugins/jquery.hotkeys.js"></script>
    <script src="/static/js/plugins/picker/picker.js"></script>
    <script src="/static/js/plugins/picker/picker.date.js"></script>
    {*<script src="/static/js/plugins/picker/picker.time.js"></script>*}
    <script src="/static/js/plugins/facebox.js"></script>
    <script src="/static/js/plugins/timeago/timeago.js"></script>
    <script src="/static/js/plugins/timeago/locales/timeago.de.js"></script>
    <script src="/static/js/plugins/select2.js"></script>
    <script src="/static/js/packgyver.js"></script>
    <script src="/static/js/script.js"></script>
    <!-- /SCRIPT -->

{else}

    <link type="text/css" rel="stylesheet" href="/static/css/_min/packgyver.all.015.min.css" />
    <script src="/static/js/_min/packgyver.all.015.min.js"></script>

{/if}

{if player('isAdmin')}

	<script src="/static/js/plugins/jquery.jeditable.js"></script>
	<script src="/static/js/admin.js"></script>

{/if}
