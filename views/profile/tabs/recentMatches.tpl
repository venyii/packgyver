{$matches = player('getRecentMatchesForPlayerId', $playerHash.id)}

<div id="content" class="content-first content-shadow grid8">
    <h4 class="headline">
        <span>Letzte Spiele</span>
    </h4>

    {if count($matches) > 0}
        {foreach $matches match name="matches"}

            {$packType = match('getPackType', $match.id)}
            {$lwin = false}
            {$rwin = false}

            {if $match.score.left > $match.score.right}
                {$lwin = true}
            {else}
                {$rwin = true}
            {/if}

            <div class="match-entry{if $dwoo.foreach.matches.last} last{/if}">
                <div style="float: left; width: 262px; padding-top: 10px;">
                    {if $match.type == '2on2'}
                        <div>
                            <span class="position teamRed">T</span>
                            <a href="/player/{$match.players.left.goal.uid}">
                                {$match.players.left.goal.firstname} {$match.players.left.goal.lastname}
                            </a>
                        </div>
                        <div style="margin-top: 13px;">
                            <span class="position teamRed">S</span>
                            <a href="/player/{$match.players.left.offense.uid}">
                                {$match.players.left.offense.firstname} {$match.players.left.offense.lastname}
                            </a>
                        </div>
                    {else}
                        <div class="margint10">
                            <span class="position teamRed">A</span>
                            <a href="/player/{$match.players.left.all.uid}">
                                {$match.players.left.all.firstname} {$match.players.left.all.lastname}
                            </a>
                        </div>
                    {/if}
                </div>

                <div style="float: left; text-align: center; width: 120px;">

                    <span style="font-size: 10px;">{$match.dateSaved}</span>

                    <div style="font-size: 44px; color: #aaa; border-radius: 8px; display: inline-block;">
                        {if $lwin}<span class="winner-team">{$match.score.left}</span>{else}{$match.score.left}{/if}&nbsp;{if $rwin}<span class="winner-team">{$match.score.right}</span>{else}{$match.score.right}{/if}
                    </div>

                    {if $packType == 'mp'}
                        <span class="noteNumb disInlBlock small new">MONSTER PACK</span>
                    {elseif $packType == 'p'}
                        <span class="noteNumb disInlBlock small">P A C K</span>
                    {/if}

                </div>

                <div style="float: left; text-align: right; width: 262px; padding-top: 10px;">
                    {if $match.type == '2on2'}
                        <div>
                            <a href="/player/{$match.players.right.offense.uid}">
                                {$match.players.right.offense.firstname} {$match.players.right.offense.lastname}
                            </a>
                            <span class="position teamYellow">S</span>
                        </div>
                        <div style="margin-top: 13px;">
                            <a href="/player/{$match.players.right.goal.uid}">
                                {$match.players.right.goal.firstname} {$match.players.right.goal.lastname}
                            </a>
                            <span class="position teamYellow">T</span>
                        </div>
                    {else}
                        <div style="margin-top: 13px;">
                            <a href="/player/{$match.players.right.all.uid}">
                                {$match.players.right.all.firstname} {$match.players.right.all.lastname}
                            </a>
                            <span class="position teamYellow">T</span>
                        </div>
                    {/if}
                </div>

                <div class="clear"></div>
            </div>
        {/foreach}
    {else}
        {$playerHash.firstname} hat in dieser Saison noch keine Spiele bestritten!
    {/if}
</div>
<div id="sidebar" class="grid4">
    <div class="content-shadow">
        {include '../../inc/sidebar/mostMatchesPlayerToday.tpl'}
    </div>
    <div class="content-shadow">
        {include '../../inc/sidebar/mostMatchesPlayerSeason.tpl'}
    </div>
    <div class="content-shadow">
        {include '../../inc/sidebar/mostMatchesPlayerAlltimeRanking.tpl'}
    </div>
</div>