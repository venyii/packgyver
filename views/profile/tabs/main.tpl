{$curStreak = $playerHash.stats.season.streaks.currentStreak}

{*
<div class="content-first content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>Derzeitiger Platz</span></h4>
    <div style="font-size: 72px;" class="textcenter">{$seasonRank}</div>
</div>
*}
<div class="content-first content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>Spiele / Tag</span></h4>
    <div style="font-size: 72px;" class="textcenter">{$gamesPerDay}</div>
</div>
<div class="content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>&Oslash; Punkte / Spiel</span></h4>
    <div style="font-size: 72px;" class="textcenter">{$playerHash.stats.season.averagePoints}</div>
</div>
<div class="content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>Packs / MonsterP.</span></h4>
    <div style="font-size: 72px;" class="textcenter nowrap">
        {$playerHash.stats.season.packCount} / {$playerHash.stats.season.monsterPackCount}
    </div>
</div>
<div class="content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>Aktueller Streak</span></h4>

    {if player('isStreakPositive', $curStreak)}
        {$streakClass = ' posTrend'}
    {elseif 0 == $curStreak}
        {$streakClass = ''}
    {else}
        {$streakClass = ' negTrend'}
    {/if}
    <div style="font-size: 72px;" class="textcenter{$streakClass}">{$playerHash.stats.season.streaks.currentStreak}</div>
</div>

<div class="grid6 content-first content-shadow paddingb0">
    <h4 class="headline"><span>15 Spiele Trend (Saison)</span></h4>
    <div id="pointTrend"></div>
</div>

<div class="grid6 content-shadow">
    <h4 class="headline"><span>{player('getFirstnamePersonal', $playerHash.id)} Ranking</span></h4>

	{if $tableSequence}
    <table class="ranking sidebar-player-ranking">
        <thead>
            <tr>
                <th class="textcenter bold">Pl.</th>
                <th class="bold">Name</th>
                <th class="bold textcenter">Streak</th>
                <th class="bold textcenter">Spiele</th>
                <th class="textcenter bold last-h">Pkt.</th>
            </tr>
        </thead>
        <tbody>
            {foreach $tableSequence tsPlayer name="tableSequence"}

            {if $dwoo.foreach.tableSequence.last}
                {$isLastV=' last-v'}
            {/if}

            {if $tsPlayer.tsPointsState == '+'}
                {$tsDiff = ' posTrend'}
                {$tsArrow = '&UpArrow;'}
                {$pointsTip = ' Punkte im Vorsprung'}
            {elseif $tsPlayer.tsPointsState == '-'}
                {$tsDiff = ' negTrend'}
                {$tsArrow = '&DownArrow;'}
                {$pointsTip = ' Punkte im Rückstand'}
            {else}
                {$tsDiff = ''}
                {$tsArrow = ''}
                {$pointsTip = ''}
            {/if}

            {if player('isStreakPositive', $tsPlayer.stats.season.streaks.currentStreak)}
                {$streak=' posTrend'}
            {elseif 0 != $tsPlayer.stats.season.streaks.currentStreak}
                {$streak=' negTrend'}
            {else}
                {$streak=''}
            {/if}

            <tr class="{cycle values=array('','odd')}">
                <td class="textcenter ranking-rank{$isLastV}">{$tsPlayer.stats.season.rank}.</td>
                <td class="{if $tsPlayer.id == $playerHash.id}bold {/if}{$isLastV}">
                    <img src="{$tsPlayer.avatar}" alt="" class="avatar small fl" />
                    <span class="ranking-player-name"><a href="/player/{$tsPlayer.uid}">{$tsPlayer.firstname} {$tsPlayer.lastname}</a></span>
                </td>
                <td class="textcenter ranking-rank{$isLastV}{$streak}">{$tsPlayer.stats.season.streaks.currentStreak}</td>
                <td class="{if $tsPlayer.id == $playerHash.id}bold {/if}textcenter ranking-rank{$isLastV}">{$tsPlayer.stats.season.countMatches}</td>
                <td class="{if $tsPlayer.id == $playerHash.id}bold {/if}textcenter {if $pointsTip} tipsy-tt{/if} last-h bold{$isLastV}{$tsDiff}" title="{if '' !== $pointsTip}{$tsPlayer.tsPoints} {$pointsTip}{/if}">{$tsPlayer.tsPointsState}{$tsPlayer.tsPoints} {$tsArrow}</td>
            </tr>

            {/foreach}
        </tbody>
    </table>
	{else}
	Es steht kein Ranking zur Verfügung.
	{/if}
</div>

<div class="grid4 content-shadow content-first">
    <h4 class="headline"><span>Siege / Niederlagen (Saison)</span></h4>
    <div id="matches-stats"></div>
</div>

<div class="grid8 content-shadow" style="width: 602px; height: 188px;">
    <h4 class="headline"><span>Gegnerstatistik</span></h4>

    {$fearedPlayer = player('getHashForPlayerId', $playerHash.stats.season.fearedPlayer.playerId)}
    {$buddyPlayer = player('getHashForPlayerId', $playerHash.stats.season.buddyPlayer.playerId)}
    {$fearedTeamPlayer = player('getHashForPlayerId', $playerHash.stats.season.fearedTeamPlayer.playerId)}
    {$victimPlayer = player('getHashForPlayerId', $playerHash.stats.season.victimPlayer.playerId)}

    <div class="textcenter fl enemy-stats">
        {if $fearedPlayer}
            <a class="bold tipsy-tt" title="{$fearedPlayer.firstname} {$fearedPlayer.lastname}" href="/player/{$fearedPlayer.uid}">
                <img src="{$fearedPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angstgegner</div>
            {if $playerHash.stats.season.fearedPlayer.matchCount}
                {$playerHash.stats.season.fearedPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $victimPlayer}
            <a class="bold tipsy-tt" title="{$victimPlayer.firstname} {$victimPlayer.lastname}" href="/player/{$victimPlayer.uid}">
                <img src="{$victimPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Lieblingsgegner</div>
            {if $playerHash.stats.season.victimPlayer.matchCount}
                {$playerHash.stats.season.victimPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $buddyPlayer}
            <a class="bold tipsy-tt tipsy-n" title="{$buddyPlayer.firstname} {$buddyPlayer.lastname}" href="/player/{$buddyPlayer.uid}">
                <img src="{$buddyPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Kicker-Kumpel</div>
            {if $playerHash.stats.season.buddyPlayer.matchCount}
                {$playerHash.stats.season.buddyPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
    {if $fearedTeamPlayer}
        <a class="bold tipsy-tt" title="{$fearedTeamPlayer.firstname} {$fearedTeamPlayer.lastname}" href="/player/{$fearedTeamPlayer.uid}">
            <img src="{$fearedTeamPlayer.avatar}" alt="" class="avatar big" />
        </a>
        {else}
        <div class="avatar big unknown">?</div>
    {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angst-Mitspieler</div>
        {if $playerHash.stats.season.fearedTeamPlayer.matchCount}
            {$playerHash.stats.season.fearedTeamPlayer.matchCount} Spiele
            {else}
            ? Spiele
        {/if}
        </div>
    </div>
</div>

<script type="text/javascript">
    var pointTrendSeasonMain = {$playerHash.stats.season.pointTrend};
    
    $(function() {
        new Highcharts.Chart({
            chart: {
                renderTo: 'pointTrend',
                defaultSeriesType: 'line',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                gridLineColor: '#ccc',
                lineColor: '#ccc',
                labels: {
                    enabled: false
                },
                title: {
                    text: 'Punktetrend der letzten 15 Spiele',
                    style: {
                        'color': '#3C3E45',
                        'font-weight': 'normal'
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                },
                gridLineColor: '#ccc',
                lineColor: '#3C3E45'
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.y + ' Punkte</strong>';
                },
                borderColor: '#ccc'
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    name: 'Punkteverlauf',
                    data: pointTrendSeasonMain,
                    color: '#3C3E45'
                }]
        });

        new Highcharts.Chart({
            chart: {
                renderTo: 'matches-stats',
                defaultSeriesType: 'bar',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['Siege', 'Niederlagen'],
                        gridLineColor: '#ccc',
                        lineColor: '#ccc',
                        labels: {
                            enabled: false
                        },
                        title: {
                            enabled: false  
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null
                        },
                        gridLineColor: '#ccc',
                        lineColor: '#3C3E45'
                    },


            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' Spiele';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    enabled: true
                    }
                }
            },
            legend: {
                enabled: false 
            },
            credits: {
                enabled: false
            },
            series: [{
                name: null,
                data: [{$playerHash.stats.season.countWins}, {$playerHash.stats.season.countLosses}],
                color: '#3C3E45'
            }]
        });
    });
</script>