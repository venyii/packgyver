<div class="grid4 content-first content-shadow" style="height: 184px;">
    <h4 class="headline"><span>Streaks</span></h4>
    <dl class="profileStats">
        <dt>Längster Win-Streak</dt>
        <dd>{$playerHash.stats.alltime.streaks.bestWinStreak}</dd>
        <dt class="last">Längster Loss-Streak</dt>
        <dd class="last">{$playerHash.stats.alltime.streaks.bestLossStreak}</dd>
    </dl>
</div>

<div class="grid8 content-shadow" style="width: 602px;">
    <h4 class="headline"><span>Gegnerstatistik</span></h4>

    {$fearedPlayer = player('getHashForPlayerId', $playerHash.stats.alltime.fearedPlayer.playerId)}
    {$buddyPlayer = player('getHashForPlayerId', $playerHash.stats.alltime.buddyPlayer.playerId)}
    {$fearedTeamPlayer = player('getHashForPlayerId', $playerHash.stats.alltime.fearedTeamPlayer.playerId)}
    {$victimPlayer = player('getHashForPlayerId', $playerHash.stats.alltime.victimPlayer.playerId)}

    <div class="textcenter fl enemy-stats">
        {if $fearedPlayer}
            <a class="bold tipsy-tt" title="{$fearedPlayer.firstname} {$fearedPlayer.lastname}" href="/player/{$fearedPlayer.uid}">
                <img src="{$fearedPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angstgegner</div>
            {if $playerHash.stats.alltime.fearedPlayer.matchCount}
                {$playerHash.stats.alltime.fearedPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $victimPlayer}
            <a class="bold tipsy-tt" title="{$victimPlayer.firstname} {$victimPlayer.lastname}" href="/player/{$victimPlayer.uid}">
                <img src="{$victimPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Lieblingsgegner</div>
            {if $playerHash.stats.alltime.victimPlayer.matchCount}
                {$playerHash.stats.alltime.victimPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $buddyPlayer}
            <a class="bold tipsy-tt tipsy-n" title="{$buddyPlayer.firstname} {$buddyPlayer.lastname}" href="/player/{$buddyPlayer.uid}">
                <img src="{$buddyPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Kicker-Kumpel</div>
            {if $playerHash.stats.alltime.buddyPlayer.matchCount}
                {$playerHash.stats.alltime.buddyPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
    {if $fearedTeamPlayer}
        <a class="bold tipsy-tt" title="{$fearedTeamPlayer.firstname} {$fearedTeamPlayer.lastname}" href="/player/{$fearedTeamPlayer.uid}">
            <img src="{$fearedTeamPlayer.avatar}" alt="" class="avatar big" />
        </a>
        {else}
        <div class="avatar big unknown">?</div>
    {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angst-Mitspieler</div>
        {if $playerHash.stats.alltime.fearedTeamPlayer.matchCount}
            {$playerHash.stats.alltime.fearedTeamPlayer.matchCount} Spiele
            {else}
            ? Spiele
        {/if}
        </div>
    </div>
</div>

<div class="clear"></div>

<div class="content-first content-shadow grid4">
    <h4 class="headline"><span>Spiele / Tore</span></h4>
    <dl class="profileStats">
        <dt>Spiele Gesamt</dt>
        <dd>{$playerHash.stats.alltime.countMatches}</dd>
        <dt>Gewonnene Spiele</dt>
        <dd>{$playerHash.stats.alltime.countWins}</dd>
        <dt>Verlorene Spiele</dt>
        <dd>{$playerHash.stats.alltime.countLosses}</dd>
        <dt class="last">Ratio</dt>
        <dd class="last">{$playerHash.stats.alltime.matchWinRatio}%</dd>
        <dt class="last">&nbsp;</dt>
        <dd class="last">&nbsp;</dd>
        <dt>Erzielte Tore (Team)</dt>
        <dd>{$playerHash.stats.alltime.playerGoals}</dd>
        <dt class="last">Gegentore (Team)</dt>
        <dd class="last">{$playerHash.stats.alltime.enemyGoals}</dd>

        {*<dt>Tordifferenz</dt>
        <dd>{$playerHash.stats.alltime.playerGoals - $playerHash.stats.alltime.enemyGoals}</dd>*}
    </dl>
</div>
<div class="content-shadow grid4" style="height: 168px;">
    <h4 class="headline"><span>Monster Packs / Packs</span></h4>
    <dl class="profileStats">
        <dt>Pack verteilt</dt>
        <dd>{$playerHash.stats.alltime.packCount} ({$playerHash.stats.alltime.packQuote}%)</dd>
        <dt class="last">Pack kassiert</dt>
        <dd class="last">{$playerHash.stats.alltime.ownedPackCount} ({$playerHash.stats.alltime.ownedPackQuote}%)</dd>
        <dt class="last">&nbsp;</dt>
        <dd class="last">&nbsp;</dd>
        <dt>Monster Pack verteilt</dt>
        <dd>{$playerHash.stats.alltime.monsterPackCount} ({$playerHash.stats.alltime.monsterPackQuote}%)</dd>
        <dt class="last">Monster Pack kassiert</dt>
        <dd class="last">{$playerHash.stats.alltime.ownedMonsterPackCount} ({$playerHash.stats.alltime.ownedMonsterPackQuote}%)</dd>
    </dl>
</div>
<div class="content-shadow grid4">
    <h4 class="headline"><span>Positionen</span></h4>
    <dl class="profileStats">
        <dt>Im Tor gespielt</dt>
        <dd>{$playerHash.stats.alltime.positions.goal}</dd>
        <dt>- davon gewonnen</dt>
        <dd>{$playerHash.stats.alltime.matchesWonInGoal} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.alltime.matchesWonInGoal, $playerHash.stats.alltime.positions.goal))}%)</span></dd>
        <dt class="last">- davon verloren</dt>
        <dd class="last">{$playerHash.stats.alltime.matchesLostInGoal} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.alltime.matchesLostInGoal, $playerHash.stats.alltime.positions.goal))}%)</span></dd>
        <dt class="last">&nbsp;</dt>
        <dd class="last">&nbsp;</dd>
        <dt>Im Sturm gespielt</dt>
        <dd>{$playerHash.stats.alltime.positions.offense}</dd>
        <dt>- davon gewonnen</dt>
        <dd>{$playerHash.stats.alltime.matchesWonInOffense} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.alltime.matchesWonInOffense, $playerHash.stats.alltime.positions.offense))}%)</span></dd>
        <dt class="last">- davon verloren</dt>
        <dd class="last">{$playerHash.stats.alltime.matchesLostInOffense} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.alltime.matchesLostInOffense, $playerHash.stats.alltime.positions.offense))}%)</span></dd>
        {*<dt>&nbsp;</dt>
        <dd>&nbsp;</dd>
        <dt class="last">Alleine gespielt</dt>
        <dd class="last">{$playerHash.stats.alltime.positions.all}</dd>*}
    </dl>
</div>

<div class="grid6 content-first content-shadow">
    <h4 class="headline"><span>Monster Packs / Packs / Spiele</span></h4>
    <div id="matches-packs"></div>
</div>

<div class="grid6 content-shadow">
    <h4 class="headline"><span>Siege / Niederlagen</span></h4>
    <div id="matches-stats"></div>
</div>

<div class="clear"></div>

<script type="text/javascript">
    $(function() {
        new Highcharts.Chart({
            chart: {
                renderTo: 'matches-stats',
                defaultSeriesType: 'bar',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['Siege', 'Niederlagen'],
                        gridLineColor: '#ccc',
                        lineColor: '#ccc',
                        labels: {
                            enabled: false
                        },
                        title: {
                            enabled: false  
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null
                        },
                        gridLineColor: '#ccc',
                        lineColor: '#3C3E45'
                    },


            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' Spiele';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    enabled: true
                    }
                }
            },
            legend: {
                enabled: false 
            },
            credits: {
                enabled: false
            },
            series: [{
                name: null,
                data: [{$playerHash.stats.alltime.countWins}, {$playerHash.stats.alltime.countLosses}],
                color: '#3C3E45'
            }]
        });

        var matchSum = {math("$playerHash.stats.alltime.countMatches-$playerHash.stats.alltime.packCount-$playerHash.stats.alltime.monsterPackCount")};
        new Highcharts.Chart({
            chart: {
                renderTo: 'matches-packs',
                defaultSeriesType: 'bar',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['MonsterPacks', 'Packs', 'Matches'],
                        gridLineColor: '#ccc',
                        lineColor: '#ccc',
                        labels: {
                            enabled: false
                        },
                        title: {
                            enabled: false  
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null
                        },
                        gridLineColor: '#ccc',
                        lineColor: '#3C3E45'
                    },


            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' Spiele';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    enabled: true
                    }
                }
            },
            legend: {
                enabled: false 
            },
            credits: {
                enabled: false
            },
            series: [{
                name: null,
                data: [{$playerHash.stats.alltime.monsterPackCount}, {$playerHash.stats.alltime.packCount}, matchSum],
                color: '#3C3E45'
            }]
        });
    });
</script>