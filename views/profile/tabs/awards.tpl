<div id="content" class="player-profile-awards content-first content-shadow">
    <h4 class="headline">
        <span>Awards</span>
    </h4>

    {if $playerAwards|count > 0}
        {foreach $playerAwards playerAward name=awardList}
            {if $playerAwards}
                <div class="award-container fl tipsy-tt tipsy-s textcenter{if $dwoo.foreach.awardList.first} first{/if}" title="{$playerAward.playerAward.comment}">
                    <div class="award">
                        <img src="{$playerAward.logo}" alt="">
                    </div>
                    <div title="{$award.description}" class="bold margint10 nowrap">{$playerAward.name}</div>
                </div>
            {else}
                {$playerHash.firstname} hat noch keine Awards!
            {/if}
        {/foreach}
    {else}
        {$playerHash.firstname} hat noch keinen Award erhalten!
    {/if}

    <div class="clear"></div>
</div>