<!DOCTYPE html>
<html>
    <head>
        <title>{if $pgPageTitle}{$pgPageTitle} | {/if}PackGyver Foosball</title>
        <meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow" />

		<link type="text/plain" rel="author" href="/static/humans.txt" />
        <link rel="SHORTCUT ICON" href="/static/img/icons/favicon.ico"/>
		<link rel="apple-touch-icon" href="/static/img/app/apple/touch-icon-iphone.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="/static/img/app/apple/touch-icon-ipad.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="/static/img/app/apple/touch-icon-iphone4.png" />

        {include 'inc/statics.tpl'}

    </head>
    <body>
        {if $pgError}

            {$pgMainContent}

        {else}
        <nav id="top-bar">
            <div class="wrapper">
                <img id="page-loader" src="/static/img/loader.gif" style="display: none;" />
                <ul id="top-bar-menu" class="top-bar">
                    <li class="first player-ucp submenu">
                        <span>
                            <img class="avatar small player-avatar-chg" src="{$pgUser.avatar}" alt="" />
                            <span>{$pgUser.firstname} {$pgUser.lastname}</span>
                        </span>
                        <div id="player-dropdown-cp-overview" class="player-dropdown-cp player-dropdown-cp-ident">
                            <div class="fl">
                                <div class="player-ucp-menu-item">
                                    <img src="/static/img/icons/edit.png" class="fl" /> <span class="fl"><a href="/player/{$pgUser.uid}">Mein Profil</a></span>
                                    <div class="clear"></div>
                                </div>
                                <div class="player-ucp-menu-item">
                                    <img src="/static/img/icons/edit.png" class="fl" /> <span class="fl"><a href="/player/{$pgUser.uid}/stats">Meine Statistik</a></span>
                                    <div class="clear"></div>
                                </div>
                                <div class="player-ucp-menu-item">
                                    <img src="/static/img/icons/edit.png" class="fl" /> <span class="fl"><a href="/player/{$pgUser.uid}/setup">Einstellungen</a></span>
                                    <div class="clear"></div>
                                </div>
                                <div class="player-ucp-menu-item">
                                    <img src="/static/img/icons/edit.png" class="fl" /> <span class="fl"><a href="/authenticate/logout">Logout</a></span>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="fr">
                                <img class="avatar big player-avatar-chg" src="{$pgUser.avatar}" width="100" alt="" />
                            </div>
                        </div>
                    </li>
                    <li class="player-notifications">
                        <div id="player-notifications" style="display: none;">
                            {include 'inc/notifications.tpl'}
                        </div>
                        <span>
                            {$notificationCount = player('countNotifications')}
                            <a href="" id="player-notifications-numb" facebox-type="fb-player-list">
                                <span id="player-notifications-count" class="noteNumb{if $notificationCount > 0} new{/if}">{$notificationCount}</span>
                            </a>
                        </span>
                    </li>
                </ul>
                <ul id="top-bar-nav" class="top-bar">
					{if $pgUser.active == 1}
                    <li class="player-check-nn">
                        <span>
                            <span class="noteNumb player-check-c {if $pgUser.checkedIn == 1}checkedIn{else}checkedOut{/if}">{if $pgUser.checkedIn == 1}Eingecheckt{else}Ausgecheckt{/if}</span>
                        </span>
                    </li>
					{/if}
                    <li id="top-bar-checked-players-item" class="submenu">
                        <div id="checkedPlayers" style="display: none;">
                            {include 'inc/checkedPlayers.tpl'}
                        </div>
                        <span>
                            <a href="#checkedPlayers" class="facebox-tr" facebox-type="fb-player-list">
                                <span class="noteNumb players-checked-in"><span id="topnav-player-checked" class="val">{player('getCheckedPlayersCount')}</span> Spieler aktiv</span>
                            </a>
                        </span>
                    </li>
                </ul>
            </div>
        </nav>

        <nav id="header-nav">
            <div class="wrapper">
                <ul>
                    <li class="first"><a href="/">Dashboard</a></li>
                    <li><a href="/play">Spielen!</a></li>
                    <li><a href="/ranking">Tabelle</a></li>
                    <li><a href="/matches">Spiele</a></li>
                    <li><a href="/stats">Gesamtstatistik</a></li>
                    <li><a href="/awards">Awards</a></li>
                    <li><a href="/halloffame">Hall Of Fame</a></li>
                    <li><a href="/archive">Archiv</a></li>
                    <li><a href="/info">PackGyver</a>
                        <ul class="sub-menu">
                            <li class="first"><a href="/info/rules">Regeln</a></li>
                            <li><a href="/info/points">Punkteübersicht</a></li>
                            <li{if !player('isAdmin')} class="last"{/if}><a href="/info">Über PackGyver</a></li>
                            {if player('isAdmin')}
                            <li class="last"><a href="/admin">Admin</a></li>
                            {/if}
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="main-content" class="wrapper">
            {$pgMainContent}
            <div class="clear"></div>
        </div>

        <div id="footer" class="container">
            <div class="wrapper">
                <ul id="menu-footer-menu" class="fl">
                    <li><a href="/info">Über PackGyver</a></li>
                    <li class="divider">|</li>
                    <li><a href="#pg-feedback-open" class="facebox-tr">Feedback senden</a></li>
                </ul>

                <div class="clear"></div>

                <div id="footer-pusher-info" class="margint10">
                    {if $pgWSService.enabled}
                    <span class="bold">Push-Service:</span> <strong class="posTrend">Online</strong> <em>({$pgWSService.service.name})</em>
                    {else}
                    <span>Push-Service:</span> <strong class="negTrend">Offline</strong>
                    {/if}
                </div>

                {if $pgIsGoogleAnalyticsEnabled}
                    {include 'inc/analytics.tpl'}
                {/if}
                <div class="clear"></div>
            </div>
        </div>

        <div class="hide">
            {include 'inc/feedback.tpl'}
            {include 'inc/matchBox.tpl'}
        </div>
        {/if}

		<script>
			window.onerror=function(m,u,l){ $.post("/log/js",{ msg:m,url:u,line:l,window:window.location.href });return true };

			$.packGyver.site.url='{$pgUrl}';

			{if player('isLoggedIn')}
			$.packGyver.player = {json_encode $pgUser};
			{/if}
        </script>

		{if $pgWSService.enabled && $pgWSService.service.name}
			<script>
				var pgWSServiceOptions = {json_encode $pgWSService.service.options};
			</script>

			<script src="/static/js/wsservice/_wsservice.js"></script>

			{$wsserviceTpl = cat($pgWSService.service.name, '.tpl')}
			{include 'inc/wsservice/$wsserviceTpl'}
		{/if}
    </body>
</html>