<div id="content" class="grid9 content-shadow">

	<h4 class="headline"><span>Cache</span></h4>
	<button name="cacheClear" class="clearCacheBtn red">Cache komplett leeren</button>
	<button name="cacheBackend" class="clearCacheBtn">CacheBackend leeren</button>
	<button name="autoload" class="clearCacheBtn">Autoload Cache leeren</button>
	<button name="dwoo" class="clearCacheBtn">Dwoo Cache leeren</button>
	<br />
	<button name="playerStats" class="clearCacheBtn">PlayerStats Cache leeren</button>
	<button name="playerStatsAlltime" class="clearCacheBtn">PlayerStats (Alltime) Cache leeren</button>
	<button name="playerStatsSeason" class="clearCacheBtn">PlayerStats (Season) Cache leeren</button>

</div>