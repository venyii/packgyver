<div id="content" class="grid6 content-shadow">
	<h4 class="headline"><span>Übersicht</span></h4>
	Hallo {$pgUser.firstname}!
</div>

<div class="grid3 content-shadow">
	<h4 class="headline"><span>Derzeit online ({$activePlayers|count})</span></h4>
	<div class="margint15">
		<ul>
			{foreach $activePlayers activePlayer}
			<li class="margint10">
				<img src="{$activePlayer.avatar}" alt="" class="avatar small fl marginr5" />
				<div><a href="/player/{$activePlayer.uid}">{$activePlayer.firstname} {$activePlayer.lastname}</a><br /><i style="font-size: 10px;">(<time class="timeago" title="{$activePlayer.lastActivity}" datetime="{pgdate('formatTimeago', $activePlayer._lastActivity)}"></time>)</i></div>
			</li>
			{/foreach}
		</ul>
	</div>
</div>