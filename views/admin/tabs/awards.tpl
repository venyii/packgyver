<div id="content" class="grid9 content-shadow">
	
{if $error}

<h4 class="headline"><span>Awards</span></h4>
<div class="alert alert-error">
    <strong>Fehler</strong><br />
    {$error}
</div>

{else}

    {if $function == 'addPlayerAward' || $function == 'editPlayerAward'}

        {if $function == 'addPlayerAward'}
            <h4 class="headline"><span>Spieler auszeichnen</span></h4>
        {else}
	    <h4 class="headline">
		<span>PlayerAward editieren</span>
		<span class="fr btn-group">
		    <button class="deletePlayerAward" awardId="{$playerAward.id}"><i class="icon-remove"></i>PlayerAward löschen</button>
		</span>
	    </h4>
        {/if}

        <form action="/admin/awards/{if $edit}edit{else}add{/if}/playerAward/{$playerAward.id}/save" method="post">
            <div class="marginb10">
                <label for="playerId">Spieler</label>
                <select id="playerId" name="playerId">
                    {foreach $players player}
                    <option value="{$player.id}"{if $edit && $playerId == $player.id} selected="selected"{/if}>{$player.firstname} {$player.lastname}</option>
                    {/foreach}
                </select>
            </div>
            <div class="marginb10">
                <label for="awardId">Award</label>
                <select id="awardId" name="awardId">
                    {foreach $awards award}
                    <option value="{$award.id}"{if $edit && $awardId == $award.id} selected="selected"{/if}>{$award.name}</option>
                    {/foreach}
                </select>
            </div>
            <div class="marginb10">
                <label for="comment">Kommentar</label>
                <input type="text" id="comment" name="comment"{if $edit} value="{$playerAward.comment}"{/if} />
            </div>
            {if !$edit}
            <div class="marginb10">
                <label for="sendNotification">Spieler Notification senden?</label>
                <input type="checkbox" id="sendNotification" name="sendNotification" />
            </div>
            {/if}
            <div class="marginb10">
                <button type="submit" name="save"><i class="icon-arrow-right"></i>{if $edit}Award editieren{else}Award verleihen{/if}</button>
            </div>
        </form>

    {elseif $function == 'addAward' || $function == 'editAward'}

        {if $function == 'addAward'}
        <h4 class="headline"><span>Award anlegen</span></h4>
        {else}
        <h4 class="headline"><span>Award editieren</span></h4>
        {/if}
        <form action="/admin/awards/{if $edit}edit{else}add{/if}/award/{$award.id}/save" enctype="multipart/form-data" method="post">
            <div class="marginb10">
                <label for="name">Name</label>
                <input type="text" id="name" name="name"{if $edit} value="{$award.name}"{/if} />
            </div>
            <div class="marginb10">
                <label for="description">Beschreibung</label>
                <textarea id="description" name="description">{if $edit}{$award.description}{/if}</textarea>
            </div>
            <div class="marginb10">
                <label for="name">Key</label>
                <input type="text" id="key" name="key"{if $edit} value="{$award.key}"{/if} />
            </div>
            <div class="marginb10">
                {if $edit}
                <img src="{$award.logo}" alt="" />
                {/if}
                <label for="logo">Logo</label>
                <input type="file" id="logo" name="logo" />
            </div>
            <div class="marginb10">
                <input type="submit" name="save" value="Speichern" />
            </div>
        </form>

    {elseif $function == 'detect'}

        <h1 class="marginb20" style="font-size: 65px; text-align: center;">{$month}/{$year}</h1>
        <div class="alert alert-info textcenter">Awards in einer vergangenen Saison vergeben: /admin/awards/detect/$month/$year</div>

        {if $status|count > 0}
            <div class="alert alert-success">
                <a class="close">×</a>
                <strong>Status</strong>
                <ul>
                {foreach $status stKey stMsg}
                    <li>{$stKey}: {if $stMsg}Vergeben!{else}Fehler{/if}</li>
                {/foreach}
                </ul>
            </div>
        {/if}

        <form action="" method="post">

            <div class="marginb20">
                <h4 class="headline"><span>MonsterAward</span></h4>
                {if $mps|count > 0}
                    {foreach $mps mpsPlayer}
                        {$mpsPlayer.player.firstname} {$mpsPlayer.player.lastname}<br />
                    {/foreach}
                {else}
                    Es sind keine Spieler für den MonsterPack Award verfügbar.
                {/if}
            </div>

            <div class="marginb20">
                <h4 class="headline"><span>Ranking</span></h4>
                {foreach $ranking rank rankingPlayer}
                    {$rank+1}. {$rankingPlayer.firstname} {$rankingPlayer.lastname}<br />
                {/foreach}
            </div>

            <div class="margint15">
                <input type="checkbox" name="awardPlayers" id="awardPlayers" /> <label for="awardPlayers">Awards vergeben?</label>
            </div>
            <div class="margint15">
                <input type="submit" name="save" value="Awards vergeben" />
            </div>

        </form>

    {else}

        <h4 class="headline">
            <span>Awards</span>
            <span class="fr btn-group">
                <button id="addPlayerAward" class="btn-redirect" data-href="/admin/awards/add/playerAward">Spieler auszeichnen</button>
                <button id="addAward" class="btn-redirect" data-href="/admin/awards/add/award">Award hinzufügen</button>
            </span>
        </h4>

        {foreach $awards award name=awardList}
        {$awardId = $award.id}
        <div class="fl" style="width: 110px; margin: 10px;">
            <div title="{$award.name} - {$award.description}" class="award tipsy-tt">
                <img src="{$award.logo}" alt="">
            </div>
            <button class="facebox-tr margint10" facebox-type="fb-player-list" href="#awPlayers_{$award.id}"><i class="icon-user"></i>{count $pAwards.$awardId} Träger</button>
            <button class="btn-redirect" data-href="/admin/awards/edit/award/{$award.id}">Editieren</button><br />
            {*<button class="deleteAward" awardId="{$award.id}">Löschen</button><br />*}
        </div>
        {/foreach}
        <div class="clearfix"></div>

	<div class="hide">
            {foreach $pAwards pAwardId pAward}
            <div id="awPlayers_{$pAwardId}">
                <h1>Spieler mit diesem Award</h1>
                <div class="fb-player-list-content fb-player-list-awards">
                        {if count($pAward) > 0}
                            {foreach $pAward playerAward}
                            <div class="fb-player-list-content-item{if $dwoo.foreach.default.last} last{/if}">
                                <img src="{$playerAward.player.avatar}" alt="" class="avatar medium fl" />
                                <div class="fl player-name" style="margin-top: 3px;">
                                    <a href="/player/{$playerAward.player.uid}">
                                        {$playerAward.player.firstname} {$playerAward.player.lastname}
				    </a>
				    <div style="font-size: 12px;">
					<ul>
					    {foreach $playerAward.awards playerAwardCollection}
					    <li>
						<a href="/admin/awards/edit/playerAward/{$playerAwardCollection.id}">
						    {if $playerAwardCollection.comment}
							{$playerAwardCollection.comment}
						    {else}
							<em>n/a</em>
						    {/if}
						</a>
					    </li>
					    {/foreach}
					</ul>
				    </div>
                                </div>
				<div class="clearfix"></div>
                            </div>
                            {/foreach}
                        {else}
                            <div class="margin10">Bisher wurde keinem Spieler dieser Award verliehen.</div>
                        {/if}
                </div>
                <div class="fb-controls shadow textright">
                    <button class="fb-close-trigger">Schließen</button>
                </div>
            </div>
            {/foreach}
        </div>

    {/if}
{/if}

</div>