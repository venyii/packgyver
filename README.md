# PackGyver

* Rename ```config.inc.php.dist``` to ```config.inc.php```
* Give write permission to ```app/{cache,logs,storage}```
* Import ```_dev/db/db_scheme.sql```
