$(function() {
	$(document).click(function() {
		hidePlayerDropdowns();
	});

	$('select.no-search').not('.noSelect2').select2({
        minimumResultsForSearch: -1
    });
	$('select').not('.noSelect2').not('.no-search').select2();
	$('.tipsy-tt').tipsy();
	$('.facebox-tr').facebox();
	$('time.timeago').timeago();
    $(document).on('click', '.fb-close-trigger', $.facebox.close);

    $('input[type="datetime"], input.pickert').pickadate({
        selectYears: true,
        selectMonths: true,
        firstDay: 1,
        monthsFull: [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember' ],
        monthsShort: [ 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez' ],
        weekdaysFull: [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ],
        weekdaysShort: [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
        today: 'Heute',
        clear: 'Löschen',
        format: 'dd.mm.yyyy'
    });

	/* main menu dropdown */
	$('nav#header-nav ul li').hover(
		function () {
			$(this).find('ul.sub-menu').css({
				'top': '50px'
			}).show();
		},
		function () {
			$(this).find('ul.sub-menu').hide();
		});

	$('.player-dropdown-cp').click(function(e) {
		e.stopPropagation();
	});

	/* remove alert boxes */
	$(document).on('click', 'div.alert a.close', function(e) {
		e.preventDefault();
		$(this).parent().fadeOut(300, function() {
			$(this).remove();
		});
	});

	$('div.content-toggle').click(function() {
		$(this).next('div').slideToggle(200, function() {
			$(this).toggleClass('hide');
		});
	});

	/* top nav dropdown */
	$('nav#top-bar ul.top-bar li.submenu').click(function (e) {
		var submenu = $(this).find('div.player-dropdown-cp');

		if (submenu.length > 0) {
			e.stopPropagation();
			e.preventDefault();

			if (!submenu.is(':visible')) {
				hidePlayerDropdowns();
				submenu.css('visibility', 'visible').show();
			} else {
				submenu.css('visibility', 'hidden').hide();
			}
		}
	});

	function hidePlayerDropdowns() {
		$('nav#top-bar div.player-dropdown-cp:visible').hide();
	}

	/* ranking sortable */
    $('#ranking').dataTable({
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            { "sType": "signed-num" },
            null
        ],
        "aaSorting": [[ 7, "desc" ]],
        "sDom": 'rt',
        "asStripeClasses": [],
        "bPaginate": false,
        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": [ 0 ]
        }]
    });
    $('#ranking-alltime').dataTable({
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            { "sType": "signed-num" },
            { "sType": "signed-num" },
            null
        ],
        "aaSorting": [[ 11, "desc" ]],
        "sDom": 'rt',
        "bPaginate": false,
        "asStripeClasses": [],
        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": [ 0 ]
        }]
    });
    $('#ranking-archive').dataTable({
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            { "sType": "signed-num" },
            { "sType": "signed-num" },
            null,
            null,
            null
        ],
        "aaSorting": [[ 12, "desc" ]],
        "sDom": 'rt',
        "bPaginate": false,
        "asStripeClasses": [],
        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": [ 0 ]
        }]
    });

	/* extension to sort signed numbers correctly */
	$.extend($.fn.dataTableExt.oSort, {
		"signed-num-pre": function ( a ) {
			return (a=="-" || a==="") ? 0 : a.replace('+','')*1;
		},
		"signed-num-asc": function ( a, b ) {
			return ((a < b) ? -1 : ((a > b) ? 1 : 0));
		},
		"signed-num-desc": function ( a, b ) {
			return ((a < b) ? 1 : ((a > b) ? -1 : 0));
		}
	});

	/* toggle sth */
	$('.toggle').click(function(e) {
		e.preventDefault();
		var trgt = $(this).attr('target');
		if (typeof trgt !== 'undefined' && trgt.length > 0){
			$(trgt).toggle();
		}
	});

	/* global class for button redirects */
    $(document).on('click', '.btn-redirect', function() {
		var location = $(this).attr('data-href');
		if(undefined !== location && location.length > 0) {
			window.location = location;
		}
	});

	/* exec post request */
	$('.btn-request').click(function() {
		var url = $(this).attr('data-href');
		if(undefined !== url && url.length > 0) {
			$.packGyver.site.pageLoader(true);
			$.post(url, null, function() {
				$.packGyver.site.pageLoader(false);
			});
		}
	});

	/* match info in overview */
    $(document).on('click', '.match-info', function() {
		$.packGyver.site.notify('Spielinformationen', $($(this).attr('data-href')).html(), getButton('Schließen', 'fb-close-trigger'));
	});

	/* change player check status */
	$('.player-check').change(function() {
		var playerId = (undefined !== $(this).attr('playerId')) ? $(this).attr('playerId') : null;
		var type = $(this).is(':checked') ? 'in' : 'out';
		$.packGyver.players.check(type, playerId);
	});
	$('.player-check-c').click(function() {
		var playerId = (undefined !== $(this).attr('playerId')) ? parseInt($(this).attr('playerId')) : '';
		var type = $(this).hasClass('checkedOut') ? 'in' : 'out';

		if (type === 'in') {
			$.packGyver.player.active = true;
			$(this).removeClass('checkedOut').addClass('checkedIn').text($.packGyver.players.status.checkedIn);
			if (!WS_SERVICE_ENABLED) {
				changeTopNavCheckedPlayers(1, '+');
			}
		} else {
			$.packGyver.player.active = false;
			$(this).removeClass('checkedIn').addClass('checkedOut').text($.packGyver.players.status.checkedOut);
			if (!WS_SERVICE_ENABLED) {
				changeTopNavCheckedPlayers(1, '-');
			}
		}

		$.packGyver.players.check(type, playerId);
	});

	/* preselect first type item in feedback form */
    $(document).on('click', 'div#facebox #pg-feedback-form .pg-feedback-options li', function() {
		var hi = $('div#facebox #pg-feedback-form input[name="type"]');
		var type = $(this);

		if(hi.length > 0) {
			$('div#facebox #pg-feedback-form .pg-feedback-options li').each(function() {
				$(this).removeClass('selected');
			});
			hi.val(type.text());
			type.addClass('selected');
		}
	});

	/* send feedback */
    $(document).on('click', 'div#facebox button.pg-feedback-send', function() {
		e.preventDefault();

		var form = $('div#facebox div#pg-feedback #pg-feedback-form');
		var data = form.serialize() + '&browser=' + encodeURIComponent($.packGyver.utils.getBrowser());

		$.facebox.close();
		$.packGyver.site.pageLoader(true);

		$.post('/feedback/save', data, function(response) {
			var msg;
			var btn = getButton('Schließen', 'fb-close-trigger');

			if (response.success) {
				msg = '<div class="margin20">Vielen Dank für deine Hilfe PackGyver besser zu machen!</div>';
			} else {
				msg = '<div class="margin20">Ups... da ist wohl ein Fehler aufgetreten!</div>';
			}

			setTimeout(function() {
				$.packGyver.site.pageLoader(false);
				$.packGyver.site.notify('Feedback', msg, btn);
			}, 1000);
		}, 'json');
	});

	/* change user password */
	$('div#changePassword form').submit(function(e) {
		e.preventDefault();
		$.packGyver.site.pageLoader(true);
		$.post($(this).attr('action'), $(this).serialize(), function(res) {
			var btn = getButton('Schließen', 'fb-close-trigger');
			$.packGyver.site.pageLoader(false);

			if (res.success) {
				if (res.redirect) {
					btn = getButton('Einloggen', 'btn-redirect', [{
						name: 'data-href',
						value: '/'
					}]);
					$.packGyver.site.notify('Passwort ändern', '<div class="margin20">Das Passwort wurde geändert - Du musst dich neu einloggen.</div>', btn);
				} else {
					$.packGyver.site.notify('Passwort ändern', '<div class="margin20">Das Passwort wurde geändert</div>', btn);
				}
			} else {
				var error = '<div class="margin20"><strong>Mögliche Fehler sind:</strong><ul><li>Aktuelles Passwort ist falsch</li><li>PW-Mindestlänge nicht beachtet</li></ul></div>';
				$.packGyver.site.notify('Passwort ändern', error, btn);
			}
		}, 'json');
	});
    
	/* change user avatar via ajax from profile */
	$('#avatarUpload #avatarFileInput').change(function() {
		var fileInput = $(this);
		var playerId = parseInt($(this).attr('player'));

		$.packGyver.site.pageLoader(true);
		$("#avatarUpload > form").ajaxForm({
			dataType: 'json',
			success: function(res) {
				$.packGyver.site.pageLoader(false);
				if(res.success) {
					$('img.player-avatar-chg').fadeOut(400, function() {
						var d = new Date();
						$('img.player-avatar-chg').attr('src', res.src + '?d='+d.getTime()).fadeIn(400);
					});
				} else {
					var btn = getButton('Schließen', 'fb-close-trigger');
					$.packGyver.site.notify('Avatar hochladen', '<div class="margin20">' + res.error + '</div>', btn);
				}
				fileInput.val('');
			},
			data: {
				'playerId': playerId
			}
		}).submit();
	});

	$('button#toggle-hidden-players').click(function() {
		var icon = $('i', $(this));

		if (icon.hasClass('icon-eye-open')) {
			icon.removeClass('icon-eye-open');
			icon.addClass('icon-eye-close');
			$('span.val', $(this)).text('ausblenden');
		} else {
			icon.removeClass('icon-eye-close');
			icon.addClass('icon-eye-open');
			$('span.val', $(this)).text('anzeigen');
		}

		$('table#ranking tbody tr.player-inactive').toggleClass('hide');
	});

	$.packGyver.players.notifications.checkDOMNotifications();

	$('a#player-notifications-numb').click(function(e) {
		e.preventDefault();

		if ($.packGyver.players.notifications.pullable) {
			$.packGyver.players.notifications.pull();
		} else if ($.packGyver.players.notifications.hasNotifications) {
			$.packGyver.players.notifications.openBox(true);
		} else {
			$.packGyver.players.notifications.openBox(false);
		}
	});

	/* HOTKEYS */
	$(document).bind('keydown', 'ctrl+p', function (e){
		window.location = '/play';
		return false;
	});
    /* /HOTKEYS */

});

function centerPanel() {
	var windowHeight = $(window).height();
	var panelHeight = $("div#pg-special-panel").height();
	var setHeight = (windowHeight/2) - panelHeight;

	if (setHeight > 0) {
		$("div#pg-special-panel").css('margin-top', setHeight);
	}
}

function getButton(text, styleClass, attrs) {
	var attributes = '';
	if (attrs) {
		attrs.forEach(function (obj) {
			attributes += obj.name + '="' + obj.value + '" ';
		});
	}
	return '<button class="' + styleClass + '"' + attributes + '>' + text + '</button>';
}

function changeTopNavCheckedPlayers(n, t) {
	if (!n) {
		n = 1;
	}

	var oldValueEl = $('span#topnav-player-checked');
	var oldValue = parseInt(oldValueEl.text());
	var newValue;

	if (t === '+') {
		newValue = oldValue + n;
		$('.fb-player-list-content-item.personal').removeClass('hide');
	} else {
		newValue = oldValue - n;
		$('.fb-player-list-content-item.personal').addClass('hide');
	}

	if (newValue === 0) {
		$('div#checked-players-none').show();
	} else if (oldValue === 0) {
		$('div#checked-players-none').hide();
	}

	oldValueEl.text(newValue);
}