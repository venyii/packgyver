/*
 * Facebox (for jQuery)
 * version: 1.3
 * @requires jQuery v1.2 or later
 * @homepage https://github.com/defunkt/facebox
 *
 * Licensed under the MIT:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright Forever Chris Wanstrath, Kyle Neath
 *
 * Usage:
 *
 *  jQuery(document).ready(function() {
 *    jQuery('a[rel*=facebox]').facebox()
 *  })
 *
 *  <a href="#terms" rel="facebox">Terms</a>
 *    Loads the #terms div in the box
 *
 *  <a href="terms.html" rel="facebox">Terms</a>
 *    Loads the terms.html page in the box
 *
 *  <a href="terms.png" rel="facebox">Terms</a>
 *    Loads the terms.png image in the box
 *
 *
 *  You can also use it programmatically:
 *
 *    jQuery.facebox('some html')
 *    jQuery.facebox('some html', 'my-groovy-style')
 *
 *  The above will open a facebox with "some html" as the content.
 *
 *    jQuery.facebox(function($) {
 *      $.get('blah.html', function(data) { $.facebox(data) })
 *    })
 *
 *  The above will show a loading screen before the passed function is called,
 *  allowing for a better ajaxy experience.
 *
 *  The facebox function can also display an ajax page, an image, or the contents of a div:
 *
 *    jQuery.facebox({ ajax: 'remote.html' })
 *    jQuery.facebox({ ajax: 'remote.html' }, 'my-groovy-style')
 *    jQuery.facebox({ image: 'stairs.jpg' })
 *    jQuery.facebox({ image: 'stairs.jpg' }, 'my-groovy-style')
 *    jQuery.facebox({ div: '#box' })
 *    jQuery.facebox({ div: '#box' }, 'my-groovy-style')
 *
 *  Want to close the facebox?  Trigger the 'close.facebox' document event:
 *
 *    jQuery(document).trigger('close.facebox')
 *
 *  Facebox also has a bunch of other hooks:
 *
 *    loading.facebox
 *    beforeReveal.facebox
 *    reveal.facebox (aliased as 'afterReveal.facebox')
 *    init.facebox
 *    afterClose.facebox
 *
 *  Simply bind a function to any of these hooks:
 *
 *   $(document).bind('reveal.facebox', function() { ...stuff to do after the facebox and contents are revealed... })
 *
 */
(function($) {
    $.facebox = function(data, klass) {
	$.facebox.loading(data.settings || [])

	if (data.ajax) fillFaceboxFromAjax(data.ajax, klass)
	else if (data.image) fillFaceboxFromImage(data.image, klass)
	else if (data.div) fillFaceboxFromHref(data.div, klass)
	else if ($.isFunction(data)) data.call($)
	else $.facebox.reveal(data, klass)
    }

    /*
     * Public, $.facebox methods
     */
    $.extend($.facebox, {
	settings: {
	    opacity : 0.2,
	    type : null,
	    modal : false,
	    loadingImage : '/static/img/vendor/facebox/loading.gif',
	    closeImage : '/static/img/vendor/facebox/closelabel.png',
	    imageTypes : [ 'png', 'jpg', 'jpeg', 'gif' ],
	    faceboxHtml : ['<div id="facebox" style="display:none;">',
	    '<div class="fb-popup">',
	    '<div class="fb-content">',
	    '</div>',
	    '<a href="#" class="fb-close"></a>',
	    '</div>',
	    '</div>'].join('')
	},

	loading: function() {
	    init();
	    if ($('#facebox .fb-loading').length == 1) {
		return true;
	    }
	    showOverlay();

	    $('#facebox .fb-content').empty().
	    append('<div class="fb-loading"><img src="'+$.facebox.settings.loadingImage+'"/></div>');

	    $('#facebox').show().css({
		top:	getPageScroll()[1] + (getPageHeight() / 10),
		left:	$(window).width() / 2 - ($('#facebox .fb-popup').outerWidth() / 2)
	    });

	    $(document).bind('keydown.facebox', function(e) {
		if (e.keyCode == 27 && false === $.facebox.settings.modal) {
		    $.facebox.close();
		}
		return true;
	    })
	    $(document).trigger('loading.facebox');
	},

	reveal: function(data, klass) {
	    $(document).trigger('beforeReveal.facebox');
	    if (klass) {
		if (typeof klass === 'object') {
		    $.extend($.facebox.settings, klass);
		    if($.facebox.settings.modal === true) {
			$('#facebox-overlay').unbind('click');
		    }
		} else {
		    $('#facebox .fb-content').addClass(klass);
		}
	    }
	    if (null !== $.facebox.settings.type) {
		$('#facebox').addClass($.facebox.settings.type);
	    }
	    $('#facebox .fb-content').empty().append(data);
	    $('#facebox .fb-popup').children().fadeIn('normal');
	    $('#facebox').css('left', $(window).width() / 2 - ($('#facebox .fb-popup').outerWidth() / 2));
	    $(document).trigger('reveal.facebox').trigger('afterReveal.facebox');
	},

	close: function() {
	    $(document).trigger('close.facebox');
	    return false;
	}
    });

    /*
     * Public, $.fn methods
     */
    $.fn.facebox = function(settings) {
	if ($(this).length == 0) {
	    return;
	}

	init(settings);

	function clickHandler() {
	    $.facebox.loading(true);

	    // support for rel="facebox.inline_popup" syntax, to add a class
	    // also supports deprecated "facebox[.inline_popup]" syntax
	    var klass;
	    if (undefined !== this.rel) {
		klass = this.rel.match(/facebox\[?\.(\w+)\]?/);
		if (klass) { 
		    klass = klass[1];
		}
	    }
	    var type = $(this).attr('facebox-type');
	    if (typeof type !== 'undefined' && type.length > 0) {
		$.facebox.settings.type = type;
	    } else {
		$.facebox.settings.type = null;
	    }

	    fillFaceboxFromHref((!this.href) ? $(this).attr('href') : this.href, klass);
	    return false;
	}

	return this.bind('click.facebox', clickHandler);
    }

    /*
     * Private methods
     */
    // called one time to setup facebox on this page
    function init(settings) {
	if ($.facebox.settings.inited) {
	    return true;
	} else {
	    $.facebox.settings.inited = true;
	}

	$(document).trigger('init.facebox');
	makeCompatible();

	var imageTypes = $.facebox.settings.imageTypes.join('|');
	$.facebox.settings.imageTypesRegexp = new RegExp('\\.(' + imageTypes + ')(\\?.*)?$', 'i');

	if (settings) {
	    $.extend($.facebox.settings, settings);
	}
	$('body').append($.facebox.settings.faceboxHtml);

	var preload = [ new Image(), new Image() ];
	preload[0].src = $.facebox.settings.closeImage;
	preload[1].src = $.facebox.settings.loadingImage;

	$('#facebox').find('.b:first, .bl').each(function() {
	    preload.push(new Image());
	    preload.slice(-1).src = $(this).css('background-image').replace(/url\((.+)\)/, '$1');
	})

	if (true === $.facebox.settings.modal) {
	    $('#facebox .close').remove();
	} else {
	    $('#facebox .fb-close').click($.facebox.close)
	    .append('<img src="' + $.facebox.settings.closeImage
		+ '" class="fb-close-image" title="close">');
	}
    }

    // getPageScroll() by quirksmode.com
    function getPageScroll() {
	var xScroll, yScroll;
	if (self.pageYOffset) {
	    yScroll = self.pageYOffset;
	    xScroll = self.pageXOffset;
	} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
	    yScroll = document.documentElement.scrollTop;
	    xScroll = document.documentElement.scrollLeft;
	} else if (document.body) {// all other Explorers
	    yScroll = document.body.scrollTop;
	    xScroll = document.body.scrollLeft;
	}
	return new Array(xScroll,yScroll);
    }

    // Adapted from getPageSize() by quirksmode.com
    function getPageHeight() {
	var windowHeight;
	if (self.innerHeight) {	// all except Explorer
	    windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
	    windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
	    windowHeight = document.body.clientHeight;
	}
	return windowHeight;
    }

    /**
     * Backwards compatibility
     */
    function makeCompatible() {
	var $s = $.facebox.settings;

	$s.loadingImage = $s.loading_image || $s.loadingImage;
	$s.closeImage = $s.close_image || $s.closeImage;
	$s.imageTypes = $s.image_types || $s.imageTypes;
	$s.faceboxHtml = $s.facebox_html || $s.faceboxHtml;
    }

    /**
     * Figures out what you want to display and displays it.
     * 
     * Formats are:
     *      - div: #id
     *      - image: example.jpg
     *      - ajax: anything else
     *      
     * @param href string
     * @param klass string
     */
    function fillFaceboxFromHref(href, klass) {
	// div
	if (href.match(/#/)) {
	    var url    = window.location.href.split('#')[0];
	    var target = href.replace(url,'');
	    if (target == '#') {
		return;
	    }
	    $.facebox.reveal($(target).html(), klass);

	// image
	} else if (href.match($.facebox.settings.imageTypesRegexp)) {
	    fillFaceboxFromImage(href, klass);
	// ajax
	} else {
	    fillFaceboxFromAjax(href, klass);
	}
    }

    function fillFaceboxFromImage(href, klass) {
	var image = new Image();
	image.onload = function() {
	    $.facebox.reveal('<div class="fb-image"><img src="' + image.src + '" /></div>', klass);
	}
	image.src = href;
    }

    function fillFaceboxFromAjax(href, klass) {
	$.facebox.jqxhr = $.get(href, function(data) {
	    $.facebox.reveal(data, klass);
	});
    }

    function showOverlay() {
	if ($('#facebox-overlay').length === 0) {
	    $("body").append('<div id="facebox-overlay" class="facebox-hide"></div>');
	}
	var overlay = $('#facebox-overlay');

	overlay.hide().addClass("facebox-overlay-bg").css('opacity', $.facebox.settings.opacity);

	if (false === $.facebox.settings.modal) {
	    overlay.click($.facebox.close);
	}
	overlay.fadeIn(200);

	return false;
    }

    function hideOverlay() {
	$('#facebox-overlay').fadeOut(200, function(){
	    $("#facebox-overlay").removeClass("facebox-overlay-bg");
	    $("#facebox-overlay").addClass("facebox-hide");
	    $("#facebox-overlay").remove();
	});

	return false;
    }

    /*
     * Bindings
     */
    $(document).bind('close.facebox', function() {
	if ($.facebox.jqxhr) {
	    $.facebox.jqxhr.abort();
	    $.facebox.jqxhr = null;
	}
	$(document).unbind('keydown.facebox');
	$('#facebox').fadeOut(function() {
	    $('#facebox').removeClass();
	    $('#facebox .fb-content').removeClass().addClass('fb-content');
	    $('#facebox .fb-loading').remove();
	    $(document).trigger('afterClose.facebox');
	})
	hideOverlay();
    })
})(jQuery);