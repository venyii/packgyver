<?php

require __DIR__ . '/../../app/init.inc.php';

foreach (PlayerCollection::create() as $player) {
    /* @var PlayerModel $player */
    $player->getStats(PlayerStatistics::CACHETYPE_SEASON)->recalculate();
    $player->getStats(PlayerStatistics::CACHETYPE_ALLTIME)->recalculate();

    CacheManager::clearPlayerCache($player->getEntity());
}

AdminManager::clearCache(AdminManager::CACHE_TYPE_CACHEBACKEND);
AdminManager::clearCache(AdminManager::CACHE_TYPE_DWOO);
