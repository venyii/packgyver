<?php

require __DIR__ . '/../../app/init.inc.php';

PlayerModel::addPlayer(PG::getDB(), 'fname', 'lname', 'admin@example.com', 'admin', '', true, 0, AuthenticationManager::TYPE_ADMIN);

$faker = Faker\Factory::create();
$logger = new \Monolog\Logger('pg_testdata', [
//    new \Monolog\Handler\StreamHandler(__DIR__ . '/testdata.log', \Monolog\Logger::DEBUG),
    new \Monolog\Handler\StreamHandler('php://output', \Monolog\Logger::DEBUG),
]);

$db = PG::getDB();

//$players = PlayerModel::getPlayers($db);
$players = [];

for ($i = 0; $i < 50; $i++) {
    $player = PlayerModel::addPlayer(
        $db,
        $faker->firstName,
        $faker->lastName,
        $faker->safeEmail,
        '123456',
        '',
        $faker->boolean(90),
        $faker->boolean(5),
        $faker->boolean(5))
    ;
    $player->setDeleted($faker->boolean(5));

    $logger->info('added player', [$player->getId()]);

    $players[] = $player;
}

$archive = new SeasonArchive();

$currentDate = new \DateTime();
$startDate = new DateTime('2018-11-10');

for ($y = 2018; $y <= 2019; $y++) {
    for ($m = 1; $m <= 12; $m++) {
        $daysInMonth = round((mktime(0, 0, 0, $m + 1, 1, $y) - mktime(0, 0, 0, $m, 1, $y)) / 86400);

        for ($d = 1; $d <= $daysInMonth; $d++) {
            $loopDate = new DateTime("$y-$m-$d");

            if ($currentDate < $loopDate) {
                $logger->info('Stopping, not generating future matches');
                break 3;
            }
            if ($startDate > $loopDate) {
                continue;
            }

            for ($i = 0; $i < $faker->numberBetween(15, 200); $i++) {
                $scores = $faker->shuffleArray([$faker->numberBetween(0, 9), 10]);

                $date = new DateTime();
                $date->setDate($y, $m, $d);
                $date->setTime($faker->numberBetween(7, 18), $faker->numberBetween(0, 59));

                $matchPlayers = $faker->randomElements($players, 4);

                $matchEntity = new MatchEntity();
                $matchEntity->setRandom($faker->boolean(95));
                $matchEntity->setDateCreated($date->format('Y-m-d H:i:s'));
                $matchEntity->setCreatedByPlayerId($faker->randomElement($matchPlayers)->getId());

                $type = $faker->randomElement([MatchModel::TYPE_1ON1, MatchModel::TYPE_2ON2]);

                $match = new MatchModel($matchEntity);
                $match->setType($type);
                $match->setDateCreated($date->format('Y-m-d H:i:s'));
                $match->setSavedByPlayer(new PlayerModel($faker->randomElement($matchPlayers)));
                $match->setDateSaved($date->add(new DateInterval('PT' . $faker->numberBetween(600, 1200) . 'S'))->getTimestamp());
                $match->setScoreRed($scores[0]);
                $match->setScoreYellow($scores[1]);
                $match->setDeleted($faker->boolean(5));

                if ($type === MatchModel::TYPE_1ON1) {
                    $match->setRedSinglePlayer(new PlayerModel($matchPlayers[0]));
                    $match->setYellowSinglePlayer(new PlayerModel($matchPlayers[1]));
                } else {
                    $match->setRedGoalPlayer(new PlayerModel($matchPlayers[0]));
                    $match->setRedOffensePlayer(new PlayerModel($matchPlayers[1]));
                    $match->setYellowGoalPlayer(new PlayerModel($matchPlayers[2]));
                    $match->setYellowOffensePlayer(new PlayerModel($matchPlayers[3]));
                }

                $match->save($db);

                $logger->info('added match', [$match->getMatchEntity()->getId(), $date->format(DateTime::W3C), $match->getScoreRed(), $match->getScoreYellow()]);
            }
        }

        if ($archive->isSeasonStorable($m, $y)) {
            $archive->storeSeason($m, $y);

            $logger->info('stored season', [$m, $y]);
        } else {
            $logger->info('skipped unstorable season', [$m, $y]);
        }
    }
}

$logger->info('done!');
