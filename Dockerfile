FROM php:7.3-fpm-alpine3.8

ENV TIMEZONE=UTC \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8 \
    PHP_MAX_FILE_SIZE=2M

RUN set -xe \
    && apk add --no-cache icu-dev \
    && docker-php-ext-install pdo pdo_mysql intl opcache

RUN set -xe \
    && apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    && pecl install apcu \
    && docker-php-ext-enable apcu \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/*

COPY --chown=www-data:www-data . /packgyver/
COPY .docker/php/fpm.conf /usr/local/etc/php-fpm.d/zz-docker.conf

WORKDIR /packgyver

USER www-data
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer install --no-dev --prefer-dist --optimize-autoloader --no-progress --no-suggest
USER root
RUN rm /usr/bin/composer
