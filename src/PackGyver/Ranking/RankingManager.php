<?php

/**
 * PackGyver - Ranking Model
 *
 * @package ranking
 */
class RankingManager {

	/**
	 *
	 * @var PlayerCollection
	 */
	private $players = null;

	/**
	 * CTOR
	 * 
	 * @param \PlayerCollection $players
	 * @param int $type
	 */
	public function __construct(\PlayerCollection $players, $type = PlayerStatistics::CACHETYPE_SEASON) {
		$this->players = $players;
		$this->sortByPoints($type);
	}

	/**
	 * get sorted player ranking
	 *
	 * @return \PlayerCollection
	 */
	public function getPlayerCollection() {
		return $this->players;
	}

	/**
	 *
	 * @param int $playerId
	 * @return int
	 */
	public function getRankForPlayerId($playerId) {
		$rank = 1;

		foreach ($this->getPlayerCollection() as $player) {
			/* @var $player \PlayerModel */
			if ($player->getId() == $playerId) {
				return $rank;
			}

			$rank++;
		}

		return 0;
	}

	/**
	 *
	 * @param int $rank
	 * @return int
	 */
	public function getPlayerForRank($rank) {
		if (intval($rank) <= $this->getPlayerCollection()->count()
				&& $this->getPlayerCollection()->getPlayers()->offsetExists($rank - 1)) {

			return $this->getPlayerCollection()->getPlayers()->offsetGet($rank - 1);
		}

		return null;
	}

	/**
	 * @param PlayerModel $player
	 * @return RankingSequence
	 */
	public function getRankingSequence(PlayerModel $player) {
		$sequence = new RankingSequence();

		return $sequence->setPlayer($player)
						->setRank($this->getRankForPlayerId($player->getId()))
						->setFrontOffset(1)
						->setBackOffset(1)
						->setPlayers($this->getPlayerCollection());
	}

	/**
	 * 
	 * @param int $type
	 */
	public function sortByPoints($type) {
		$players = $this->getPlayerCollection()->getPlayers();

		if ($type === PlayerStatistics::CACHETYPE_SEASON) {
			$players->uasort(array($this, '_sortBySeasonPoints'));
		} else {
			$players->uasort(array($this, '_sortByAlltimePoints'));
		}

		$this->getPlayerCollection()->setPlayers(array_values(iterator_to_array($players)));
	}

	/**
	 * 
	 * @param PlayerModel $a
	 * @param PlayerModel $b
	 * @return bool
	 */
	public function _sortBySeasonPoints(PlayerModel $a, PlayerModel $b) {
		return $a->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints() < $b->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints();
	}

	/**
	 * 
	 * @param PlayerModel $a
	 * @param PlayerModel $b
	 * @return bool
	 */
	public function _sortByAlltimePoints(PlayerModel $a, PlayerModel $b) {
		return $a->getStats(PlayerStatistics::CACHETYPE_ALLTIME)->getPoints() < $b->getStats(PlayerStatistics::CACHETYPE_ALLTIME)->getPoints();
	}

}
