<?php

/**
 * PackGyver - Ranking Sequence
 * 
 * @package ranking
 */
class RankingSequence {

	/**
	 *
	 * @var int
	 */
	private $frontOffset = 1;

	/**
	 *
	 * @var int
	 */
	private $backOffset = 1;

	/**
	 *
	 * @var \PlayerModel
	 */
	private $player;

	/**
	 *
	 * @var int
	 */
	private $rank;

	/**
	 *
	 * @var \PlayerModel[]
	 */
	private $players;

	/**
	 * 
	 * @return int
	 */
	public function getFrontOffset() {
		return $this->frontOffset;
	}

	/**
	 * 
	 * @param int $frontOffset
	 * @return \RankingSequence
	 */
	public function setFrontOffset($frontOffset) {
		$this->frontOffset = $frontOffset;

		return $this;
	}

	/**
	 * Increase the front offset
	 */
	protected function increaseFrontOffset() {
		$this->frontOffset++;
	}

	/**
	 * 
	 * @return int
	 */
	public function getBackOffset() {
		return $this->backOffset;
	}

	/**
	 * 
	 * @param int $backOffset
	 * @return \RankingSequence
	 */
	public function setBackOffset($backOffset) {
		$this->backOffset = $backOffset;

		return $this;
	}

	/**
	 * Increase the back offset
	 */
	protected function increaseBackOffset() {
		$this->backOffset++;
	}

	/**
	 * 
	 * @return \PlayerModel
	 */
	public function getPlayer() {
		return $this->player;
	}

	/**
	 * 
	 * @param PlayerModel $player
	 * @return \RankingSequence
	 */
	public function setPlayer(PlayerModel $player) {
		$this->player = $player;

		return $this;
	}

	/**
	 * 
	 * @return int
	 */
	public function getRank() {
		return $this->rank;
	}

	/**
	 * @param int $rank
	 * @return $this
	 */
	public function setRank($rank) {
		$this->rank = $rank;

		return $this;
	}

	/**
	 * 
	 * @return \PlayerCollection
	 */
	public function getPlayers() {
		return $this->players;
	}

	/**
	 * 
	 * @param PlayerCollection $players
	 * @return \RankingSequence
	 */
	public function setPlayers(PlayerCollection $players) {
		$this->players = $players;

		return $this;
	}

	/**
	 * 
	 * @return array
	 */
	public function toHash() {
		if ($this->getRank() === 1) {
			$this->increaseBackOffset();
		} else if ($this->getRank() === $this->getPlayers()->count()) {
			$this->increaseFrontOffset();
		}

		$rankKey = ($this->getRank() - 1);
		$frontUntil = $rankKey - $this->getFrontOffset();
		$backUntil = $rankKey + $this->getBackOffset();
		$playerHashes = array();

		for ($i = ($rankKey - 1); $i >= $frontUntil; $i--) {
			if ($this->getPlayers()->getPlayers()->offsetExists($i)) {
				$player = $this->getPlayers()->getPlayers()->offsetGet($i);
				$playerHash = $player->toHash(true);
				$playerHash['stats']['season']['rank'] = ($i + 1);

				if ($i === ($rankKey - 1)) {
					$playerHash['tsPoints'] = $player->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints() - $this->getPlayer()->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints();
					$playerHash['tsPointsState'] = '+';
				} else {
					$playerHash['tsPoints'] = $player->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints() - $this->getPlayer()->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints();
					$playerHash['tsPointsState'] = '+';
				}

				$playerHashes[] = $playerHash;
			}
		}

		$playerHashes = array_reverse($playerHashes);

		$me = $this->getPlayer()->toHash(true);
		$me['stats']['season']['rank'] = ($rankKey + 1);
		$me['stats']['season']['rank'] = ($rankKey + 1);
		$me['tsPoints'] = $this->getPlayer()->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints();
		$me['tsPointsState'] = '';
		$playerHashes[] = $me;

		for ($ii = ($rankKey + 1); $ii <= $backUntil; $ii++) {
			if ($this->getPlayers()->getPlayers()->offsetExists($ii)) {
				$player = $this->getPlayers()->getPlayers()->offsetGet($ii);
				$playerHash = $player->toHash(true);
				$playerHash['stats']['season']['rank'] = ($ii + 1);

				if ($ii === ($rankKey + 1)) {
					$playerHash['tsPoints'] = $this->getPlayer()->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints() - $player->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints();
					$playerHash['tsPointsState'] = '-';
				} else {
					$playerHash['tsPoints'] = $this->getPlayer()->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints() - $player->getStats(PlayerStatistics::CACHETYPE_SEASON)->getPoints();
					$playerHash['tsPointsState'] = '-';
				}

				$playerHashes[] = $playerHash;
			}
		}

		return $playerHashes;
	}

}
