<?php

/**
 * PackGyver - Award Manager
 */
class AwardManager {
	//Key => Streak::Win::10 for Winning Streak >= 10  

	const KEY_AWARD_STREAK_WIN = "Streak::Win::";

	/**
	 *
	 * @return string
	 */
	public static function getAwardLogoPath() {
		$path = PG::getUploadDir() . DS . 'awards';
		if (!is_dir($path)) {
			mkdir($path, 0755, true);
		}
		return $path;
	}

	/**
	 *
	 * @return string
	 */
	public static function getAwardLogoUri() {
		return '/static/upload/awards/';
	}

	/**
	 * add award
	 *
	 * @param PDO $db
	 * @param string $name
	 * @param string $description
	 * @return AwardEntity
	 */
	public static function addAward(PDO $db, $name, $description = null, $key = null, $logo = 'logo') {
		if (strlen($name) > 0) {
			$entity = new AwardEntity();
			$entity->setName($name);
			$entity->setDescription($description);
			$entity->setKey($key);
			$entity->insertIntoDatabase($db);

			if (isset($_FILES[$logo])) {
				$entity->setLogo(self::handleLogoUpload($entity->getId(), $logo))->updateToDatabase($db);
			}

			return $entity;
		}

		return null;
	}

	/**
	 *
	 * @param int $awardId
	 * @param string $logo
	 * @return string
	 */
	public static function handleLogoUpload($awardId, $logo = 'logo') {
		if (isset($_FILES[$logo])) {
			$ext = FileUtil::getFileExtension($_FILES[$logo]['name']);

			if (in_array($ext, array('jpg', 'png'))) {
				$filename = 'award_' . $awardId . '.' . $ext;

				if (move_uploaded_file($_FILES[$logo]['tmp_name'], self::getAwardLogoPath() . DS . $filename)) {
					return self::getAwardLogoUri() . $filename;
				}
			}
		}
		return null;
	}

	/**
	 * delete award
	 *
	 * @param PDO $db
	 * @param int $awardId
	 */
	public static function deleteAwardById(PDO $db, $awardId) {
		$entity = AwardEntity::findById($db, intval($awardId));
		if ($entity instanceof AwardEntity && count($entity->fetchPlayerAwardEntityCollection($db)) < 1) {
			$entity->deleteFromDatabase($db);
		}
	}

	/**
	 * add award to player
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param AwardEntity $award
	 * @param string $comment
	 * @param int $date
	 * @param bool $sendNotification
	 */
	public static function addPlayerAward(PDO $db, PlayerEntity $player, AwardEntity $award, $comment = null, $date = null, $sendNotification = false) {
		//if (!self::hasPlayerAward($db, $player, $award)) {
		if (null === $date) {
			$date = time();
		}

		$entity = new PlayerAwardEntity();
		$entity->setPlayerId($player->getId());
		$entity->setAwardId($award->getId());
		$entity->setComment($comment);
		$entity->setDate(DateUtil::formatForDB($date));
		$entity->insertIntoDatabase($db);

		if (true === $sendNotification) {
			self::sendAwardGivenNotificationToPlayer(new PlayerModel($player), $award);
		}

		//}
	}

	/**
	 * remove award from player
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param AwardEntity $award
	 */
	public static function deletePlayerAward(PDO $db, PlayerEntity $player, AwardEntity $award) {
		$filter[] = new DFC(PlayerAwardEntity::FIELD_AWARDID, $award->getId(), DFC::EXACT);
		$filter[] = new DFC(PlayerAwardEntity::FIELD_PLAYERID, $player->getId());
		$entity = PlayerAwardEntity::findByFilter($db, $filter);

		if ($entity[0] instanceof PlayerAwardEntity) {
			$entity[0]->deleteFromDatabase($db);
		}
	}

	/**
	 * check if player has an award already
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param AwardEntity $award
	 * @return bool
	 */
	public static function hasPlayerAward(PDO $db, PlayerEntity $player, AwardEntity $award) {
		$filter[] = new DFC(PlayerAwardEntity::FIELD_AWARDID, $award->getId(), DFC::EXACT);
		$filter[] = new DFC(PlayerAwardEntity::FIELD_PLAYERID, $player->getId());
		$entity = PlayerAwardEntity::findByFilter($db, $filter);

		return !empty($entity);
	}

	/**
	 *
	 * @param PDO $db
	 * @return PlayerAwardEntity[]
	 */
	public static function getAwards(PDO $db) {
		$sort = array(
			new DSC(AwardEntity::FIELD_NAME, DSC::ASC)
		);
		return AwardEntity::findByFilter($db, array(), true, $sort);
	}

	/**
	 *
	 * @param PDO $db
	 * @return array
	 */
	public static function getAwardHashes(PDO $db) {
		$awards = self::getAwards($db);

		$awardHash = array();
		foreach ($awards as $award) {
			if ($award instanceof AwardEntity) {
				$awardHash[] = self::getHashForAward($award);
			}
		}
		return $awardHash;
	}

	/**
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param bool $asHash
	 * @return AwardEntity or array if $asHash = true
	 */
	public static function getAwardsForPlayer(PDO $db, PlayerEntity $player, $asHash = false) {
		$awards = array();
		$filter[] = new DFC(PlayerAwardEntity::FIELD_PLAYERID, $player->getId(), DFC::EXACT);
		$playerAwards = PlayerAwardEntity::findByFilter($db, $filter);

		if (!empty($playerAwards)) {
			$aC = 0;
			foreach ($playerAwards as $playerAward) {
				$tmpAward = null;
				if ($playerAward instanceof PlayerAwardEntity) {
					$tmpAward = $playerAward->fetchAwardEntity($db);
					if ($tmpAward instanceof AwardEntity) {
						if (true === $asHash) {
							$awards[$aC] = self::getHashForAward($tmpAward);
							$awards[$aC]['playerAward'] = self::getHashForPlayerAward($playerAward);
						} else {
							$awards[$aC] = $tmpAward;
						}
						$aC++;
					}
				}
			}
		}

		return $awards;
	}

	/**
	 * get PlayerAwardEntity for player and award
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param AwardEntity $award
	 * @return PlayerAwardEntity
	 */
	public static function getPlayerAward(PDO $db, PlayerEntity $player, AwardEntity $award) {
		$filter[] = new DFC(PlayerAwardEntity::FIELD_PLAYERID, $player->getId(), DFC::EXACT);
		$filter[] = new DFC(PlayerAwardEntity::FIELD_AWARDID, $award->getId(), DFC::EXACT);
		$playerAward = PlayerAwardEntity::findByFilter($db, $filter);

		if (!empty($playerAward) && $playerAward[0] instanceof PlayerAwardEntity) {
			return $playerAward[0];
		}
		return null;
	}

	/**
	 * get PlayerAwardEntity for player and award
	 *
	 * @param PDO $db
	 * @param AwardEntity $award
	 * @param bool $grouped
	 * @return array
	 */
	public static function getPlayerHashesForAward(PDO $db, AwardEntity $award, $grouped = true) {
		$filter[] = new DFC(PlayerAwardEntity::FIELD_AWARDID, $award->getId(), DFC::EXACT);
		$playerAwards = PlayerAwardEntity::findByFilter($db, $filter);
		$playerAwardHashes = array();

		if (!empty($playerAwards)) {
			foreach ($playerAwards as $pKey => $playerAward) {
				if ($playerAward instanceof PlayerAwardEntity) {
					$player = PlayerEntity::findById($db, $playerAward->getPlayerId());
					if ($player instanceof PlayerEntity) {
						$playerModel = new PlayerModel($player);

						if (true === $grouped) {
							$playerAwardHashes[$player->getId()]['player'] = $playerModel->toHash();
							$playerAwardHashes[$player->getId()]['awards'][$pKey] = self::getHashForPlayerAward($playerAward);
						} else {
							$playerAwardHashes[$playerAward->getId()]['playerHash'] = $playerModel->toHash();
							$playerAwardHashes[$playerAward->getId()]['playerAward'] = self::getHashForPlayerAward($playerAward);
						}
					}
				}
			}
		}

		return $playerAwardHashes;
	}

	/**
	 *
	 * @param AwardEntity $award
	 * @return array
	 */
	public static function getHashForAward(AwardEntity $award) {
		return array(
			'id' => $award->getId(),
			'name' => $award->getName(),
			'description' => $award->getDescription(),
			'logo' => $award->getLogo(),
			'key' => $award->getKey()
		);
	}

	/**
	 *
	 * @param PlayerAwardEntity $playerAward
	 * @return array
	 */
	public static function getHashForPlayerAward(PlayerAwardEntity $playerAward) {
		return array(
			'id' => $playerAward->getId(),
			'comment' => $playerAward->getComment(),
			'playerId' => $playerAward->getPlayerId(),
			'awardId' => $playerAward->getAwardId()
		);
	}

	/**
	 * get awards by key
	 * 
	 * @param PDO $db
	 * @param string $key
	 * @return array
	 */
	public static function getAwardsByKey(PDO $db, $key) {
		$filter = array(
			new DFC(AwardEntity::FIELD_KEY, $key, DFC::BEGINS_WITH)
		);
		return AwardEntity::findByFilter($db, $filter);
	}

	/**
	 * has player award in season
	 * 
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param AwardEntity $award
	 * @param array $seasonDates
	 * @return bool
	 */
	public static function hasPlayerAwardInSeason(PDO $db, PlayerEntity $player, AwardEntity $award, $seasonDates = null) {
		if (null === $seasonDates) {
			$seasonDates = SeasonManager::getCurrentSeasonDates();
		}
		$filter = array(
			new DFC(PlayerAwardEntity::FIELD_DATE, $seasonDates['start'], DFC::GREATER),
			new DFC(PlayerAwardEntity::FIELD_DATE, $seasonDates['end'], DFC::SMALLER),
			new DFC(PlayerAwardEntity::FIELD_PLAYERID, $player->getId(), DFC::EXACT),
			new DFC(PlayerAwardEntity::FIELD_AWARDID, $award->getId(), DFC::EXACT)
		);
		$result = PlayerAwardEntity::findByFilter($db, $filter);
		return !empty($result);
	}

	/**
	 *
	 * @param PlayerModel $player
	 * @param AwardEntity $award
	 * @return \Notification
	 */
	public static function sendAwardGivenNotificationToPlayer(PlayerModel $player, AwardEntity $award) {
		$nMessage = DwooProxy::getInstance()->parseTemplate('award/notificationAward', array(
			'award' => AwardManager::getHashForAward($award)
				));

		$notification = new Notification();
		$notification->setPlayer($player)
				->setTitle('Neuer Award!')
				->setMessage($nMessage)
				->setDateSent(DateUtil::formatForDB());
		$notification->save();

		/* notify */
		$wsservice = WebSocketServiceManager::getInstance();

		if ($wsservice->isEnabled()) {
			$wsservice->getDataDispatcher()->triggerNotificationReceived($player->getEntity(), $notification);
		}

		return $notification;
	}

}
