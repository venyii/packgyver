<?php

/**
 * PackGyver - Award Detector
 *
 * @package award
 */
class AwardDetector {

	/**
	 *
	 * @var \SeasonArchive
	 */
	private $archive;

	/**
	 *
	 * @var int
	 */
	private $month;

	/**
	 *
	 * @var int
	 */
	private $year;

	/**
	 *
	 * @param \SeasonArchive $archive
	 */
	public function setArchive(\SeasonArchive $archive) {
		$this->archive = $archive;
	}

	/**
	 *
	 * @param int $month
	 */
	public function setMonth($month) {
		$this->month = $month;
	}

	/**
	 *
	 * @param int $year
	 */
	public function setYear($year) {
		$this->year = $year;
	}

	/**
	 *
	 * @return \SeasonArchive
	 */
	public function getArchive() {
		return $this->archive;
	}

	/**
	 *
	 * @return int
	 */
	public function getMonth() {
		return $this->month;
	}

	/**
	 *
	 * @return int
	 */
	public function getYear() {
		return $this->year;
	}

	/**
	 *
	 * @param int $month
	 * @param int $year
	 */
	public function __construct($month, $year) {
		$this->month = $month;
		$this->year = $year;
		$this->archive = new SeasonArchive();
	}

	/**
	 *
	 * @return bool
	 */
	public function isValid() {
		$archive = new SeasonArchive();
		$curDates = SeasonManager::getCurrentSeasonDatesSimple();

		return ($this->getMonth() == $curDates['month'] && $this->getYear() == $curDates['year'])
				|| $archive->isSeasonStored($this->getMonth(), $this->getYear());
	}

	/**
	 *
	 * @return array
	 * @throws \PackGyverException 
	 */
	private function getPlayers() {
		$archive = new SeasonArchive();
		$curDates = SeasonManager::getCurrentSeasonDatesSimple();
		$players = array();

		if ($this->getMonth() == $curDates['month'] && $this->getYear() == $curDates['year']) {
			$ranking = new RankingManager(PlayerCollection::create());
			$players = $ranking->getPlayerCollection();
		} else if ($archive->isSeasonStored($this->getMonth(), $this->getYear())) {
			$players = $archive->getSeason($this->getMonth(), $this->getYear())->getPlayerCollection();
		}

		return $players;
	}

	/**
	 *
	 * @return array
	 */
	public function getMostMonsterPacksPlayer() {
		$hash = array();
		$players = $this->getPlayers($this->getMonth(), $this->getYear());

		foreach ($players as $player) {
			$playerHash = $player->toHash(true);

			if ($playerHash['stats']['season']['monsterPackCount'] > 0) {
				$hash[] = array(
					'player' => $playerHash,
					'monsterPackCount' => $playerHash['stats']['season']['monsterPackCount']
				);
			}
		}

		if (count($hash) > 0) {
			$sort = new SortUtil();
			$hash = array_values($sort->sort($hash, 'monsterPackCount'));
		}

		$topPlayers = array();
		$lastTop = null;

		foreach ($hash as $h) {
			if (null === $lastTop) {
				$lastTop = $h['monsterPackCount'];
				$topPlayers[] = $h;
				continue;
			}

			if ($lastTop === $h['monsterPackCount']) {
				$topPlayers[] = $h;
			} else {
				break;
			}
		}

		return $topPlayers;
	}

	/**
	 *
	 * @return array
	 */
	public function getRanking() {
		$ranking = new RankingManager($this->getPlayers());

		if ($ranking->getPlayerCollection()->count() >= 3) {
			$playerHash = array(
				$ranking->getPlayerForRank(1)->toHash(true),
				$ranking->getPlayerForRank(2)->toHash(true),
				$ranking->getPlayerForRank(3)->toHash(true)
			);

			return array_splice($playerHash, 0, 3);
		}

		return array();
	}

}

?>