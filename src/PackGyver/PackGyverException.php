<?php

/**
 * PackGyver - Exception
 */
class PackGyverException extends Exception {

	public static function logException(Throwable $e) {
		Logger::get()->error($e->getMessage(), array('exception' => $e));

		if (PG::isDevMode()) {
			throw $e;
		}
	}

}
