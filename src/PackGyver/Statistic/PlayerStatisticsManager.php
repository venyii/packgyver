<?php

/**
 * PackGyver - Player Statistics
 */
class PlayerStatisticsManager {

	const TYPE_SEASON = 0;
	const TYPE_ALLTIME = 1;

	/**
	 *
	 * @var string
	 */
	private static $cacheDir = 'players';

	/**
	 *
	 * @var array
	 */
	private static $validTypes = array(
		self::TYPE_SEASON => 'season',
		self::TYPE_ALLTIME => 'alltime'
	);

	/**
	 *
	 * @param int $type
	 * @return bool
	 */
	public static function isValidCacheType($type) {
		return isset(self::$validTypes[$type]);
	}

	/**
	 *
	 * @param PlayerEntity $player
	 * @param int $type
	 * @return string
	 */
	public static function getPlayerCachePathForType(PlayerEntity $player, $type) {
		return self::getPlayerCacheDirForType($type) . self::getPlayerCacheFileForType($player, $type);
	}

	/**
	 *
	 * @return string
	 * @throws PackGyverException
	 */
	private static function getPlayerCacheDirForType($type) {
		if (self::isValidCacheType($type)) {
			return StatisticsManager::getStatsCacheDir() . DS . self::$cacheDir . DS . self::$validTypes[$type] . DS;
		} else {
			throw new PackGyverException('invalid player cache type: ' . $type);
		}
	}

	/**
	 * clear either all player stats or only specific type cache
	 *
	 * @param int $type
	 */
	public static function clearPlayerStatsCache($type = null) {
		if (null !== $type && self::isValidCacheType($type)) {
			$files = new DirectoryIterator(self::getPlayerCacheDirForType($type));
			foreach ($files as $file) {
				if (!$file->isDot()) {
					unlink($file->getRealPath());
				}
			}
		} else {
			foreach (self::$validTypes as $type => $cacheType) {
				$files = new DirectoryIterator(self::getPlayerCacheDirForType($type));
				foreach ($files as $file) {
					if (!$file->isDot()) {
						unlink($file->getRealPath());
					}
				}
			}
		}
	}

	/**
	 * get all player wins
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param int $month
	 * @param int $year
	 * @return int
	 */
	public static function getAllMatchesPlayerWinCount(PDO $db, PlayerEntity $player, $month = null, $year = null) {
		$sql = "SELECT count(M.id) from `match` AS M
                INNER JOIN `playerMatch` PM ON M.id = PM.matchId
                WHERE ((PM.team = 0
                AND M.resultTeamRed = 10
                AND PM.playerId = " . $db->quote($player->getId()) . ")
                OR (PM.team = 1
                AND M.resultTeamYellow = 10
                AND PM.playerId = " . $db->quote($player->getId()) . "))";

		if (null !== $month) {
			$sql .= " AND MONTH(M.dateCreated)= " . (int) $month;
		}

		if (null !== $year) {
			$sql .= " AND YEAR(M.dateCreated)= " . (int) $year;
		}

		return (int) $db->query($sql)->fetchColumn();
	}

}
