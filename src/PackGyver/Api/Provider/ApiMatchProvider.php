<?php

/**
 * API - Match Result Provider
 * 
 * <capabilities>
 * /match/changeResult	- Modify match result
 * /match/open		- Get unsaved matches
 * </capabilities>
 *
 * @package api
 */
class ApiMatchProvider extends ApiProviderAbstract {

	/**
	 *
	 * @return array
	 */
	public function changeResult() {
		$result = array(
			'success' => false
		);

		if (isset($_POST['matchId']) && is_numeric($_POST['matchId'])) {
			$db = PG::getDB();
			$matchEntity = MatchEntity::findById($db, intval($_POST['matchId']));

			if ($matchEntity instanceof MatchEntity) {
				if (MatchModel::hasPermission($matchEntity) && MatchManager::isMatchCurrentMatch($db, $matchEntity)) {
					$team = ($_POST['team'] == 'red') ? 'red' : 'yellow';
					$type = ($_POST['type'] == '+') ? '+' : '-';
					$score = 0;

					$valid = false;

					if ($type == '+') {
						if ($team == 'red') {
							$valid = ($matchEntity->getResultTeamRed() < 10);
						} else {
							$valid = ($matchEntity->getResultTeamYellow() < 10);
						}
					} else {
						if ($team == 'red') {
							$valid = ($matchEntity->getResultTeamRed() > 0);
						} else {
							$valid = ($matchEntity->getResultTeamYellow() > 0);
						}
					}

					if (true === $valid) {
						if ($team == 'red') {
							if ($type == '+') {
								$matchEntity->setResultTeamRed($matchEntity->getResultTeamRed() + 1);
							} else {
								$matchEntity->setResultTeamRed($matchEntity->getResultTeamRed() - 1);
							}
							$score = $matchEntity->getResultTeamRed();
						} else {
							if ($type == '+') {
								$matchEntity->setResultTeamYellow($matchEntity->getResultTeamYellow() + 1);
							} else {
								$matchEntity->setResultTeamYellow($matchEntity->getResultTeamYellow() - 1);
							}
							$score = $matchEntity->getResultTeamYellow();
						}

						if ($score == 10) {
							$matchEntity->setDateSaved(DateUtil::formatForDB());
						}

						$matchEntity->updateToDatabase($db);
						$result['success'] = true;

						if ($score == 10) {
							$matchModel = new MatchModel($matchEntity);
							foreach ($matchModel->getPlayers() as $matchPlayer) {
								/* @var $matchPlayer PlayerModel */
								$matchPlayer->getStats(PlayerStatistics::CACHETYPE_SEASON)->recalculate();
							}

							$matchModel->attach(new WinStreakObserver());
							$matchModel->notify();
						}

						$wsservice = WebSocketServiceManager::getInstance();

						if ($wsservice->isEnabled()) {
							$wsservice->getDataDispatcher()->triggerMatchScoreEvent(AuthenticationManager::getInstance()->getPlayer(), $matchEntity, $team, $score);
						}
					}
				}
			}
		}

		return $result;
	}

	/**
	 *
	 * @return array
	 */
	public function open() {
		echo "<pre>";
		var_dump('@fix');
		exit;
		$db = PG::getDB();

		$playerId = (isset($_POST['playerId'])) ? intval($_POST['playerId']) : null;
		$month = (isset($_POST['month'])) ? intval($_POST['month']) : null;
		$year = (isset($_POST['year'])) ? intval($_POST['year']) : null;
		$limit = (isset($_POST['limit'])) ? intval($_POST['limit']) : null;
		$unsaved = null;

		if (isset($_POST['unsaved'])) {
			$unsaved = (true == $_POST['unsaved']);
		}

		$player = PlayerEntity::findById($db, 2);

		if ($player instanceof PlayerEntity) {
			if (null !== $playerId) {
				$matches = MatchManager::getPlayerMatches($db, $player, $month, $year, $limit, $unsaved, false);
			} else {
				$matches = MatchManager::getMatches(false, $unsaved, $month, $year, $matches);
			}

			echo "<pre>";
			var_dump($matches);
			exit;

			$dummy = array(1, 2, 3);

			foreach ($dummy as $match) {
				$matchNode = $dom->createElement('OpenMatch');
				$matchNode->appendChild($dom->createElement('id', rand(100, 1337)));
				$matchNode->appendChild($dom->createElement('date', DateUtil::format()));
				$matchNode->appendChild($dom->createElement('team', 'Martin'));

				$enemies = $dom->createElement('enemies');
				$enemyDummies = array('Gideon', 'Johannes');

				foreach ($enemyDummies as $enemy) {
					$enemyNode = $dom->createElement('enemy');
					$enemyNode->appendChild($dom->createElement('name', $enemy));
					$enemyNode->appendChild($dom->createElement('position', 'goal'));
					$enemyNode->appendChild($dom->createElement('team', 'red'));
					$enemies->appendChild($enemyNode);
				}

				$matchNode->appendChild($enemies);
				$rootNode->appendChild($matchNode);
			}

			$dom->appendChild($rootNode);
		}

		return array('df' => 'sdfs');
	}

}

?>