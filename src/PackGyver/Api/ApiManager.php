<?php

/**
 * API - Manager
 *
 * @package api
 */
class ApiManager {

	const USER_AGENT = 'PackGyverApi/0.1';
	const API_RESPONSE_ROOT = 'PackGyverApiResponse';
	const API_RESPONSE_ERROR = 'PackGyverApiError';
	const DEFAULT_API_METHOD = 'index';

	/**
	 *
	 * @var ApiAccess
	 */
	private $apiAccess;

	/**
	 *
	 * @return ApiAccess
	 */
	public function getApiAccess() {
		return $this->apiAccess;
	}

	/**
	 *
	 * @param ApiAccess $apiAccess
	 */
	public function __construct(ApiAccess $apiAccess) {
		$this->apiAccess = $apiAccess;
	}

	/**
	 *
	 * @param string $function
	 * @param array $parameters
	 */
	public function handle($function, $parameters) {
		$provider = ApiProviderAbstract::getProviderByMapping($function);

		if ($provider instanceof ApiProvider) {
			$valid = false;
			$response = null;
			$apiProvider = ($provider->getProvider());

			if (isset($parameters[0]) && in_array($parameters[0], $provider->getCapabilities())) {
				if (is_callable(array($apiProvider, $parameters[0]))) {
					$response = call_user_func(array($apiProvider, $parameters[0]));
					$valid = true;
				}
			}

			if (false === $valid) {
				if (is_callable(array($apiProvider, self::DEFAULT_API_METHOD))) {
					$response = call_user_func(array($apiProvider, self::DEFAULT_API_METHOD));
					$valid = true;
				}
			}

			if (true === $valid) {
				return $response;
			}
		}

		throw new ApiException('Invalid mapping!');
	}

	/**
	 *
	 * @return bool
	 */
	public function isValid() {
		return true === $this->getApiAccess()->isValid(); // && $_SERVER['HTTP_USER_AGENT'] == self::USER_AGENT;
	}

	/**
	 *
	 * @param array $result
	 * @param bool $error
	 * @return string
	 */
	public function render($result, $error) {
		$output = array();

		if (true === $error) {
			$output[self::API_RESPONSE_ERROR] = $result;
		} else {
			$output[self::API_RESPONSE_ROOT] = $result;
		}

		return json_encode($output);
	}

	/**
	 *
	 * @param ApiException $exc
	 * @return string
	 */
	public function dieCrying(Exception $exc) {
		$error = array();
		//$error['type'] = get_class($exc);
		$error['message'] = $exc->getMessage();
		return $error;
	}

}

?>