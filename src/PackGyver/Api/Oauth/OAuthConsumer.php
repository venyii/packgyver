<?php

/**
 * OAuth - Consumer
 *
 * @package oauth
 * @copyright Andy Smith - 2007
 * @see https://code.google.com/p/oauth/
 */
class OAuthConsumer {

	public $key;
	public $secret;

	function __construct($key, $secret, $callback_url = NULL) {
		$this->key = $key;
		$this->secret = $secret;
		$this->callback_url = $callback_url;
	}

	function __toString() {
		return "OAuthConsumer[key=$this->key,secret=$this->secret]";
	}

}

?>