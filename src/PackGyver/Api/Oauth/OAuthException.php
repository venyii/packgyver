<?php

/**
 * OAuth - Exception
 *
 * @package oauth
 * @copyright Andy Smith - 2007
 * @see https://code.google.com/p/oauth/
 */
class OAuthException extends Exception {
	
}

?>