<?php

/**
 * PackGyver - Players Model
 */
class PlayerModel {

	/**
	 *
	 * @var PlayerEntity
	 */
	private $entity;

	/**
	 *
	 * @var PlayerStatistics
	 */
	private $seasonStats;

	/**
	 *
	 * @var PlayerStatistics
	 */
	private $alltimeStats;

	/**
	 *
	 * @var NotificationCollection
	 */
	private $notifications;

	/**
	 *
	 * @var array
	 */
	private static $validAvatarExt = array('jpg', 'png');

	/**
	 *
	 * @return PlayerEntity
	 */
	public function getEntity() {
		return $this->entity;
	}

	/**
	 *
	 * @return PlayerStatistics
	 */
	private function getSeasonStats() {
		if (null === $this->seasonStats) {
			$this->seasonStats = new PlayerStatistics($this, PlayerStatistics::CACHETYPE_SEASON);
            $this->seasonStats->load();
		}

		return $this->seasonStats;
	}

	/**
	 *
	 * @return PlayerStatistics
	 */
	private function getAlltimeStats() {
		if (null === $this->alltimeStats) {
			$this->alltimeStats = new PlayerStatistics($this, PlayerStatistics::CACHETYPE_ALLTIME);
            $this->alltimeStats->load();
		}

		return $this->alltimeStats;
	}

	/**
	 *
	 * @return int
	 */
	public function getId() {
		return $this->getEntity()->getId();
	}

	/**
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->getEntity()->getEmail();
	}

	/**
	 *
	 * @return string
	 */
	public function getGtalk() {
		return $this->getEntity()->getGtalk();
	}

	/**
	 *
	 * @return string
	 */
	public function getFirstname() {
		return $this->getEntity()->getFirstname();
	}

	/**
	 *
	 * @return string
	 */
	public function getLastname() {
		return $this->getEntity()->getLastname();
	}

	/**
	 *
	 * @return string
	 */
	public function getDateRegistered() {
		return $this->getEntity()->getDateRegistered();
	}

	/**
	 * e.g. 'M. Mustermann'
	 *
	 * @return string
	 */
	public function getNameShort() {
		return substr($this->getFirstname(), 0, 1) . '. ' . $this->getLastname();
	}

	/**
	 * 
	 * @return string
	 */
	public function getFirstnamePersonal() {
		if (StringUtil::endsWith('s', $this->getFirstname()) || StringUtil::endsWith('x', $this->getFirstname())) {
			$title = '%s\'';
		} else {
			$title = '%s\'s';
		}

		return sprintf($title, $this->getFirstname());
	}

	/**
	 * CTOR
	 *
	 * @param PlayerEntity $entity
	 */
	public function __construct(PlayerEntity $entity) {
		$this->entity = $entity;
	}

	/**
	 * check if user is logged in
	 *
	 * @return bool
	 */
	public function isLoggedIn() {
		return AuthenticationManager::getInstance()->isLoggedIn();
	}

	/**
	 *
	 * @return PlayerEntity
	 */
	public function getLoggedInPlayer() {
		return AuthenticationManager::getInstance()->getPlayer();
	}

	/**
	 *
	 * @return string
	 */
	public function getUrlIdentifier() {
		return substr($this->getEmail(), 0, strpos($this->getEmail(), '@'));
	}

	/**
	 *
	 * @return bool
	 */
	public function isCheckedIn() {
		return 1 === intval($this->getEntity()->getCheckedIn());
	}

	/**
	 *
	 * @return bool
	 */
	public function isActive() {
		return 1 === intval($this->getEntity()->getActive());
	}

	/**
	 *
	 * @return bool
	 */
	public function isDeleted() {
		return 1 === intval($this->getEntity()->getDeleted());
	}

	/**
	 * check if the player is checked in, active and not deleted
	 *
	 * @return bool
	 */
	public function isValidToPlay() {
		if (false === $this->isActive()
				|| true === $this->isDeleted()
				|| false === $this->isCheckedIn()
		) {
			return false;
		}

		return true;
	}

	/**
	 *
	 * @param int $type
	 * @return PlayerStatistics
	 */
	public function getStats($type) {
		if ($type === PlayerStatistics::CACHETYPE_SEASON) {
			return $this->getSeasonStats();
		} else {
			return $this->getAlltimeStats();
		}
	}

	/**
	 *
	 * @param PlayerStatistics $stats
	 * @param int $type
	 * @return array
	 */
	public function setStats(PlayerStatistics $stats, $type) {
		if ($type === PlayerStatistics::CACHETYPE_SEASON) {
			return $this->seasonStats = $stats;
		} else {
			return $this->alltimeStats = $stats;
		}
	}

	/**
	 *
	 * @todo implement (maybe)
	 */
	public function calculateStrength() {
		
	}

	/**
	 *
	 * @return NotificationCollection
	 */
	public function getNotifications() {
		if (null === $this->notifications) {
			$this->notifications = new NotificationCollection($this);
			$this->notifications->getCollectionForPlayer();
		}

		return $this->notifications;
	}

	/**
	 *
	 * @param bool $withStats
	 * @return array
	 */
	public function toHash($withStats = false) {
		$data = array(
			'id' => $this->getId(),
			'firstname' => $this->getFirstname(),
			'lastname' => $this->getLastname(),
			'shortName' => $this->getNameShort(),
			'avatar' => self::getAvatarForPlayerId($this->getId()),
			'checkedIn' => $this->isCheckedIn(),
			'active' => $this->isActive(),
			'deleted' => $this->isDeleted(),
			'email' => $this->getEmail(),
			'gtalk' => $this->getGtalk(),
			'type' => $this->getEntity()->getType(),
			'uid' => $this->getUrlIdentifier(),
			'_lastActivity' => $this->getEntity()->getLastActivity(),
			'lastActivity' => DateUtil::formatFromDB($this->getEntity()->getLastActivity()),
			'_dateRegistered' => $this->getEntity()->getDateRegistered(),
			'dateRegistered' => DateUtil::formatFromDB($this->getEntity()->getDateRegistered())
		);

		if (true === $withStats) {
			$data['stats']['season'] = $this->getStats(PlayerStatistics::CACHETYPE_SEASON)->toHash();
			$data['stats']['alltime'] = $this->getStats(PlayerStatistics::CACHETYPE_ALLTIME)->toHash();
		}

		return $data;
	}

	/**
	 *
	 * @return string
	 */
	public static function getAvatarDir() {
		return PG::getStaticDir() . DS . 'upload/avatar/';
	}

	/**
	 *
	 * @return string
	 */
	public static function getAvatarDirSimple() {
		return '/static/upload/avatar/';
	}

	/**
	 *
	 * @param PDO $db
	 * @param int $playerId
	 * @param string $format
	 * @return array
	 */
	public static function getPlayerNameById(PDO $db, $playerId, $format = 'firstname lastname') {
		$sql = "SELECT firstname, lastname FROM player WHERE id = " . $db->quote(intval($playerId));
		$r = $db->query($sql)->fetch(PDO::FETCH_NAMED);

		return str_replace(array('firstname', 'lastname'), array($r['firstname'], $r['lastname']), $format);
	}

	/**
	 *
	 * @param PDO $db
	 * @param bool $active
	 * @param bool $checkedIn
	 * @param bool $deleted
	 * @return PlayerEntity[]
	 */
	public static function getPlayers(PDO $db, $active = null, $checkedIn = null, $deleted = null) {
		$sql = "SELECT * FROM `player`";

		if (null !== $active || null !== $checkedIn || null !== $deleted) {
			$sql .= ' WHERE ';
		}

		if (null !== $active) {
			if (true === $active) {
				$sql .= "active = 1 ";
			} else {
				$sql .= "active = 0 ";
			}
		}
		if (null !== $checkedIn) {
			if (null !== $active) {
				$sql .= 'AND ';
			}
			if (true === $checkedIn) {
				$sql .= "checkedIn = 1 ";
			} else {
				$sql .= "checkedIn = 0 ";
			}
		}
		if (null !== $deleted) {
			if (null !== $active || null !== $checkedIn) {
				$sql .= 'AND ';
			}
			if (true === $deleted) {
				$sql .= "deleted = 1 ";
			} else {
				$sql .= "deleted = 0 ";
			}
		}

		$sql.=" ORDER BY `firstname` ASC";

		return PlayerEntity::findBySql($db, $sql);
	}

	/**
	 * change password for player
	 *
	 * @param PlayerEntity $player
	 * @param string $oldPassword
	 * @param string $newPassword
	 * @return bool
	 */
	public static function changePassword(PlayerEntity $player, $oldPassword, $newPassword) {
		if (self::isPlayerAuthorized($player->getId()) && strlen($newPassword) >= 4) {
			$auth = AuthenticationManager::getInstance();
			$hashedOldPassword = AuthenticationProviderBasic::hash($player->getEmail(), $oldPassword);
			$hashedNewPassword = AuthenticationProviderBasic::hash($player->getEmail(), $newPassword);

			$adminCheck = false;
			if (PlayerModel::isPlayerAdmin() && $auth->getPlayer()->getId() != $player->getId()) {
				$adminCheck = true;
			}

			if ($adminCheck || $player->getPassword() == $hashedOldPassword) {
				$player->setPassword($hashedNewPassword);
				$player->updateToDatabase(PG::getDB());
				return true;
			}
		}
		return false;
	}

	/**
	 * if no playerId is passed, the current user will be checked for admin type.
	 *
	 * @param int $playerId
	 * @return bool
	 */
	public static function isPlayerAdmin($playerId = null) {
		if (null !== $playerId) {
			$player = PlayerEntity::findById(PG::getDB(), $playerId);

			if ($player instanceof PlayerEntity) {
				return $player->getType() == AuthenticationManager::TYPE_ADMIN;
			}

			return false;
		}

		$player = AuthenticationManager::getInstance()->getPlayer();

		return (($player instanceof PlayerEntity) && $player->getType() == AuthenticationManager::TYPE_ADMIN);
	}

	/**
	 * check if user is the user the action is for, or if the user has admin status
	 *
	 * @param int $playerId
	 * @return bool
	 */
	public static function isPlayerAuthorized($playerId = null) {
		return self::isPlayerAdmin() || (AuthenticationManager::getInstance()->getPlayer()->getId() == $playerId);
	}

	/**
	 * @static
	 * @param int $playerId
	 * @return string
	 */
	public static function getAvatarForPlayerId($playerId) {
		foreach (self::$validAvatarExt as $ext) {
			if (file_exists(self::getAvatarDir() . intval($playerId) . '.' . $ext)) {
				return self::getAvatarDirSimple() . intval($playerId) . '.' . $ext;
			}
		}
		return self::getAvatarDirSimple() . 'dummy.png';
	}

	/**
	 *
	 * @param int $playerId
	 */
	private static function cleanUpAvatarsForPlayerId($playerId) {
		foreach (self::$validAvatarExt as $ext) {
			if (file_exists(self::getAvatarDir() . intval($playerId) . '.' . $ext)) {
				unlink(self::getAvatarDir() . intval($playerId) . '.' . $ext);
			}
		}
	}

	/**
	 * handle basic avatar upload
	 *
	 * @static
	 * @return array
	 */
	public static function handleAvatarUpload() {
		$db = PG::getDB();
		$playerId = intval($_POST['playerId']);

		if (self::existsPlayer($db, $playerId) && isset($_FILES['avatarUpload'])) {
			if (self::isPlayerAuthorized($playerId)) {
				$name = $_FILES['avatarUpload']['name'];
				$size = $_FILES['avatarUpload']['size'];

				if (strlen($name) > 0) {
					$ext = FileUtil::getFileExtension($name);
					if (in_array($ext, self::$validAvatarExt)) {
						if ($size < 512000) {
							$newImageName = $playerId . '.' . $ext;
							$tmpName = $_FILES['avatarUpload']['tmp_name'];

							self::cleanUpAvatarsForPlayerId($playerId);
							if (move_uploaded_file($tmpName, self::getAvatarDir() . $newImageName)) {
								$result = array(
									'success' => true,
									'src' => self::getAvatarDirSimple() . $newImageName
								);
							} else {
								$result = array(
									'success' => false,
									'error' => 'Upload fehlgeschlagen'
								);
							}
						} else {
							$result = array(
								'success' => false,
								'error' => 'Dateigröße max. 512KB'
							);
						}
					} else {
						$result = array(
							'success' => false,
							'error' => 'Ungültiges Dateiformat.'
						);
					}
				} else {
					$result = array(
						'success' => false,
						'error' => 'Kein Bild ausgewählt'
					);
				}
			} else {
				$result = array(
					'success' => false,
					'error' => 'Keine Berechtigung'
				);
			}
		} else {
			$result = array(
				'success' => false,
				'error' => 'Total Fail.'
			);
		}
		return $result;
	}

	/**
	 * @static
	 * @param PDO $db
	 * @param bool $active
	 * @param bool $checkedIn
	 * @param bool $deleted
	 * @param bool $withStatistics
	 * @return array
	 */
	public static function getPlayersAsHash(PDO $db, $active = null, $checkedIn = null, $deleted = null, $withStatistics = false) {
		$hash = array();
		foreach (self::getPlayers($db, $active, $checkedIn, $deleted) as $player) {
			if ($player instanceof PlayerEntity) {
				$playerModel = new PlayerModel($player);
				$hash[] = $playerModel->toHash($withStatistics);
			}
		}
		return $hash;
	}

	/**
	 * count active players
	 *
	 * @param PDO $db
	 * @param bool $active
	 * @return int
	 */
	public static function countCheckedinPlayers(PDO $db, $active = true) {
		$sql = "SELECT COUNT(id) FROM player WHERE checkedIn = 1";

		if (true === $active) {
			$sql .= ' AND active = 1';
		}

		return intval($db->query($sql)->fetchColumn());
	}

	/**
	 * @param PDO $db
	 * @param string $firstname
	 * @param string $lastname
	 * @param string $email
	 * @param string $password
	 * @param string $gtalk
	 * @param bool $active
	 * @param bool $checkedIn
	 * @param string $type - AuthenticationManager::TYPE_ADMIN or standard: AuthenticationManager::TYPE_USER
	 * @return PlayerEntity
	 */
    public static function addPlayer(PDO $db, $firstname, $lastname, $email, $password, $gtalk, $active = false, $checkedIn = false, $type = null) {
		$player = new PlayerEntity();
		$player->setFirstname($firstname)
				->setLastname($lastname)
				->setEmail($email)
				->setGtalk($gtalk)
				->setPassword(AuthenticationProviderBasic::hash($email, $password))
				->setActive($active)
				->setDeleted(false)
				->setCheckedIn($checkedIn)
				->setLastActivity(DateUtil::formatForDB())
				->setDateRegistered(DateUtil::formatForDB())
        ;

		if (null === $type) {
			$type = AuthenticationManager::TYPE_USER;
		}
		$player->setType($type);

		$player->insertIntoDatabase($db);

		return $player;
	}

	/**
	 *
	 * @param PDO $db
	 * @param int $playerId
	 * @return bool
	 */
	public static function existsPlayer(PDO $db, $playerId) {
		return (PlayerEntity::findById($db, $playerId) instanceof PlayerEntity);
	}

	/**
	 *
	 * @param PDO $db
	 * @param string $uid
	 * @return PlayerEntity|null
	 */
	public static function getPlayerByUrlIdentifier(PDO $db, $uid) {
		$sql = "SELECT * FROM `player` WHERE email LIKE " . $db->quote($uid . '@%') . "";
		$player = PlayerEntity::findBySql($db, $sql);

		if (!empty($player)) {
			return $player[0];
		}
		return null;
	}

	/**
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param MatchEntity $match
	 * @return bool
	 */
	public static function isPlayerInWinningTeam(PDO $db, PlayerEntity $player, MatchEntity $match) {
		$filter[] = new DFC(PlayerMatchEntity::FIELD_MATCHID, $match->getId(), DFC::EXACT);
		$filter[] = new DFC(PlayerMatchEntity::FIELD_PLAYERID, $player->getId(), DFC::EXACT);
		$playerMatch = PlayerMatchEntity::findByFilter($db, $filter);

		if ($playerMatch[0] instanceof PlayerMatchEntity) {
			if (MatchManager::TEAM_RED == $playerMatch[0]->getTeam()) {
				return $match->getResultTeamRed() > $match->getResultTeamYellow();
			} else {
				return $match->getResultTeamYellow() > $match->getResultTeamRed();
			}
		}

		return false;
	}

}
