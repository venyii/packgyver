<?php

/**
 * PackGyver - Notification Collection
 *
 * @package notification
 */
class NotificationCollection implements IteratorAggregate, Countable {

	/**
	 *
	 * @var PlayerModel
	 */
	private $player;

	/**
	 *
	 * @var ArrayObject
	 */
	private $notifications = null;

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setPlayer(PlayerModel $player) {
		$this->player = $player;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getPlayer() {
		return $this->player;
	}

	/**
	 *
	 * @return Iterator
	 */
	public function getIterator() {
		return $this->notifications;
	}

	/**
	 *
	 * @return int
	 */
	public function count() {
		return $this->notifications->count();
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function __construct(PlayerModel $player = null) {
		$this->notifications = new ArrayObject(array());

		if (null !== $player) {
			$this->player = $player;
		}
	}

	/**
	 *
	 * @return ArrayObject
	 */
	public function getCollectionForPlayer() {
		if ($this->count() === 0) {
			$notifications = NotificationEntity::findByFilter(PG::getDB(), array(
						new DFC(NotificationEntity::FIELD_PLAYERID, $this->getPlayer()->getId(), DFC::EXACT)
							), true, new DSC(NotificationEntity::FIELD_DATESENT, DSC::DESC));

			foreach ($notifications as $notification) {
				$note = new Notification();
				$note->assignByEntity($notification);
				$note->setPlayer($this->getPlayer());

				$this->notifications->append($note);
			}
		}

		return $this->notifications;
	}

	/**
	 *
	 * @return Notification[]
	 */
	public function getNotifications() {
		$notifications = array();

		foreach (NotificationEntity::findByFilter(PG::getDB(), array()) as $notificationEntity) {
			$notification = new Notification();
			$notification->assignByEntity($notificationEntity);

			$notifications[] = $notification;
		}

		return $notifications;
	}

	/**
	 *
	 * @return array
	 */
	public function toHash() {
		$hash = array();

		foreach ($this->notifications as $key => $notification) {
			/* @var Notification $notification */
			$hash[$key] = $notification->toHash();
		}

		return $hash;
	}

}

?>