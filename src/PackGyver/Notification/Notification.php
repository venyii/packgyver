<?php

/**
 * PackGyver - Notification
 *
 * @package notification
 */
class Notification {

	/**
	 *
	 * @var string
	 */
	private $title;

	/**
	 *
	 * @var string
	 */
	private $message;

	/**
	 *
	 * @var string
	 */
	private $dateSent;

	/**
	 *
	 * @var NotificationEntity
	 */
	private $entity;

	/**
	 *
	 * @var PlayerModel
	 */
	private $player;

	/**
	 *
	 * @param PlayerModel $player
	 * @return Notification
	 */
	public function setPlayer(PlayerModel $player) {
		$this->player = $player;
		return $this;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getPlayer() {
		return $this->player;
	}

	/**
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 *
	 * @param string
	 * @return Notification
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 *
	 * @param string $message
	 * @return Notification
	 */
	public function setMessage($message) {
		$this->message = $message;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDateSent() {
		return $this->dateSent;
	}

	/**
	 *
	 * @param string $dateSent
	 * @return Notification
	 */
	public function setDateSent($dateSent) {
		$this->dateSent = $dateSent;
		return $this;
	}

	/**
	 *
	 * @return NotificationEntity
	 */
	public function getEntity() {
		return $this->entity;
	}

	/**
	 *
	 * @param NotificationEntity $entity
	 */
	public function setEntity(NotificationEntity $entity) {
		$this->entity = $entity;
	}

	/**
	 *
	 * @param NotificationEntity $notification
	 */
	public function assignByEntity(NotificationEntity $notification) {
		$this->entity = $notification;
		$this->title = $notification->getTitle();
		$this->message = $notification->getMessage();
		$this->dateSent = $notification->getDateSent();
		$this->player = new PlayerModel($notification->fetchPlayerEntity(PG::getDB()));
	}

	/**
	 * save notification 
	 */
	public function save() {
		$entity = new NotificationEntity();
		$entity->setTitle($this->getTitle())
				->setMessage($this->getMessage())
				->setPlayerId($this->getPlayer()->getId())
				->setDateSent($this->getDateSent());

		if (null === $this->getEntity()) {
			$entity->insertIntoDatabase(PG::getDB());
		} else {
			$entity->setId($this->getEntity()->getId())->updateToDatabase(PG::getDB());
		}

		$this->setEntity($entity);
	}

	/**
	 * delete entity 
	 */
	public function delete() {
		if ($this->getEntity() instanceof NotificationEntity) {
			$this->getEntity()->deleteFromDatabase(PG::getDB());
		}
	}

	/**
	 *
	 * @return array
	 */
	public function toHash() {
		$hash = array();

		$hash['id'] = $this->getEntity()->getId();
		$hash['title'] = $this->getTitle();
		$hash['message'] = $this->getMessage();
		$hash['dateSent'] = DateUtil::formatFromDB($this->getDateSent());
		$hash['player'] = $this->getPlayer()->toHash();

		return $hash;
	}

}

?>