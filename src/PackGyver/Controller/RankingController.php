<?php

/**
 * PackGyver - Ranking Controller
 */
class RankingController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Ranking::Default';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Tabelle');

		if (Cache::getCache()->exists(self::CACHE_DEFAULT)) {
			return new Response(Cache::getCache()->get(self::CACHE_DEFAULT));
		}

		$rankingManager = new RankingManager(PlayerCollection::create(null, null, false));
		$ranking = $rankingManager->getPlayerCollection();

		foreach ($ranking as $playerRank) {
			/* @var $playerRank PlayerModel */
			if (!$playerRank->isActive() && $playerRank->getStats(PlayerStatistics::CACHETYPE_SEASON)->getCountMatches() === 0) {
				$ranking->deletePlayer($playerRank);
			}
		}

		$data = array(
			'function' => 'all',
			'players' => $ranking->toHash(true),
			'season' => SeasonManager::getCurrentSeasonDates()
		);

		$cache = $this->render('ranking', $data);
		Cache::getCache()->set(self::CACHE_DEFAULT, $cache);

		return new Response($cache);
	}

}
