<?php

/**
 * PackGyver - Admin Controller
 */
class AdminController extends Controller {

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Admin Center');

		if (!PlayerModel::isPlayerAdmin()) {
			$this->redirect('/');
		}

		$data = array(
			'activePlayers' => AdminManager::getLoggedInPlayers(10)->toHash()
		);

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/overview', $data),
							'function' => null
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionAjax() {
		$response = null;

		if ($this->getRequest()->isAjax()) {
			$db = PG::getDB();

			switch ($this->getParameter(0)) {
				case 'clearCache':
					$type = trim(htmlentities($this->getParameter(1)));

					if ('' != $type) {
						AdminManager::clearCache($type);
					}

					$response = null;
					break;
				case 'changePlayerStatus':
					if (is_numeric($this->getParameter(1))) {
						AdminManager::changePlayerStatus($db, (int) $this->getParameter(1));
					}

					$response = null;
					break;
				case 'changeSetting':
					$id = (int) $_POST['id'];
					$value = trim($_POST['value']);
					$response = null;
					$settingEntity = SettingsEntity::findById($db, $id);

					if ($settingEntity instanceof SettingsEntity) {
						$settingEntity->setValue($value);
						$settingEntity->updateToDatabase($db);
						$response = $value;
					} else {
						$response = 'ADMIN_EDIT_SETTINGS_ERROR';
					}
					break;
			}
		}

		return Response::withMimeType($response);
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionPlayers() {
		$this->setMetaTitle('Spieler');

		$data = array();
		$db = PG::getDB();

		if ($this->getParameter(0) == 'create' || ($this->getParameter(0) == 'edit' && is_numeric($this->getParameter(1)))) {
			$isEdit = ($this->getParameter(0) == 'edit');

			if (true === $isEdit) {
				$player = PlayerEntity::findById($db, (int) $this->getParameter(1));
			} else {
				$player = new PlayerEntity();
			}

			if ($player instanceof PlayerEntity) {
				$data['action'] = (true === $isEdit) ? 'edit' : 'create';

				if ('save' == $this->getParameter(2) && isset($_POST['save'])) {
					$firstname = ValidationUtil::getInput($_POST['firstname']);
					$lastname = ValidationUtil::getInput($_POST['lastname']);
					$gtalk = ValidationUtil::getInput($_POST['gtalk']);
					$email = ValidationUtil::getInput($_POST['email']);
					$type = ValidationUtil::getInputInt($_POST['type']);
					$active = (isset($_POST['active']) ? true : false);
					$checkedIn = (isset($_POST['checkedIn']) ? true : false);
					$deleted = (isset($_POST['deleted']) ? true : false);
					$password = trim($_POST['password']);
					$setPassword = ('' != $password && strlen($password) >= 4);

					if ($firstname && $lastname && ValidationUtil::validateEmail($gtalk) && ValidationUtil::validateEmail($email) && null !== $type) {
						$player->setFirstname($firstname)
								->setLastname($lastname)
								->setGtalk($gtalk)
								->setEmail($email)
								->setActive($active)
								->setCheckedIn($checkedIn)
								->setDeleted($deleted)
								->setType($type === 1 ? 1 : 0);

						if (true === $setPassword) {
							$player->setPassword(AuthenticationProviderBasic::hash($email, $password));
						}

						if (true === $isEdit) {
							$player->updateToDatabase($db);

							CacheManager::clearPlayerProfile(Cache::getCache(), $player, PlayerController::SECTION_PROFILE);
						} else {
							$player->setLastActivity(DateUtil::formatForDB());
							$player->setDateRegistered(DateUtil::formatForDB());
							$player->insertIntoDatabase($db);
						}

						$data['success'] = true;
					} else {
						$data['success'] = false;
					}

					$data['isSent'] = true;
				}

				if (true === $isEdit) {
					$playerModel = new PlayerModel($player);
					$data['player'] = $playerModel->toHash();
				} else {
					$data['player'] = null;
				}
			}
		} else {
			$data['players'] = PlayerModel::getPlayersAsHash($db, null, null, false);
			$data['playersDeleted'] = PlayerModel::getPlayersAsHash($db, null, null, true);
		}

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/players', $data),
							'function' => 'players'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionMatches() {
		$this->setMetaTitle('Spiele');

		$matchData = array();

		if ($this->getParameter(0) == 'add' || $this->getParameter(0) == 'edit') {
			$isEdit = ($this->getParameter(0) == 'edit');
			$db = PG::getDB();
			$type = null;

			$matchData['edit'] = $isEdit;
			$matchData['isSent'] = false;
			$matchData['success'] = false;

			if (true === $isEdit && (int) $this->getParameter(1) > 0) {
				$matchEntity = MatchEntity::findById($db, (int) $this->getParameter(1));

				if ($matchEntity instanceof MatchEntity) {
					$match = new MatchModel($matchEntity);
					$type = $match->getTypeName();
					$matchData['match'] = $match->toHash();
				}
			}

			if (!$type && false === $isEdit) {
				$type = ($this->getParameter(1) == '1on1') ? '1on1' : '2on2';
			}

			if (true === $isEdit && $match instanceof MatchModel && isset($_POST['deletePermanent'])) {
				if ($match->getScoreRed() === 0 && $match->getScoreYellow()) {
					Cache::getCache()->delete(PlayController::CACHE_DEFAULT);
				}

				$match->delete($db);
				$this->redirect('/admin/matches');
			}

			if (false === $isEdit && $this->getParameter(1) == 'save' && isset($_POST['addMatch'])) {
				$matchEntity = new MatchEntity();
				$matchEntity->assignDefaultValues();

				$match = new MatchModel($matchEntity);
				$match->setDateCreated(DateUtil::formatForDB());
				$match->setRandom(false);
				$match->setDeleted(false);
				$match->setCreatedByPlayer(new PlayerModel(AuthenticationManager::getInstance()->getPlayer()));
				$match->setSavedByPlayer(new PlayerModel(AuthenticationManager::getInstance()->getPlayer()));
				$match->setScoreRed(0);
				$match->setScoreYellow(0);
				$match->assignPlayersFromRequest();
				$isCreated = $match->register($db);

				if ($isCreated) {
					$matchData['success'] = true;
					Cache::getCache()->delete(PlayController::CACHE_DEFAULT);
				}
			}

			if ($this->getParameter(2) == 'save' && isset($_POST['editMatch'])) {
				$matchData['isSent'] = true;

				if ((true === $isEdit || true === $isCreated) && $match instanceof MatchModel) {
					if (is_numeric($_POST['scoreRed']) && is_numeric($_POST['scoreYellow'])) {
						$resultRed = (int) $_POST['scoreRed'];
						$resultYellow = (int) $_POST['scoreYellow'];

						if (MatchManager::isValidScore($resultRed, $resultYellow)
								|| ($resultRed === 0 && $resultYellow === 0)) {

							$markDeleted = isset($_POST['markDeleted']);

							$match->setScoreRed($resultRed);
							$match->setScoreYellow($resultYellow);
							$match->setDeleted($markDeleted);

							if ($match->getType() === MatchModel::TYPE_1ON1) {
								$match->setRedSinglePlayer(new PlayerModel(PlayerEntity::findById($db, (int) $_POST['redSinglePlayer'])));
								$match->setYellowSinglePlayer(new PlayerModel(PlayerEntity::findById($db, (int) $_POST['yellowSinglePlayer'])));
							} else {
								$match->setRedGoalPlayer(new PlayerModel(PlayerEntity::findById($db, (int) $_POST['teamRedGoal'])));
								$match->setRedOffensePlayer(new PlayerModel(PlayerEntity::findById($db, (int) $_POST['teamRedOffense'])));
								$match->setYellowGoalPlayer(new PlayerModel(PlayerEntity::findById($db, (int) $_POST['teamYellowGoal'])));
								$match->setYellowOffensePlayer(new PlayerModel(PlayerEntity::findById($db, (int) $_POST['teamYellowOffense'])));
							}

							$match->save($db);
							$match->recalculatePlayerStats();

							foreach ($match->getPlayers() as $player) {
								CacheManager::clearPlayerCache($player->getEntity());
							}
							Cache::getCache()->delete(PlayController::CACHE_DEFAULT);
							Cache::getCache()->delete(RankingController::CACHE_DEFAULT);
							Cache::getCache()->delete(StatsController::CACHE_DEFAULT);

							$matchData['match'] = $match->toHash();
							$matchData['success'] = true;
						}
					}
				}
			}

			$matchData['type'] = $type;
			$matchData['players'] = PlayerModel::getPlayersAsHash($db, null, null, false);
			$matchData['function'] = $this->getParameter(0);
		} else {
			$matches = MatchManager::getMatches(null, true, null, null, null, 'dateCreated');

			$matchData['matches'] = [];
			foreach ($matches as $match) {
				$matchData['matches'][] = $match->toHash();
			}
		}

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/matches', $matchData),
							'function' => 'matches'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionAwards() {
		$this->setMetaTitle('Awards');

		$data = array();
		$section = null;
		$db = PG::getDB();
		$cache = Cache::getCache();

		if ($this->getParameter(0) == 'add' && PlayerModel::isPlayerAdmin()) {
			$data['edit'] = false;

			if ($this->getParameter(1) == 'playerAward') {
				$data['function'] = 'addPlayerAward';
				$data['players'] = PlayerModel::getPlayersAsHash($db, null, null, false);
				$data['awards'] = AwardManager::getAwardHashes($db);

				if ($this->getParameter(3) == 'save') {
					$player = PlayerEntity::findById($db, (int) $_POST['playerId']);
					$award = AwardEntity::findById($db, (int) $_POST['awardId']);
					$comment = trim(htmlspecialchars($_POST['comment']));
					$sendNotification = isset($_POST['sendNotification']) ? true : false;

					if ($player instanceof PlayerEntity && $award instanceof AwardEntity) {
						AwardManager::addPlayerAward($db, $player, $award, $comment);

						if (true === $sendNotification) {
							$notification = AwardManager::sendAwardGivenNotificationToPlayer(new PlayerModel($player), $award);
							$wsservice = WebSocketServiceManager::getInstance();

							if ($wsservice->isEnabled()) {
								$wsservice->getDataDispatcher()->triggerNotificationReceived($player, $notification);
							}
						}

						$cache->delete(AwardsController::CACHE_DEFAULT);
						CacheManager::clearPlayerProfile($cache, $player, PlayerController::SECTION_AWARDS);
					}
					$this->redirect('/admin/awards');
				}
			} else {
				$data['function'] = 'addAward';
				$data['awards'] = AwardManager::getAwardHashes($db);
				$data['award'] = null;

				if ($this->getParameter(3) == 'save') {
					$name = trim(htmlspecialchars($_POST['name']));
					$description = trim(htmlspecialchars($_POST['description']));
					$key = trim(htmlspecialchars($_POST['key']));

					if ('' != $name) {
						AwardManager::addAward($db, $name, $description, $key);
					}

					$cache->delete(AwardsController::CACHE_DEFAULT);

					$this->redirect('/admin/awards');
				}
			}
		} else if ($this->getParameter(0) == 'edit' && is_numeric($this->getParameter(2)) && PlayerModel::isPlayerAdmin()) {
			$id = (int) $this->getParameter(2);
			$data['edit'] = true;

			if ($this->getParameter(1) == 'playerAward') {
				$data['function'] = 'editPlayerAward';
				$playerAwardEntity = PlayerAwardEntity::findById($db, $id);

				if ($playerAwardEntity instanceof PlayerAwardEntity) {
					$data['playerId'] = $playerAwardEntity->getPlayerId();
					$data['awardId'] = $playerAwardEntity->getAwardId();
					$data['playerAward'] = AwardManager::getHashForPlayerAward($playerAwardEntity);
					$data['players'] = PlayerModel::getPlayersAsHash($db, null, null, false);
					$data['awards'] = AwardManager::getAwardHashes($db);

					if ($this->getParameter(3) == 'save') {
						$player = PlayerEntity::findById($db, (int) $_POST['playerId']);
						$award = AwardEntity::findById($db, (int) $_POST['awardId']);
						$comment = trim(htmlspecialchars($_POST['comment']));

						if ($player instanceof PlayerEntity && $award instanceof AwardEntity) {
							if ($playerAwardEntity instanceof PlayerAwardEntity) {
								$playerAwardEntity->setPlayerId($player->getId());
								$playerAwardEntity->setAwardId($award->getId());
								$playerAwardEntity->setComment($comment);
								$playerAwardEntity->updateToDatabase($db);

								$cache->delete(AwardsController::CACHE_DEFAULT);
								CacheManager::clearPlayerProfile($cache, $player, PlayerController::SECTION_AWARDS);
							}
							$this->redirect('/admin/awards');
						}
					}
				} else {
					$data['error'] = 'Kein Player Award mit dieser ID gefunden';
				}
			} else {
				$data['function'] = 'editAward';
				$award = AwardEntity::findById($db, $id);

				if ($award instanceof AwardEntity) {
					$data['award'] = AwardManager::getHashForAward($award);

					if ($this->getParameter(3) == 'save') {
						$name = trim(htmlspecialchars($_POST['name']));
						$description = trim(htmlspecialchars($_POST['description']));
						$key = trim(htmlspecialchars($_POST['key']));

						if ('' != $name) {
							if ($award instanceof AwardEntity) {
								$award->setName($name);
								$award->setDescription($description);
								$award->setKey($key);

								if (null !== ($logo = AwardManager::handleLogoUpload($award->getId()))) {
									$award->setLogo($logo);
								}

								$award->updateToDatabase($db);

								$cache->delete(AwardsController::CACHE_DEFAULT);
							}
							$this->redirect('/admin/awards');
						}
					}
				} else {
					$data['error'] = 'Kein Award mit dieser ID gefunden';
				}
			}
		} else if ($this->getParameter(0) == 'delete' && $this->getRequest()->isAjax()) {
			$id = (int) $this->getParameter(2);
			$success = false;
			$error = null;

			if ($this->getParameter(1) == 'playerAward') {
				$playerAwardEntity = PlayerAwardEntity::findById($db, $id);

				if ($playerAwardEntity instanceof PlayerAwardEntity) {
					$playerAwardEntity->deleteFromDatabase($db);
					$success = true;

					Cache::getCache()->delete(AwardsController::CACHE_DEFAULT);
					CacheManager::clearPlayerProfile($cache, $playerAwardEntity->fetchPlayerEntity($db)->getId(), PlayerController::SECTION_AWARDS);
				}
			} else {
				$award = AwardEntity::findById($db, $id);

				if ($award instanceof AwardEntity) {
					if (0 === count($award->fetchPlayerAwardEntityCollection($db))) {
						$award->deleteFromDatabase($db);
						$success = true;

						Cache::getCache()->delete(AwardsController::CACHE_DEFAULT);
					} else {
						$error = 'Dieser Award hat derzeit noch Träger und kann somit nicht gelöscht werden';
					}
				}
			}

			$data = array(
				'id' => $id,
				'success' => $success,
				'error' => $error
			);

			return Response::withMimeType(json_encode($data));
		} else if ($this->getParameter(0) == 'detect') {
			$this->setMetaTitle('Award Detector');

			$awardPlayers = isset($_POST['awardPlayers']);
			$section = 'detect';

			$data['function'] = 'detect';
			$data['error'] = false;
			$data['status'] = [];

			$month = (int) $this->getParameter(1);
			$year = (int) $this->getParameter(2);

			if (!$month || !$year) {
				$month = date('n');
				$year = date('Y');
			}

			$detector = new AwardDetector($month, $year);

			if ($detector->isValid()) {
				$data['mps'] = $detector->getMostMonsterPacksPlayer();
				$data['ranking'] = $detector->getRanking();
				$data['month'] = DateUtil::getMonthNameForMonth($month);
				$data['year'] = $year;

				if (true === $awardPlayers) {
					$status = array();
					$comment = DateUtil::getMonthNameForMonth($month) . ' \'' . substr($year, 2);

					if (!empty($data['mps'])) {
						foreach ($data['mps'] as $mpsPlayer) {
							$monsterPackAward = AwardEntity::findByFilter($db, array(
										new DFC(AwardEntity::FIELD_KEY, 'Season::MonsterPack::Most')
									));

							if ($monsterPackAward[0] instanceof AwardEntity) {
								$player = PlayerEntity::findById($db, $mpsPlayer['player']['id']);
								AwardManager::addPlayerAward($db, $player, $monsterPackAward[0], $comment, null, true);
								CacheManager::clearPlayerProfile($cache, $player, PlayerController::SECTION_AWARDS);
								$status['monsterPacks'] = true;
							} else {
								$status['monsterPacks'] = false;
							}
						}
					}

					// 1st
					$firstPlaceAward = AwardEntity::findByFilter($db, array(
								new DFC(AwardEntity::FIELD_KEY, 'Season::Rank::First')
							));
					if ($firstPlaceAward[0] instanceof AwardEntity) {
						$player = PlayerEntity::findById($db, $data['ranking'][0]['id']);
						AwardManager::addPlayerAward($db, $player, $firstPlaceAward[0], $comment, null, true);
						CacheManager::clearPlayerProfile($cache, $player, PlayerController::SECTION_AWARDS);
						$status['seasonFirst'] = true;
					} else {
						$status['seasonFirst'] = false;
					}

					// 2nd
					$secondPlaceAward = AwardEntity::findByFilter($db, array(
								new DFC(AwardEntity::FIELD_KEY, 'Season::Rank::Second')
							));
					if ($secondPlaceAward[0] instanceof AwardEntity) {
						$player = PlayerEntity::findById($db, $data['ranking'][1]['id']);
						AwardManager::addPlayerAward($db, $player, $secondPlaceAward[0], $comment, null, true);
						CacheManager::clearPlayerProfile($cache, $player, PlayerController::SECTION_AWARDS);
						$status['seasonSecond'] = true;
					} else {
						$status['seasonSecond'] = false;
					}

					// 3rd
					$thirdPlaceAward = AwardEntity::findByFilter($db, array(
								new DFC(AwardEntity::FIELD_KEY, 'Season::Rank::Third')
							));
					if ($thirdPlaceAward[0] instanceof AwardEntity) {
						$player = PlayerEntity::findById($db, $data['ranking'][2]['id']);
						AwardManager::addPlayerAward($db, $player, $thirdPlaceAward[0], $comment, null, true);
						CacheManager::clearPlayerProfile($cache, $player, PlayerController::SECTION_AWARDS);
						$status['seasonThird'] = true;
					} else {
						$status['seasonThird'] = false;
					}

					$data['status'] = $status;
				}
			} else {
				$data['error'] = 'Ein Fehler ist aufgetreten.';
			}
		} else {
			$awards = AwardManager::getAwards($db);
			$data['pAwards'] = array();

			foreach ($awards as $award) {
				if ($award instanceof AwardEntity) {
					$data['pAwards'][$award->getId()] = AwardManager::getPlayerHashesForAward($db, $award);
				}
			}

			$data['awards'] = AwardManager::getAwardHashes($db);
		}

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/awards', $data),
							'function' => 'awards',
							'section' => $section
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionLogs() {
		$this->setMetaTitle('Logs');

		$data = array('logs' => Logger::getLogs(3, 50));

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/logs', $data),
							'function' => 'activity'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionCache() {
		$this->setMetaTitle('Cache');

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/cache', array()),
							'function' => 'cache'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionNotifications() {
		$this->setMetaTitle('Notifications');

		$data = array();
		$db = PG::getDB();

		if ($this->getParameter(0) == 'create' || $this->getParameter(0) == 'edit') {
			$data['function'] = ($this->getParameter(0) == 'create') ? 'create' : 'edit';
			$data['isEdit'] = $data['function'] == 'edit';
			$data['players'] = PlayerModel::getPlayersAsHash($db, null, null, false);

			if ($data['function'] == 'edit') {
				$notificationEntity = NotificationEntity::findById($db, (int) $this->getParameter(1));

				if ($notificationEntity instanceof NotificationEntity) {
					$notification = new Notification();
					$notification->assignByEntity($notificationEntity);
					$data['notification'] = $notification->toHash();
				} else {
					$this->redirect('/admin/notifications');
				}
			}

			if ($this->getParameter(2) == 'save' && isset($_POST['save'])) {
				$title = ValidationUtil::getInput($_POST['title']);
				$recipients = $_POST['recipients'];

				if (isset($_POST['isHTML'])) {
					$message = $_POST['message'];
				} else {
					$message = ValidationUtil::getInput($_POST['message']);
				}

				if (null !== $title && null !== $message && is_array($recipients) && count($recipients) > 0) {
					foreach ($recipients as $recipient) {
						$player = PlayerEntity::findById($db, (int) $recipient);

						if ($player instanceof PlayerEntity) {
							if ($data['function'] == 'edit') {
								$notificationEntity = NotificationEntity::findById($db, (int) $this->getParameter(1));

								if ($notificationEntity instanceof NotificationEntity) {
									$notification = new Notification();
									$notification->assignByEntity($notificationEntity);
								}
							} else {
								$notification = new Notification();
							}

							if ($notification instanceof Notification) {
								$notification->setTitle($title)
										->setMessage($message)
										->setDateSent(DateUtil::formatForDB())
										->setPlayer(new PlayerModel($player))
										->save();

								$wsservice = WebSocketServiceManager::getInstance();

								if ($wsservice->isEnabled()) {
									$wsservice->getDataDispatcher()->triggerNotificationReceived($player, $notification);
								}
							}
						}
					}
					$data['success'] = true;
				} else {
					$data['success'] = false;
				}

				$data['isSent'] = true;
			}
		} else if ($this->getParameter(0) == 'delete') {
			$db = PG::getDB();
			$notification = NotificationEntity::findById($db, (int) $this->getParameter(1));

			if ($notification instanceof NotificationEntity) {
				$notification->deleteFromDatabase($db);
			}

			$this->redirect('/admin/notifications');
		} else {
			$notifications = new NotificationCollection();

			$data['notifications'] = [];
			foreach ($notifications->getNotifications() as $notification) {
				/* @var Notification $notification */
				$data['notifications'][] = $notification->toHash();
			}
		}

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/notifications', $data),
							'function' => 'notifications'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionArchive() {
		$this->setMetaTitle('Archiv');

		if ($this->getParameter(0) == 'store') {
			$result = array(
				'message' => null
			);
			$fromMonth = (int) $_POST['fromMonth'];
			$fromYear = (int) $_POST['fromYear'];
			$toMonth = (int) $_POST['toMonth'];
			$toYear = (int) $_POST['toYear'];

			if (0===$toMonth||$toYear===0) {
				$dates=SeasonManager::getCurrentSeasonDatesSimple();
				$toMonth=$dates['month'];
				$toYear=$dates['year'];
			}

			$fromDate = $fromYear . '-' . $fromMonth . '-01';
			$toDate = $toYear . '-' . $toMonth . '-01';

			foreach (DateUtil::getMonthsBetween($fromDate, $toDate) as $storeDate) {
				$seasonArchive = new SeasonArchive();
				$month = $storeDate['month'];
				$year = $storeDate['year'];
				$resultMsg=null;

				if (false === $seasonArchive->isSeasonStored($month, $year)) {
					if (true === $seasonArchive->isSeasonStorable($month, $year)) {
						$seasonArchive->storeSeason($month, $year);
						Cache::getCache()->delete(ArchiveController::CACHE_DEFAULT);

						$resultMsg = 'Die Saison ' . $month . '/' . $year . ' wurde archiviert';
//						$result['message'] .= 'Die Saison ' . $month . '/' . $year . ' wurde archiviert';
					}
				} else {
					$resultMsg = 'Die Saison ' . $month . '/' . $year . ' wurde bereits archiviert';
//					$result['message'] .= 'Die Saison ' . $month . '/' . $year . ' wurde bereits archiviert';
				}

				if (null === $resultMsg) {
					$resultMsg = 'Die Saison ' . $month . '/' . $year . ' konnte nicht archiviert werden';
				}

				$result['message'] .= $resultMsg . '<br />';
			}

			return Response::withMimeType(json_encode($result), 'application/json');
		}

		$data = array(
			'storedSeasons' => SeasonArchive::getStoredSeasonYearHash()
		);

		return new Response($this->render('admin/view', array(
							'content' => $this->render('admin/tabs/archive', $data),
							'function' => 'archive'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionSettings() {
		$this->setMetaTitle('Settings');

		return new Response($this->render('admin/view', array(
							'function' => 'settings',
							'content' => $this->render('admin/tabs/settings', array(
								'settings' => SettingsManager::getSettingsAsHash(PG::getDB())
							))
						)));
	}

}
