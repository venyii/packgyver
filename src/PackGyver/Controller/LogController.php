<?php

/**
 * PackGyver - Log Controller
 */
class LogController extends Controller {

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		return Response::withMimeType(null);
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionJs() {
		if ($this->getRequest()->isAjax()) {
			$msg = trim($_POST['msg']);
			$url = trim($_POST['url']);
			$line = 'Line: ' . trim($_POST['line']);
			$window = trim($_POST['window']);
			$d = ' | ';

			if ($msg && $window) {
//				Logger::get()->error($msg . $d . $url . $d . $line . $d . $window);
			}
		}

		return Response::withMimeType(null);
	}

}
