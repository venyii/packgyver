<?php

/**
 * PackGyver - 404 Controller
 */
class NotfoundController extends Controller {

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Ooops..');

		return new Response($this->render('notfound', array()));
	}

}
