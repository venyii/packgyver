<?php

/**
 * PackGyver - Archive Controller
 */
class ArchiveController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Archive::Default';
	const CACHE_SEASON = 'PG::Cache::Controller::Archive::Season::%d::%d';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Archiv');

		if (Cache::getCache()->exists(self::CACHE_DEFAULT)) {
			return new Response(Cache::getCache()->get(self::CACHE_DEFAULT));
		}

		$data = array();
		$archive = new SeasonArchive();
		$seasons = $archive->getSeasons();
		$data['currentYear'] = (int) date('Y');
		$data['seasons'] = [];

		if ($seasons->count() > 0) {
			foreach ($seasons as $key => $season) {
				/* @var $season \Season */
				$ranking = new RankingManager($season->getPlayerCollection());
				$season->setPlayerCollection($ranking->getPlayerCollection());

				$data['seasons'][$season->getYear()][$key] = $season->toHash(false);
				$data['seasons'][$season->getYear()][$key]['players'] = $season->getTop3Players()->toHash(true);
			}
		}

		$cache = $this->render('archive', $data);
		Cache::getCache()->set(self::CACHE_DEFAULT, $cache);

		return new Response($cache);
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionSeason() {
		$this->setMetaTitle('Saison-Archiv');

		$data = array();
		$year = (int) $this->getParameter(0);
		$month = (int) $this->getParameter(1);

		if (0 !== $year && 0 !== $month) {
			$seasonCacheKey = sprintf(self::CACHE_SEASON, $year, $month);

			if (Cache::getCache()->exists($seasonCacheKey)) {
				return new Response(Cache::getCache()->get($seasonCacheKey));
			}

			$seasonArchive = new SeasonArchive();

			if ($seasonArchive->isSeasonStored($month, $year)) {
				$this->setMetaTitle('Saison-Archiv ' . $month . '/' . $year);

				$ranking = new RankingManager($seasonArchive->getSeason($month, $year)->getPlayerCollection());

				$data['players'] = $ranking->getPlayerCollection()->toHash(true);
				$data['season'] = SeasonManager::getSeasonDates($month, $year);
				$data['error'] = false;
				$data['function'] = 'season';
			}
		}

		if (!isset($data['players'])) {
			$data['error'] = true;
		}

		if (!$data['error'] && $seasonCacheKey) {
			$cache = $this->render('archive', $data);
			Cache::getCache()->set($seasonCacheKey, $cache);
		}

		return new Response($cache);
	}

}
