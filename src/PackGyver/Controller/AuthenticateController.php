<?php

/**
 * PackGyver - Authenticate Controller
 */
class AuthenticateController extends Controller {

	/**
	 * redirect to main site 
	 */
	public function actionDefault() {
		$this->redirect('/');
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionService() {
		if (AuthenticationManager::getInstance()->isLoggedIn()) {
			$this->redirect('/');
		}

		$this->setMetaTitle('Login');

		$providerResponse = null;
		$provider = AuthenticationManager::getAuthenticationProviderForId($this->getParameter(0));

		if ($provider instanceof AuthenticationProviderInterface) {
			$providerResponse = $provider->handleAuthentication()->getResponse();
		}

		if ($providerResponse instanceof Response) {
			return $providerResponse;
		}

		UrlUtil::redirect('/');
	}

	/**
	 * logout the current user and redirect to main site (login) 
	 */
	public function actionLogout() {
		AuthenticationManager::getInstance()->logout();
		$this->redirect('/');
	}

}
