<?php

/**
 * PackGyver - About Controller
 */
class InfoController extends Controller {

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Über PackGyver');

		return new Response($this->render('info', array(
							'function' => null
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionRules() {
		$this->setMetaTitle('Regeln');

		return new Response($this->render('info', array(
							'function' => 'rules'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionCode() {
		$this->setMetaTitle('Code-Stats');

		return new Response($this->render('info', array(
							'function' => 'code'
						)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionPoints() {
		$this->setMetaTitle('Punkteübersicht');

		return new Response($this->render('info', array(
							'function' => 'points'
						)));
	}

}
