<?php

/**
 * PackGyver - Date Util
 */
class DateUtil {

	/**
	 * @var array
	 */
	private static $monthNames = array(
		'Januar',
		'Februar',
		'März',
		'April',
		'Mai',
		'Juni',
		'Juli',
		'August',
		'September',
		'Oktober',
		'November',
		'Dezember'
	);

	/**
	 * time to desired format
	 *
	 * @param string $format
	 * @param int $time
	 * @return string
	 */
	public static function format($time = null, $format = null) {
		if (null === $time) {
			$time = time();
		}
		if (null === $format) {
			$format = PG::DEFAULT_DATETIMEFORMAT;
		}

		return date($format, $time);
	}

	/**
	 * get timestamp in db format
	 *
	 * @param int $timestamp
	 * @return string
	 */
	public static function formatForDB($timestamp = null) {
		if (null === $timestamp) {
			$timestamp = time();
		}
		if ($timestamp instanceof DateTime) {
			$timestamp = $timestamp->getTimestamp();
		}

		return date("Y-m-d H:i:s", $timestamp);
	}

	/**
	 *
	 * @param int $timestamp
	 * @return string
	 */
	public static function formatISO8601($timestamp = null) {
		if (null === $timestamp) {
			$timestamp = time();
		}

		return date('c', $timestamp);
	}

	/**
	 * get formatted date
	 *
	 * @param string $date
	 * @param bool $plain
	 * @return string
	 */
	public static function formatFromDB($date, $plain = false) {
		if (true === $plain) {
			return strtotime($date);
		}
		return date(PG::DEFAULT_DATETIMEFORMAT, strtotime($date));
	}

	/**
	 * @param int $month
	 * @return string
	 */
	public static function getMonthNameForMonth($month) {
		return isset(self::$monthNames[$month - 1]) ? self::$monthNames[$month - 1] : null;
	}

	/**
	 * @param int $month
	 * @return string
	 */
	public static function getShortMonthNameForMonth($month) {
		$monthName = self::getMonthNameForMonth($month);

		if (null !== $monthName) {
			$monthName = substr($monthName, 0, 3);
		}

		return $monthName;
	}

	/**
	 * @param string $fromDate
	 * @param string $toDate
	 * @return array
	 */
	public static function getMonthsBetween($fromDate, $toDate) {
		$start = new DateTime($fromDate);
		$interval = new DateInterval('P1M');
		$end = new DateTime($toDate);

		if ($fromDate === $toDate) {
			return array(
				array(
					'month' => $start->format('n'),
					'year' => $start->format('Y')
				)
			);
		}

		$period = new DatePeriod($start, $interval, $end);

		$between = array();

		foreach ($period as $dt) {
			$between[] = array(
				'month' => $dt->format('n'),
				'year' => $dt->format('Y')
			);
		}

		return $between;
	}

	/**
	 * @param string $startDate
	 * @param string $endDate
	 * @param array $holidays
	 * @return string
	 */
	public static function getWorkingDays($startDate, $endDate, $holidays = array()) {
		// do strtotime calculations just once
		$endDate = strtotime($endDate);
		$startDate = strtotime($startDate);

		//The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
		//We add one to inlude both dates in the interval.
		$days = ($endDate - $startDate) / 86400 + 1;

		$no_full_weeks = floor($days / 7);
		$no_remaining_days = fmod($days, 7);

		//It will return 1 if it's Monday,.. ,7 for Sunday
		$the_first_day_of_week = date("N", $startDate);
		$the_last_day_of_week = date("N", $endDate);

		//---->The two can be equal in leap years when february has 29 days, the equal sign is added here
		//In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
		if ($the_first_day_of_week <= $the_last_day_of_week) {
			if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week)
				$no_remaining_days--;
			if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week)
				$no_remaining_days--;
		}
		else {
			// (edit by Tokes to fix an edge case where the start day was a Sunday
			// and the end day was NOT a Saturday)
			// the day of the week for start is later than the day of the week for end
			if ($the_first_day_of_week == 7) {
				// if the start date is a Sunday, then we definitely subtract 1 day
				$no_remaining_days--;

				if ($the_last_day_of_week == 6) {
					// if the end date is a Saturday, then we subtract another day
					$no_remaining_days--;
				}
			} else {
				// the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
				// so we skip an entire weekend and subtract 2 days
				$no_remaining_days -= 2;
			}
		}

		//The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
		//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
		$workingDays = $no_full_weeks * 5;
		if ($no_remaining_days > 0) {
			$workingDays += $no_remaining_days;
		}

		//We subtract the holidays
		foreach ($holidays as $holiday) {
			$time_stamp = strtotime($holiday);
			//If the holiday doesn't fall in weekend
			if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
				$workingDays--;
		}

		return $workingDays;
	}

}
