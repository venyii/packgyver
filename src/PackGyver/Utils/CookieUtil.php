<?php

/**
 * PackGyver - Cookie Util
 */
class CookieUtil {

	/**
	 * create cookie
	 *
	 * @param string $cookieName
	 * @param mixed $cookieValue
	 * @param int $cookieExpire
	 * @param string $cookiePath
	 * @return bool
	 */
	public static function write($cookieName, $cookieValue, $cookieExpire = 86400, $cookiePath = "/") {
		if ('' != $cookieName) {
			return setcookie($cookieName, serialize($cookieValue), time() + $cookieExpire, $cookiePath);
		}
		return false;
	}

	/**
	 * delete cookie
	 *
	 * @param string $cookieName
	 * @param string $cookiePath
	 * @return bool
	 */
	public static function kill($cookieName, $cookiePath = "/") {
		if (isset($_COOKIE[$cookieName])) {
			return setcookie($cookieName, "", time() - 86400, $cookiePath);
		} else {
			return false;
		}
	}

	/**
	 * read cookie
	 *
	 * @param string $cookieName
	 * @return mixed or null
	 */
	public static function read($cookieName) {
		if (isset($_COOKIE[$cookieName])) {
			return unserialize($_COOKIE[$cookieName]);
		}
		return null;
	}

	/**
	 * check whether cookie is set
	 *
	 * @param string $cookieName
	 * @return bool
	 */
	public static function isValid($cookieName) {
		return isset($_COOKIE[$cookieName]);
	}

}
