<?php

/**
 * PackGyver - Browser Util
 */
class BrowserUtil {

	/**
	 * Key: BrowserName ::getBrowser()
	 * Value: Min. Version ::getVersion()
	 *
	 * @var array
	 */
	private static $validBrowsers = array(
		Browser::BROWSER_CHROME => 23,
		Browser::BROWSER_FIREFOX => 17,
		Browser::BROWSER_SAFARI => 5,
		Browser::BROWSER_OPERA => 12
	);

	/**
	 * check for safari/chrome/firefox/opera in LATEST version
	 * 
	 * @return bool
	 */
	public static function isAcceptable() {
		$browser = Browser::getInstance();

		if (isset(self::$validBrowsers[$browser->getBrowser()])) {
			$version = (int) $browser->getVersion();

			if ($version >= self::$validBrowsers[$browser->getBrowser()]) {
				return true;
			}
		}

		return false;
	}

	/**
	 * get browser information hash
	 *
	 * @return array
	 */
	public static function getInformation() {
		$browser = Browser::getInstance();

		return array(
			'name' => $browser->getBrowser(),
			'version' => $browser->getVersion(),
			'userAgent' => $browser->getUserAgent()
		);
	}

}
