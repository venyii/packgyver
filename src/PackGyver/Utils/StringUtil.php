<?php

/**
 * PackGyver - String Util
 */
class StringUtil {

	/**
	 * Check whether a string begins with $needle or not.
	 * 
	 * <code>
	 * StringUtil::beginsWith('Hal', 'Hallo'); -> true
	 * StringUtil::beginsWith('Halo', 'Hallo'); -> false
	 * </code>
	 *
	 * @param string $needle
	 * @param string $haystack
	 * @return bool
	 */
	public static function beginsWith($needle, $haystack) {
		return $needle == (substr($haystack, 0, strlen($needle)));
	}

	/**
	 * Check whether a string ends with $needle or not.
	 *
	 * <code>
	 * StringUtil::beginsWith('llo', 'Hallo'); -> true
	 * StringUtil::beginsWith('alo', 'Hallo'); -> false
	 * </code>
	 *
	 * @param string $needle
	 * @param string $haystack
	 * @return bool
	 */
	public static function endsWith($needle, $haystack) {
		return $needle == (substr($haystack, -strlen($needle)));
	}

	/**
	 * Check wheter a string contains a certain value or not.
	 *
	 * @param string $needle
	 * @param string $haystack
	 * @param bool $caseInsensitive
	 * @return bool
	 */
	public static function contains($needle, $haystack, $caseInsensitive = false) {
		if (false !== $caseInsensitive) {
			return stristr($haystack, $needle);
		}
		return strstr($haystack, $needle);
	}

	/**
	 * truncate string
	 *
	 * @param string $string
	 * @param int $length
	 * @param string $append
	 * @return string
	 */
	public static function truncate($string, $length, $append = '...') {
		if (strlen($string) > $length) {
			$string = substr($string, 0, $length) . $append;
		}
		return $string;
	}

}
