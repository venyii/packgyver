<?php

/**
 * PackGyver - Sort Util
 */
class SortUtil {

	const TYPE_ASC = 'asc';
	const TYPE_DESC = 'desc';

	/**
	 * @var string
	 */
	protected $key;

	/**
	 * runs the sort, and returns sorted array
	 *
	 * @param array $array
	 * @param string $key
	 * @param string $sort
	 * @return array 
	 */
	public function sort($array, $key, $sort = self::TYPE_DESC) {
		$this->key = $key;
		uasort($array, array($this, (self::TYPE_ASC === $sort) ? 'compare' : 'compareReverse'));

		return $array;
	}

	/**
	 * for ascending order
	 *
	 * @param array $x
	 * @param array $y
	 * @return int
	 */
	private function compare($x, $y) {
		if ($x[$this->key] == $y[$this->key]) {
			return 0;
		} else if ($x[$this->key] < $y[$this->key]) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * for descending order
	 *
	 * @param array $x
	 * @param array $y
	 * @return int
	 */
	private function compareReverse($x, $y) {
		if ($x[$this->key] == $y[$this->key]) {
			return 0;
		} else if ($x[$this->key] > $y[$this->key]) {
			return -1;
		} else {
			return 1;
		}
	}

}
