<?php

/**
 * PackGyver - Bijective
 */
class Bijective {

	/**
	 * @var string
	 */
	private $dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * CTOR 
	 */
	public function __construct() {
		$this->dictionary = str_split($this->dictionary);
	}

	/**
	 * @param int $i
	 * @return string
	 */
	public function encode($i) {
		if ($i == 0) {
			return $this->dictionary[0];
		}

		$result = array();
		$base = count($this->dictionary);

		while ($i > 0) {
			$result[] = $this->dictionary[($i % $base)];
			$i = floor($i / $base);
		}

		$result = array_reverse($result);

		return join("", $result);
	}

	/**
	 * @param string $input
	 * @return int
	 */
	public function decode($input) {
		$i = 0;
		$base = count($this->dictionary);

		$input = str_split($input);

		foreach ($input as $char) {
			$pos = array_search($char, $this->dictionary);

			$i = $i * $base + $pos;
		}

		return $i;
	}

}
