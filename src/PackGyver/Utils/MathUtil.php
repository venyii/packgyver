<?php

/**
 * PackGyver - Math Util
 */
class MathUtil {

	/**
	 * Check if the params are in the given range
	 * 
	 * <code>
	 * MathUtil::between(23, 50, 44); -> true
	 * MathUtil::between(23, 50, 23); -> true
	 * MathUtil::between(23, 50, 22); -> false
	 * </code>
	 *
	 * @param int $min
	 * @param int $max
	 * @param int $haystack
	 * @return bool
	 */
	public static function between($min, $max, $haystack) {
		if (is_numeric($min) && is_numeric($max) && is_numeric($haystack)) {
			if ($haystack >= $min && $haystack <= $max) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Generate a GUID
	 * 
	 * @see http://php.net/manual/en/function.uniqid.php#94959
	 * @return string
	 */
	public static function generateGUID() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
						// 32 bits for "time_low"
						mt_rand(0, 0xffff), mt_rand(0, 0xffff),
						// 16 bits for "time_mid"
						mt_rand(0, 0xffff),
						// 16 bits for "time_hi_and_version",
						// four most significant bits holds version number 4
						mt_rand(0, 0x0fff) | 0x4000,
						// 16 bits, 8 bits for "clk_seq_hi_res",
						// 8 bits for "clk_seq_low",
						// two most significant bits holds zero and one for variant DCE1.1
						mt_rand(0, 0x3fff) | 0x8000,
						// 48 bits for "node"
						mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

}
