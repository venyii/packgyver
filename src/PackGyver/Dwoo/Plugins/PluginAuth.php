<?php

/**
 * PackGyver - Auth Dwoo Plugin
 */
class PluginAuth extends \Dwoo\Plugin {

	/**
	 * @param string $function
	 * @param array|null $parameters
	 * @return string
	 */
	public function process($function, $parameters = null) {
		if ($function == 'getGoogleAuthLink') {
			return AuthenticationManager::getAuthenticationProviderForId('googleoauth')->getAuthUrl();
		}
		if ($function == 'authEnabledFor') {
			$authProvider = AuthenticationManager::getAuthenticationProviderForId($parameters[0]);

			return $authProvider ? $authProvider->isEnabled() : false;
		}
	}
}
