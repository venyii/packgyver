<?php

/**
 * PackGyver - Sidebar Dwoo Plugin
 */
class PluginWidget extends \Dwoo\Plugin {

	/**
	 * @param string $function
	 * @param array|null $parameters
	 * @return mixed
	 */
	public function process($function, $parameters = null) {
		switch ($function) {
			case 'getRankingData':
				$ranking = new RankingManager(PlayerCollection::create(true, null, false));
				return $ranking->getPlayerCollection()->toHash(true);

			case 'getMostSeasonPointsPlus':
				return WidgetManager::getMostSeasonPointsPlus();

			case 'getMostSeasonPointsMinus':
				return WidgetManager::getMostSeasonPointsMinus();

			case 'getSaisonMostMonsterPacks':
				return WidgetManager::getMostMonsterPacksRanking('season', $parameters[0]);

			case 'getAlltimeMostMonsterPacks':
				return WidgetManager::getMostMonsterPacksRanking('alltime', $parameters[0]);

			case 'getAlltimeMostMonsterPacksOwned':
				return WidgetManager::getMostMonsterPacksOwnedRanking('alltime', $parameters[0]);

			case 'getSaisonMostPacks':
				return WidgetManager::getMostPacksRanking('season', $parameters[0]);

			case 'getAlltimeMostPacks':
				return WidgetManager::getMostPacksRanking('alltime', $parameters[0]);

			case 'getSeasonLongestWinStreak':
				return WidgetManager::getLongestWinStreak('season', $parameters[0]);

			case 'getSeasonLongestLossStreak':
				return WidgetManager::getLongestLossStreak('season', $parameters[0]);

			case 'getAlltimeLongestWinStreak':
				return WidgetManager::getLongestWinStreak('alltime', $parameters[0]);

			case 'getAlltimeLongestLossStreak':
				return WidgetManager::getLongestLossStreak('alltime', $parameters[0]);

			case 'getAlltimeMostMatches':
				return WidgetManager::getMostMatchesPlayer('alltime', $parameters[0]);

			case 'getSeasonMostMatches':
				return WidgetManager::getMostMatchesPlayer('season');

			case 'getTodayMostMatches':
				return WidgetManager::getTodaysMostMatchesPlayer();

			case 'getAwardRanking':
				return WidgetManager::getAwardRanking($parameters[0]);

			case 'getAwardsGiven':
				return WidgetManager::getAwardsGiven($parameters[0]);
		}

		return null;
	}

}
