<?php

/**
 * PackGyver - json_encode
 */
class PluginJson_encode extends \Dwoo\Plugin {

	/**
	 * @param array $data
	 * @return string
	 */
	public function process(array $data) {
		return json_encode($data);
	}

}
