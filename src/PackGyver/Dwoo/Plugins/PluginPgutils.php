<?php

/**
 * PackGyver - PG Utils Dwoo Plugin
 */
class PluginPgutils extends \Dwoo\Plugin {

	/**
	 * @param string $function
	 * @param array $parameters
	 * @return int|null
	 */
	public function process($function, $parameters) {
		if ($function == 'calcPercents') {
			if (is_array($parameters)) {
				$a = (int) $parameters[0];
				$b = (int) $parameters[1];

				if (0 !== $a && 0 !== $b) {
					return round(($a / $b) * 100, 1);
				}
			}

			return 0;
		}

		return null;
	}

}
