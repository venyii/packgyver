<?php

/**
 * PackGyver - PG Utils Dwoo Plugin
 */
class PluginPgdate extends \Dwoo\Plugin {

	/**
	 * @param string $function
	 * @param array|string $parameters
	 * @return string|null
	 */
	public function process($function, $parameters) {
		if ($function == 'formatTimeago') {
			if (!is_numeric($parameters)) {
				$parameters = DateUtil::formatFromDB($parameters, true);
			}

			return DateUtil::formatISO8601($parameters);
		}

		return null;
	}

}
