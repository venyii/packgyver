<?php

/**
 * PackGyver - Player Dwoo Plugin
 */
class PluginMatch extends \Dwoo\Plugin {

	/**
	 * @param string $function
	 * @param int|null $matchId
	 * @return string|null
	 */
	public function process($function, $matchId = null) {
		if ($function == 'getPackType') {
			$match = MatchEntity::findById(PG::getDB(), $matchId);
			if ($match instanceof MatchEntity) {
				if ($match->getResultTeamRed() == 0 || $match->getResultTeamYellow() == 0) {
					return 'mp';
				} else if ($match->getResultTeamRed() < 5 || $match->getResultTeamYellow() < 5) {
					return 'p';
				}
			}
		}

		return null;
	}

}
