<?php

/**
 * PackGyver - Player Dwoo Plugin
 */
class PluginPlayer extends \Dwoo\Plugin {

    /**
     * @param string $function
     * @param int|null $playerId
     * @return array|bool|int|null|string
     */
    public function process($function, $playerId = null) {
		if ($function == 'isLoggedIn') {
			return AuthenticationManager::getInstance()->isLoggedIn();
		} else if ($function == 'isCheckedIn') {
			return 1 == AuthenticationManager::getInstance()->getPlayer()->getCheckedIn();
		} else if ($function == 'isAdmin') {
			return PlayerModel::isPlayerAdmin((null !== $playerId) ? $playerId : null);
		} else if ($function == 'getPlayerNameById') {
			return PlayerModel::getPlayerNameById(PG::getDB(), $playerId);
		} else if ($function == 'getFirstnamePersonal') {
			$player = PlayerEntity::findById(PG::getDB(), (int) $playerId);

			if ($player instanceof PlayerEntity) {
				$player = new PlayerModel($player);

				return $player->getFirstnamePersonal();
			}

			return null;
		} else if ($function == 'isStreakPositive') {
			$trend = $playerId;
			return StringUtil::beginsWith('+', $trend);
		} else if ($function == 'getRecentMatchesForPlayerId') {
			$player = PlayerEntity::findById(PG::getDB(), (int) $playerId);
			if ($player instanceof PlayerEntity) {
				return MatchManager::getRecentMatchesHashesForPlayer(PG::getDB(), $player, 6);
			}
			return array();
		} else if ($function == 'getHashForPlayerId') {
			$player = PlayerEntity::findById(PG::getDB(), (int) $playerId);
			if ($player instanceof PlayerEntity) {
				$playerModel = new PlayerModel($player);
				return $playerModel->toHash();
			}
			return array();
		} else if ($function == 'getCheckedPlayers') {
			$players = array();
			$inclusiveCurrentPlayer = (true == $playerId);

			foreach (PlayerModel::getPlayers(PG::getDB(), true, true, false) as $player) {
				if (false === $inclusiveCurrentPlayer) {
					if ($player->getId() == AuthenticationManager::getInstance()->getPlayer()->getId()) {
						continue;
					}
				}

				$playerModel = new PlayerModel($player);
				$players[] = $playerModel->toHash();
			}

			return $players;
        } else if ($function == 'getPlayers') {
            $players = array();
            $inclusiveCurrentPlayer = (true == $playerId);

            foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
                if (false === $inclusiveCurrentPlayer) {
                    if ($player->getId() == AuthenticationManager::getInstance()->getPlayer()->getId()) {
                        continue;
                    }
                }

                $playerModel = new PlayerModel($player);
                $players[] = $playerModel->toHash();
            }

            return $players;
		} else if ($function == 'getCheckedPlayersCount') {
			return PlayerModel::countCheckedinPlayers(PG::getDB());
		} else if ($function == 'getNotificationsHash') {
			$player = new PlayerModel(AuthenticationManager::getInstance()->getPlayer());
			return $player->getNotifications()->toHash();
		} else if ($function == 'countNotifications') {
			$player = new PlayerModel(AuthenticationManager::getInstance()->getPlayer());
			return $player->getNotifications()->count();
		}
	}
}
