<?php

/**
 * PackGyver - Dwoo Proxy
 *
 * @package dwoo
 */
class DwooFactory {

	/**
	 * @return \Dwoo\Core
	 */
	public static function create() {
		$compiledDir = PG::getCacheDir() . '/dwoo/compiled';
		$cacheDir = PG::getCacheDir() . '/dwoo/cache';

		if (!is_dir($compiledDir)) {
			mkdir($compiledDir, 0755, true);
		}

		if (!is_dir($cacheDir)) {
			mkdir($cacheDir, 0755, true);
		}

		$dwoo = new \Dwoo\Core($compiledDir, $cacheDir);
		$loader = new \Dwoo\Loader($cacheDir);
		$loader->addDirectory(__DIR__ . '/Plugins');

		$dwoo->setLoader($loader);

		return $dwoo;
	}

}
