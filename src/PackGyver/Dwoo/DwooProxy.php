<?php

/**
 * PackGyver - Dwoo Proxy
 *
 * @package dwoo
 */
class DwooProxy {

	const TPL_EXT = 'tpl';

	/**
	 * @var \Dwoo\Core
	 */
	private static $instance = null;

	/**
	 * @var \Dwoo\Core
	 */
	private static $dwoo = null;

	/**
	 * @var array
	 */
	private $params = array();

	/**
	 * @return DwooProxy
	 */
	public static function getInstance() {
		if (null === self::$instance) {
			self::$instance = new DwooProxy();
		}

		return self::$instance;
	}

	/**
	 * CTOR
	 *
	 * Set global dwoo params
	 */
	public function __construct() {
		$this->addDefaultParams();
	}

	/**
	 * Add the default dwoo parameters
	 */
	private function addDefaultParams() {
		$this->addParam('pgIsDevMode', PG::isDevMode());
		$this->addParam('pgIsGoogleAnalyticsEnabled', PG::isGoogleAnalyticsEnabled());
		$this->addParam('pgPageTitle', Router::instance()->getPageTitle());

		$this->addParam('pgWSService', WebSocketServiceManager::getInstance()->toHash());

		$this->addParam('pgUrl', PG::getSiteUrl());
		$this->addParam('pgIsMobile', MobileManager::isMobileDevice());

		if (AuthenticationManager::getInstance()->isLoggedIn()) {
			$playerModel = new PlayerModel(AuthenticationManager::getInstance()->getPlayer());
			$this->addParam('pgUser', $playerModel->toHash(true));
		} else {
			$this->addParam('pgUser', null);
		}
	}

	/**
	 * @return \Dwoo\Core
	 */
	public function getDwoo() {
		if (null === self::$dwoo) {
			self::$dwoo = DwooFactory::create();
		}

		return self::$dwoo;
	}

	/**
	 * @param string $tpl
	 * @param array $data
	 * @return mixed
	 */
	public function parseTemplate($tpl, $data = array()) {
		$data = array_merge($this->params, $data);

		return $this->getDwoo()->get(sprintf('%s/%s.%s', PG::getTemplateDir(), $tpl, self::TPL_EXT), $data);
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 */
	public function addParam($key, $value) {
		$this->params[$key] = $value;
	}

}
