<?php

/**
 * PackGyver - Observable Subject
 */
abstract class ObservableSubject implements SplSubject {

	/**
	 *
	 * @var \ObserverStorage
	 */
	private $storage;

	/**
	 * 
	 * @return \ObserverStorage
	 */
	public function getStorage() {
		if (null === $this->storage) {
			$this->storage = new ObserverStorage();
		}

		return $this->storage;
	}

	/**
	 * 
	 * @param \ObserverStorage $storage
	 */
	public function setStorage(\ObserverStorage $storage) {
		$this->storage = $storage;
	}

	/**
	 * 
	 * @param SplObserver $observer
	 */
	public function attach(SplObserver $observer) {
		$this->getStorage()->attach($observer);
	}

	/**
	 * 
	 * @param SplObserver $observer
	 */
	public function detach(SplObserver $observer) {
		$this->getStorage()->detach($observer);
	}

	/**
	 * Notify an observer
	 */
	public function notify() {
		foreach ($this->getStorage() as $observer) {
			/* @var $observer SplObserver */
			$observer->update($this);
		}
	}

}
