<?php

/**
 * PackGyver - WinStreakObserver
 */
class WinStreakObserver implements SplObserver {

	/**
	 *
	 * @param SplSubject $subject
	 */
	public function update(SplSubject $subject) {
		$db = PG::getDB();
		if ($subject instanceof MatchModel) {
			if ($subject->getScoreRed() > $subject->getScoreYellow()) {
				// red wins
				if ($subject->getType() === MatchModel::TYPE_1ON1) {
					$playerAll = $subject->getRedSinglePlayer();
					/* @var $playerAll PlayerModel */
				} else {
					$playerGoal = $subject->getRedGoalPlayer();
					/* @var $playerGoal PlayerModel */
					$playerOffense = $subject->getRedOffensePlayer();
					/* @var $playerOffense PlayerModel */
				}
			} else {
				// yellow wins
				if ($subject->getType() === MatchModel::TYPE_1ON1) {
					$playerAll = $subject->getYellowSinglePlayer();
					/* @var $playerAll PlayerModel */
				} else {
					$playerGoal = $subject->getYellowGoalPlayer();
					/* @var $playerGoal PlayerModel */
					$playerOffense = $subject->getYellowOffensePlayer();
					/* @var $playerOffense PlayerModel */
				}
			}

			$awards = AwardManager::getAwardsByKey($db, AwardManager::KEY_AWARD_STREAK_WIN);
			if ($subject->getType() === MatchModel::TYPE_1ON1) {
				$playerStreaks = $playerAll->getStats(PlayerStatistics::CACHETYPE_SEASON)->getStreaks();
			} else {
				$playerGoalStreaks = $playerGoal->getStats(PlayerStatistics::CACHETYPE_SEASON)->getStreaks();
				$playerOffenseStreaks = $playerOffense->getStats(PlayerStatistics::CACHETYPE_SEASON)->getStreaks();
			}

			foreach ($awards as $award) {
				if ($award instanceof AwardEntity) {
					$awardKeys = explode('::', $award->getKey());
					$streak = end($awardKeys);
					if (is_numeric($streak)) {
						$streak = intval($streak);
						if ($subject->getType() === MatchModel::TYPE_1ON1) {
							if ($playerStreaks['currentWinStreak'] >= $streak) {
								$this->addPlayerAward($db, $playerAll, $award);
							}
						} else {
							if ($playerGoalStreaks['currentWinStreak'] >= $streak) {
								$this->addPlayerAward($db, $playerGoal, $award);
							}

							if ($playerOffenseStreaks['currentWinStreak'] >= $streak) {
								$this->addPlayerAward($db, $playerOffense, $award);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * add award for player
	 * 
	 * @param PDO $db
	 * @param PlayerModel $player
	 * @param AwardEntity $award 
	 */
	private function addPlayerAward(PDO $db, PlayerModel $player, AwardEntity $award) {
		if (!AwardManager::hasPlayerAward($db, $player->getEntity(), $award)) {
			$comment = DateUtil::getMonthNameForMonth(date("m")) . " '" . date("y");
			AwardManager::addPlayerAward($db, $player->getEntity(), $award, $comment, null, true);
		}
	}

}

?>