<?php

/**
 * PackGyver - XMPPHP Base
 */
class XMPPHPBase {

	/**
	 *
	 * @var XMPPHP_XMPP
	 */
	private $xmpp = null;

	/**
	 *
	 * @return XMPPHP_XMPP
	 */
	public function getXMPP() {
		return $this->xmpp;
	}

	/**
	 * CTOR
	 *
	 * @param XMPPHPConfiguration $config
	 */
	public function __construct(XMPPHPConfiguration $config) {
		$this->xmpp = new XMPPHP_XMPP(
						$config->getHost(),
						$config->getPort(),
						$config->getUser(),
						$config->getPassword(),
						$config->getResource(),
						$config->getServer(),
						$config->getPrintLog(),
						$config->getLogLevel()
		);

		try {
			$this->xmpp->connect();
			$this->xmpp->processUntil('session_start');
			$this->xmpp->presence();
		} catch (XMPPHP_Exception $e) {
			// Exception Log...
		}
	}

	/**
	 * disconnect socket earlier
	 */
	public function disconnect() {
		$this->xmpp->disconnect();
	}

	/**
	 * DTOR
	 * 
	 * close connection
	 */
	public function __destruct() {
		$this->xmpp->disconnect();
	}

}
