<?php

/**
 * WebSocket Service - Manager
 *
 * @package websocket
 */
class WebSocketServiceManager {

	const STANDARD_SERVICE = 'beaconpush';

	/**
	 *
	 * @var bool
	 */
	private $enabled;

	/**
	 *
	 * @var \WebSocketServiceProviderInterface
	 */
	private $serviceProvider;

	/**
	 *
	 * @var \WebSocketServiceDataDispatcher
	 */
	private $dataDispatcher;

	/**
	 *
	 * @var string
	 */
	private $services = array();

	/**
	 *
	 * @var SettingsManager
	 */
	private $settingsManager;

	/**
	 *
	 * @var WebSocketServiceManager
	 */
	private static $instance;

	/**
	 * 
	 * @return WebSocketServiceManager
	 */
	public static function getInstance() {
		if (null === self::$instance) {
			self::$instance = new WebSocketServiceManager();
		}

		return self::$instance;
	}

	/**
	 * CTOR
	 * 
	 * Register Services
	 */
	public function __construct() {
		$this->settingsManager = new SettingsManager('wsservice');
		$this->dataDispatcher = new WebSocketServiceDataDispatcher($this);
		$this->registerServices();
	}

	/**
	 * 
	 * @return \WebSocketServiceDataDispatcher
	 */
	public function getDataDispatcher() {
		return $this->dataDispatcher;
	}

	/**
	 * Register the default WebSocket services
	 * 
	 * @return void
	 */
	private function registerServices() {
		$this->registerService(WebSocketServiceProviderPusher::SERVICE_KEY, 'WebSocketServiceProviderPusher');
		$this->registerService(WebSocketServiceProviderBeaconPush::SERVICE_KEY, 'WebSocketServiceProviderBeaconPush');
		$this->registerService(WebSocketServiceProviderVocket::SERVICE_KEY, 'WebSocketServiceProviderVocket');
	}

	/**
	 *
	 * @return bool
	 */
	public function isEnabled() {
		if (null === $this->enabled) {
			$enabled = $this->settingsManager->getValue('isEnabled', null);

			if (null === $enabled) {
				$this->settingsManager->setValue('isEnabled', 0);
				$enabled = 0;
			}

			$this->enabled = 0 !== (int) $enabled;
		}

		return $this->enabled;
	}

	/**
	 * 
	 * @param string $key
	 * @param string $class
	 */
	public function registerService($key, $class) {
		$this->services[$key] = $class;
	}

	/**
	 * 
	 * @param string $key
	 * @return string|null
	 */
	public function getService($key) {
		if (isset($this->services[$key])) {
			return $this->services[$key];
		}

		return null;
	}

	/**
	 * 
	 * @param string $key
	 * @return bool
	 */
	public function existsService($key) {
		return isset($this->services[$key]);
	}

	/**
	 * 
	 * @return \WebSocketServiceProviderInterface
	 * @throws \WebSocketServiceException
	 */
	public function getServiceProvider() {
		$service = null;

		if (null === $this->serviceProvider) {
			$service = $this->fetchServiceFromSettings();

			if ($service && $this->existsService($service)) {
				$serviceClass = $this->getService($service);

				$this->serviceProvider = new $serviceClass;
			}
		}

		if (null === $this->serviceProvider && $service) {
            $this->resetService();

			throw new WebSocketServiceException(sprintf('WebSocketService %s not found', $service));
		} else if (null === $this->serviceProvider) {
            throw new WebSocketServiceException('No WebSocketService found');
        }

		return $this->serviceProvider;
	}

	/**
	 * 
	 * @return string
	 */
	private function fetchServiceFromSettings() {
		if (null === ($service = $this->settingsManager->getValue('service'))) {
			$this->settingsManager->setValue('service', self::STANDARD_SERVICE);
			$service = self::STANDARD_SERVICE;
		}

		return $service;
	}

    private function resetService() {
        $this->settingsManager->setValue('isEnabled', 0);
        $this->settingsManager->setValue('service', '');
    }

	/**
	 * 
	 * @return array
	 */
	public function toHash() {
		$hash = array();

		$hash['enabled'] = $this->isEnabled();

		if ($this->isEnabled()) {
			$hash['service'] = $this->getServiceProvider()->toHash();
		} else {
			$hash['service'] = null;
		}

		return $hash;
	}

}
