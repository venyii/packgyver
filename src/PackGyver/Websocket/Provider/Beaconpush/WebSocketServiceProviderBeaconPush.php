<?php

/**
 * WebSocket Service - BeaconPush Provider
 *
 * @package websocket.beaconpush
 */
class WebSocketServiceProviderBeaconPush implements WebSocketServiceProviderInterface {

	const SERVICE_KEY = 'beaconpush';
	const API_BASE_URL = 'http://api.beaconpush.com/%s/%s/%s';

	/* commands */
	const CMD_USERS = 'users';
	const CMD_CHANNELS = 'channels';

	/* methods */
	const METHOD_POST = 'POST';
	const METHOD_GET = 'GET';
	const METHOD_DELETE = 'DELETE';

	/**
	 * The configuration object
	 *
	 * @var \WebSocketServiceConfigurationBeaconPush
	 */
	private $configuration;

	/**
	 * The available commands
	 *
	 * @var array
	 */
	private $commands = array(
		self::CMD_USERS,
		self::CMD_CHANNELS
	);

	/**
	 * The available methods
	 *
	 * @var array
	 */
	private $methods = array(
		self::METHOD_POST,
		self::METHOD_GET,
		self::METHOD_DELETE
	);

	/**
	 * CTOR
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Initialize the provider, load parameters from settings
	 */
	public function init() {
		$settings = SettingsManager::getGroup('beaconpush');

		$apiKey = $settings->getValue('apiKey');
		$apiSecret = $settings->getValue('apiSecret');
		$requestTimeout = $settings->getValue('requestTimeout');

		if (!$apiKey) {
			$settings->setValue('apiKey', '');
		}
		if (!$apiSecret) {
			$settings->setValue('apiSecret', '');
		}
		if (!$requestTimeout) {
			$settings->setValue('requestTimeout', 30);
		}

		$this->configuration = new WebSocketServiceConfigurationBeaconPush();
		$this->configuration->setApiKey($apiKey)
				->setApiSecret($apiSecret)
				->setRequestTimeout($requestTimeout);
	}

	/**
	 * Get the service configuration
	 * 
	 * @return \WebSocketServiceConfigurationBeaconPush
	 */
	public function getConfiguration() {
		return $this->configuration;
	}

	/**
	 * Check if the passed method is valid
	 * 
	 * @param string $method
	 * @return bool
	 */
	protected function isValidMethod($method) {
		return in_array($method, $this->methods);
	}

	/**
	 * Check if the passed command is valid
	 * 
	 * @param string $cmd
	 * @return bool
	 */
	protected function isValidCommand($cmd) {
		return in_array($cmd, $this->commands);
	}

	/**
	 * Push data to a channel
	 * 
	 * @param string $channel
	 * @param string $eventName
	 * @param array $data
	 */
	public function pushToChannel($channel, $eventName, array $data = array()) {
		return $this->doRequest(self::CMD_CHANNELS, self::METHOD_POST, $channel, array('channel' => $channel, 'name' => $eventName, 'data' => $data));
	}

	/**
	 * Execute the request
	 * 
	 * @param string $cmd
	 * @param string $method
	 * @param string $param
	 * @param array $data
	 * @return string
	 * @throws WebSocketServiceException
	 */
	protected function doRequest($cmd, $method, $param = null, array $data = array()) {
		if (!$this->isValidMethod($method)) {
			throw new WebSocketServiceException(sprintf('Method \'%s\' not found.', $method));
		}
		if (!$this->isValidCommand($cmd)) {
			throw new WebSocketServiceException(sprintf('Command \'%s\' not found.', $cmd));
		}

		$apiUrl = sprintf(self::API_BASE_URL, $this->getConfiguration()->getApiVersion(), $this->getConfiguration()->getApiKey(), $cmd);

		if (null !== $param) {
			if (is_array($param)) {
				$apiUrl .= '/' . implode('/', $param);
			} else {
				$apiUrl .= '/' . $param;
			}
		}

		$headers = array(
			'X-Beacon-Secret-Key: ' . $this->getConfiguration()->getApiSecret(),
		);

		$ch = curl_init($apiUrl);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->getConfiguration()->getRequestTimeout());
		curl_setopt($ch, CURLOPT_VERBOSE, 0);

		switch ($method) {
			case self::METHOD_POST:
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
				break;
			case self::METHOD_GET:
				curl_setopt($ch, CURLOPT_HTTPGET, true);
				break;
			case self::METHOD_DELETE:
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, self::METHOD_DELETE);
				break;
		}

		$result = curl_exec($ch);

		if (false === $result) {
			throw new WebSocketServiceException('cURL request failed: ' . curl_error($ch));
		}

		curl_close($ch);

		return ('' !== $result) ? json_decode($result) : null;
	}

	/**
	 * 
	 * @return array
	 */
	public function toHash() {
		return array(
			'name' => self::SERVICE_KEY,
			'config' => $this->getConfiguration()->toHash(),
			'options' => $this->getConfiguration()->getOptionsHash()
		);
	}

}
