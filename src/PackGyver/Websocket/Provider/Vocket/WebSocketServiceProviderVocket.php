<?php

/**
 * WebSocket Service - Pusher Provider
 *
 * @package websocket.pusher
 */
class WebSocketServiceProviderVocket implements WebSocketServiceProviderInterface {

	const SERVICE_KEY = 'vocket';

	/**
	 *
	 * @var \WebSocketServiceConfigurationVocket
	 */
	private $configuration;

	/**
	 * CTOR
	 *
	 * Initialize the configuration
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Initialize the provider, load parameters from settings
	 */
	public function init() {
        if (!extension_loaded('zmq')) {
            throw new RuntimeException('The ZMQ extension is not installed!');
        }

		$settings = SettingsManager::getGroup(self::SERVICE_KEY);

		$host = $settings->getValue('host');
		$port = $settings->getValue('port');
		$internalPort = $settings->getValue('internalPort');

        if (!$host) {
            $settings->setValue('host', '');
        }
        if (!$port) {
            $settings->setValue('port', '');
        }
        if (!$internalPort) {
            $settings->setValue('internalPort', '');
        }

		$this->configuration = new WebSocketServiceConfigurationVocket();
		$this->configuration->setHost($host)
				->setPort($port)
                ->setInternalPort($internalPort);
	}

	/**
	 * Get the service configuration
	 *
	 * @return \WebSocketServiceConfigurationVocket
	 */
	public function getConfiguration() {
		return $this->configuration;
	}

	/**
	 * Push data to a channel
	 *
	 * @param string $channel
	 * @param string $eventName
	 * @param array $data
	 */
	public function pushToChannel($channel, $eventName, array $data = array()) {
        $payload = array(
            'channel' => $channel,
            'event' => $eventName,
            'data' => $data
        );
error_log('call! '.uniqid());

        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, self::SERVICE_KEY);
        $socket->connect(sprintf('tcp://%s:%s', $this->configuration->getHost(), $this->configuration->getInternalPort()));

        $socket->send(json_encode($payload));
	}

	/**
	 *
	 * @return array
	 */
	public function toHash() {
		return array(
			'name' => self::SERVICE_KEY,
			'config' => $this->getConfiguration()->toHash(),
			'options' => $this->getConfiguration()->getOptionsHash()
		);
	}

}

?>