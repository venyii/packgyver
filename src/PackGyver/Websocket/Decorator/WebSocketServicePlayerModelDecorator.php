<?php

/**
 * PackGyver - Player Model Decorator
 *
 * @package websocket
 */
class WebSocketServicePlayerModelDecorator extends PlayerModel {

	/**
	 * 
	 * @param bool $withStats
	 * @return array
	 */
	public function toHash($withStats = false) {
		return array(
			'id' => $this->getId(),
			'firstname' => $this->getFirstname(),
			'lastname' => $this->getLastname(),
			'shortName' => $this->getNameShort(),
			'avatar' => self::getAvatarForPlayerId($this->getId()),
			'uid' => $this->getUrlIdentifier()
		);
	}

}

?>