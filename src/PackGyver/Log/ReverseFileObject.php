<?php

/**
 * PackGyver - Reverse File Object
 */
class ReverseFileObject extends \SplFileObject {

	/**
	 * @var int
	 */
	private $lineCount;

	/**
	 * @var int
	 */
	private $loc;

	/**
	 * {@inheritdoc}
	 */
	public function __construct($filename, $use_include_path = true) {
		parent::__construct($filename, 'r', $use_include_path);
		$this->lineCount = 0;
		while (!$this->eof()) {
			$this->seek($this->lineCount);
			++$this->lineCount;
		}
		$this->rewind();
	}

	/**
	 * {@inheritdoc}
	 */
	public function next() {
		$this->loc--;
		if (!$this->valid()) {
			return;
		}
		$this->seek($this->loc);
	}

	/**
	 * {@inheritdoc}
	 */
	public function key() {
		return $this->loc;
	}

	/**
	 * {@inheritdoc}
	 */
	public function valid() {
		return !($this->loc < 0 || $this->loc > $this->lineCount);
	}

	/**
	 * {@inheritdoc}
	 */
	public function rewind() {
		$this->seek($this->lineCount);
		$this->loc = $this->lineCount;
	}
}