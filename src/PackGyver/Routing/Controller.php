<?php

/**
 * PackGyver - Controller Interface
 */
abstract class Controller {

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string
	 */
	private $method;

	/**
	 * @var array
	 */
	private $parameters;

	/**
	 * @return string
	 */
	public function getMetaTitle() {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setMetaTitle($title) {
		$this->title = $title;
	}

	/**
	 * @param int $index
	 * @return string
	 */
	protected function getParameter($index) {
		return isset($this->parameters[$index]) ? $this->parameters[$index] : null;
	}

	/**
	 * @return array
	 */
	protected function getParameters() {
		return $this->parameters;
	}

	/**
	 * @param array $parameters
	 */
	public function setParameters($parameters) {
		$this->parameters = $parameters;
	}

	/**
	 * @return string
	 */
	public function getMethod() {
		return $this->method;
	}

	/**
	 * @param string $method
	 */
	public function setMethod($method) {
		$this->method = $method;
	}

	/**
	 * @return string
	 */
	abstract public function actionDefault();

	/**
	 * @return Request
	 */
	protected function getRequest() {
		return Request::getInstance();
	}

	/**
	 * @param string $tpl
	 * @param array $data
	 * @return string
	 */
	public function render($tpl, $data) {
		return DwooProxy::getInstance()->parseTemplate($tpl, $data);
	}

	/**
	 * redirect to the given URI and exit
	 *
	 * @param string $uri
	 */
	protected function redirect($uri) {
		UrlUtil::redirect($uri);
	}

}
