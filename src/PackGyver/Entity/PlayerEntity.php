<?php

/**
 * 
 *
 * @version 1.102
 * @package entity
 */
class PlayerEntity extends Db2PhpEntityBase implements Db2PhpEntityModificationTracking {
	private static $CLASS_NAME='PlayerEntity';
	const SQL_IDENTIFIER_QUOTE='`';
	const SQL_TABLE_NAME='player';
	const SQL_INSERT='INSERT INTO `player` (`id`,`email`,`password`,`firstname`,`lastname`,`gtalk`,`type`,`active`,`checkedIn`,`deleted`,`lastActivity`,`dateRegistered`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
	const SQL_INSERT_AUTOINCREMENT='INSERT INTO `player` (`email`,`password`,`firstname`,`lastname`,`gtalk`,`type`,`active`,`checkedIn`,`deleted`,`lastActivity`,`dateRegistered`) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
	const SQL_UPDATE='UPDATE `player` SET `id`=?,`email`=?,`password`=?,`firstname`=?,`lastname`=?,`gtalk`=?,`type`=?,`active`=?,`checkedIn`=?,`deleted`=?,`lastActivity`=?,`dateRegistered`=? WHERE `id`=?';
	const SQL_SELECT_PK='SELECT * FROM `player` WHERE `id`=?';
	const SQL_DELETE_PK='DELETE FROM `player` WHERE `id`=?';
	const FIELD_ID=-1872091320;
	const FIELD_EMAIL=-1325504945;
	const FIELD_PASSWORD=267435784;
	const FIELD_FIRSTNAME=762510958;
	const FIELD_LASTNAME=1886770830;
	const FIELD_GTALK=-1323449274;
	const FIELD_TYPE=511889959;
	const FIELD_ACTIVE=1735834291;
	const FIELD_CHECKEDIN=1027806303;
	const FIELD_DELETED=688548364;
	const FIELD_LASTACTIVITY=822409650;
	const FIELD_DATEREGISTERED=1501161341;
	private static $PRIMARY_KEYS=array(self::FIELD_ID);
	private static $AUTOINCREMENT_FIELDS=array(self::FIELD_ID);
	private static $FIELD_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_EMAIL=>'email',
		self::FIELD_PASSWORD=>'password',
		self::FIELD_FIRSTNAME=>'firstname',
		self::FIELD_LASTNAME=>'lastname',
		self::FIELD_GTALK=>'gtalk',
		self::FIELD_TYPE=>'type',
		self::FIELD_ACTIVE=>'active',
		self::FIELD_CHECKEDIN=>'checkedIn',
		self::FIELD_DELETED=>'deleted',
		self::FIELD_LASTACTIVITY=>'lastActivity',
		self::FIELD_DATEREGISTERED=>'dateRegistered');
	private static $PROPERTY_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_EMAIL=>'email',
		self::FIELD_PASSWORD=>'password',
		self::FIELD_FIRSTNAME=>'firstname',
		self::FIELD_LASTNAME=>'lastname',
		self::FIELD_GTALK=>'gtalk',
		self::FIELD_TYPE=>'type',
		self::FIELD_ACTIVE=>'active',
		self::FIELD_CHECKEDIN=>'checkedIn',
		self::FIELD_DELETED=>'deleted',
		self::FIELD_LASTACTIVITY=>'lastActivity',
		self::FIELD_DATEREGISTERED=>'dateRegistered');
	private static $PROPERTY_TYPES=array(
		self::FIELD_ID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_EMAIL=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_PASSWORD=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_FIRSTNAME=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_LASTNAME=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_GTALK=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_TYPE=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_ACTIVE=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_CHECKEDIN=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_DELETED=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_LASTACTIVITY=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_DATEREGISTERED=>Db2PhpEntity::PHP_TYPE_STRING);
	private static $FIELD_TYPES=array(
		self::FIELD_ID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_EMAIL=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,120,0,false),
		self::FIELD_PASSWORD=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,false),
		self::FIELD_FIRSTNAME=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,false),
		self::FIELD_LASTNAME=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,false),
		self::FIELD_GTALK=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,120,0,false),
		self::FIELD_TYPE=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_ACTIVE=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_CHECKEDIN=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_DELETED=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_LASTACTIVITY=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,false),
		self::FIELD_DATEREGISTERED=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,true));
	private static $DEFAULT_VALUES=array(
		self::FIELD_ID=>null,
		self::FIELD_EMAIL=>'',
		self::FIELD_PASSWORD=>'',
		self::FIELD_FIRSTNAME=>'',
		self::FIELD_LASTNAME=>'',
		self::FIELD_GTALK=>'',
		self::FIELD_TYPE=>'0',
		self::FIELD_ACTIVE=>'0',
		self::FIELD_CHECKEDIN=>'0',
		self::FIELD_DELETED=>'0',
		self::FIELD_LASTACTIVITY=>'',
		self::FIELD_DATEREGISTERED=>null);
	private $id;
	private $email;
	private $password;
	private $firstname;
	private $lastname;
	private $gtalk;
	private $type;
	private $active;
	private $checkedIn;
	private $deleted;
	private $lastActivity;
	private $dateRegistered;

	/**
	 * set value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @param mixed $id
	 * @return PlayerEntity
	 */
	public function &setId($id) {
		$this->notifyChanged(self::FIELD_ID,$this->id,$id);
		$this->id=$id;
		return $this;
	}

	/**
	 * get value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * set value for email 
	 *
	 * type:VARCHAR,size:120,default:null
	 *
	 * @param mixed $email
	 * @return PlayerEntity
	 */
	public function &setEmail($email) {
		$this->notifyChanged(self::FIELD_EMAIL,$this->email,$email);
		$this->email=$email;
		return $this;
	}

	/**
	 * get value for email 
	 *
	 * type:VARCHAR,size:120,default:null
	 *
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * set value for password 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @param mixed $password
	 * @return PlayerEntity
	 */
	public function &setPassword($password) {
		$this->notifyChanged(self::FIELD_PASSWORD,$this->password,$password);
		$this->password=$password;
		return $this;
	}

	/**
	 * get value for password 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @return mixed
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * set value for firstname 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @param mixed $firstname
	 * @return PlayerEntity
	 */
	public function &setFirstname($firstname) {
		$this->notifyChanged(self::FIELD_FIRSTNAME,$this->firstname,$firstname);
		$this->firstname=$firstname;
		return $this;
	}

	/**
	 * get value for firstname 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @return mixed
	 */
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	 * set value for lastname 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @param mixed $lastname
	 * @return PlayerEntity
	 */
	public function &setLastname($lastname) {
		$this->notifyChanged(self::FIELD_LASTNAME,$this->lastname,$lastname);
		$this->lastname=$lastname;
		return $this;
	}

	/**
	 * get value for lastname 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @return mixed
	 */
	public function getLastname() {
		return $this->lastname;
	}

	/**
	 * set value for gtalk 
	 *
	 * type:VARCHAR,size:120,default:null
	 *
	 * @param mixed $gtalk
	 * @return PlayerEntity
	 */
	public function &setGtalk($gtalk) {
		$this->notifyChanged(self::FIELD_GTALK,$this->gtalk,$gtalk);
		$this->gtalk=$gtalk;
		return $this;
	}

	/**
	 * get value for gtalk 
	 *
	 * type:VARCHAR,size:120,default:null
	 *
	 * @return mixed
	 */
	public function getGtalk() {
		return $this->gtalk;
	}

	/**
	 * set value for type 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $type
	 * @return PlayerEntity
	 */
	public function &setType($type) {
		$this->notifyChanged(self::FIELD_TYPE,$this->type,$type);
		$this->type=$type;
		return $this;
	}

	/**
	 * get value for type 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * set value for active 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $active
	 * @return PlayerEntity
	 */
	public function &setActive($active) {
		$this->notifyChanged(self::FIELD_ACTIVE,$this->active,$active);
		$this->active=$active;
		return $this;
	}

	/**
	 * get value for active 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * set value for checkedIn 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $checkedIn
	 * @return PlayerEntity
	 */
	public function &setCheckedIn($checkedIn) {
		$this->notifyChanged(self::FIELD_CHECKEDIN,$this->checkedIn,$checkedIn);
		$this->checkedIn=$checkedIn;
		return $this;
	}

	/**
	 * get value for checkedIn 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getCheckedIn() {
		return $this->checkedIn;
	}

	/**
	 * set value for deleted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $deleted
	 * @return PlayerEntity
	 */
	public function &setDeleted($deleted) {
		$this->notifyChanged(self::FIELD_DELETED,$this->deleted,$deleted);
		$this->deleted=$deleted;
		return $this;
	}

	/**
	 * get value for deleted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getDeleted() {
		return $this->deleted;
	}

	/**
	 * set value for lastActivity 
	 *
	 * type:DATETIME,size:19,default:null
	 *
	 * @param mixed $lastActivity
	 * @return PlayerEntity
	 */
	public function &setLastActivity($lastActivity) {
		$this->notifyChanged(self::FIELD_LASTACTIVITY,$this->lastActivity,$lastActivity);
		$this->lastActivity=$lastActivity;
		return $this;
	}

	/**
	 * get value for lastActivity 
	 *
	 * type:DATETIME,size:19,default:null
	 *
	 * @return mixed
	 */
	public function getLastActivity() {
		return $this->lastActivity;
	}

	/**
	 * set value for dateRegistered 
	 *
	 * type:DATETIME,size:19,default:null,nullable
	 *
	 * @param mixed $dateRegistered
	 * @return PlayerEntity
	 */
	public function &setDateRegistered($dateRegistered) {
		$this->notifyChanged(self::FIELD_DATEREGISTERED,$this->dateRegistered,$dateRegistered);
		$this->dateRegistered=$dateRegistered;
		return $this;
	}

	/**
	 * get value for dateRegistered 
	 *
	 * type:DATETIME,size:19,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getDateRegistered() {
		return $this->dateRegistered;
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public static function getTableName() {
		return self::SQL_TABLE_NAME;
	}

	/**
	 * Get array with field id as index and field name as value
	 *
	 * @return array
	 */
	public static function getFieldNames() {
		return self::$FIELD_NAMES;
	}

	/**
	 * Get array with field id as index and property name as value
	 *
	 * @return array
	 */
	public static function getPropertyNames() {
		return self::$PROPERTY_NAMES;
	}

	/**
	 * get the field name for the passed field id.
	 *
	 * @param int $fieldId
	 * @param bool $fullyQualifiedName true if field name should be qualified by table name
	 * @return string field name for the passed field id, null if the field doesn't exist
	 */
	public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName=true) {
		if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
			return null;
		}
		$fieldName=self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
		if ($fullyQualifiedName) {
			return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
		}
		return $fieldName;
	}

	/**
	 * Get array with field ids of identifiers
	 *
	 * @return array
	 */
	public static function getIdentifierFields() {
		return self::$PRIMARY_KEYS;
	}

	/**
	 * Get array with field ids of autoincrement fields
	 *
	 * @return array
	 */
	public static function getAutoincrementFields() {
		return self::$AUTOINCREMENT_FIELDS;
	}

	/**
	 * Get array with field id as index and property type as value
	 *
	 * @return array
	 */
	public static function getPropertyTypes() {
		return self::$PROPERTY_TYPES;
	}

	/**
	 * Get array with field id as index and field type as value
	 *
	 * @return array
	 */
	public static function getFieldTypes() {
		return self::$FIELD_TYPES;
	}

	/**
	 * Assign default values according to table
	 * 
	 */
	public function assignDefaultValues() {
		$this->assignByArray(self::$DEFAULT_VALUES);
	}


	/**
	 * return hash with the field name as index and the field value as value.
	 *
	 * @return array
	 */
	public function toHash() {
		$array=$this->toArray();
		$hash=array();
		foreach ($array as $fieldId=>$value) {
			$hash[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		return $hash;
	}

	/**
	 * return array with the field id as index and the field value as value.
	 *
	 * @return array
	 */
	public function toArray() {
		return array(
			self::FIELD_ID=>$this->getId(),
			self::FIELD_EMAIL=>$this->getEmail(),
			self::FIELD_PASSWORD=>$this->getPassword(),
			self::FIELD_FIRSTNAME=>$this->getFirstname(),
			self::FIELD_LASTNAME=>$this->getLastname(),
			self::FIELD_GTALK=>$this->getGtalk(),
			self::FIELD_TYPE=>$this->getType(),
			self::FIELD_ACTIVE=>$this->getActive(),
			self::FIELD_CHECKEDIN=>$this->getCheckedIn(),
			self::FIELD_DELETED=>$this->getDeleted(),
			self::FIELD_LASTACTIVITY=>$this->getLastActivity(),
			self::FIELD_DATEREGISTERED=>$this->getDateRegistered());
	}


	/**
	 * return array with the field id as index and the field value as value for the identifier fields.
	 *
	 * @return array
	 */
	public function getPrimaryKeyValues() {
		return array(
			self::FIELD_ID=>$this->getId());
	}

	/**
	 * cached statements
	 *
	 * @var array<string,array<string,PDOStatement>>
	 */
	private static $stmts=array();
	private static $cacheStatements=true;
	
	/**
	 * prepare passed string as statement or return cached if enabled and available
	 *
	 * @param PDO $db
	 * @param string $statement
	 * @return PDOStatement
	 */
	protected static function prepareStatement(PDO $db, $statement) {
		if(self::isCacheStatements()) {
			if (in_array($statement, array(self::SQL_INSERT, self::SQL_INSERT_AUTOINCREMENT, self::SQL_UPDATE, self::SQL_SELECT_PK, self::SQL_DELETE_PK))) {
				$dbInstanceId=spl_object_hash($db);
				if (null===self::$stmts[$statement][$dbInstanceId]) {
					self::$stmts[$statement][$dbInstanceId]=$db->prepare($statement);
				}
				return self::$stmts[$statement][$dbInstanceId];
			}
		}
		return $db->prepare($statement);
	}

	/**
	 * Enable statement cache
	 *
	 * @param bool $cache
	 */
	public static function setCacheStatements($cache) {
		self::$cacheStatements=true==$cache;
	}

	/**
	 * Check if statement cache is enabled
	 *
	 * @return bool
	 */
	public static function isCacheStatements() {
		return self::$cacheStatements;
	}

	/**
	 * Query by Example.
	 *
	 * Match by attributes of passed example instance and return matched rows as an array of PlayerEntity instances
	 *
	 * @param PDO $db a PDO Database instance
	 * @param PlayerEntity $example an example instance defining the conditions. All non-null properties will be considered a constraint, null values will be ignored.
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return PlayerEntity[]
	 */
	public static function findByExample(PDO $db,PlayerEntity $example, $and=true, $sort=null) {
		$exampleValues=$example->toArray();
		$filter=array();
		foreach ($exampleValues as $fieldId=>$value) {
			if (null!==$value) {
				$filter[$fieldId]=$value;
			}
		}
		return self::findByFilter($db, $filter, $and, $sort);
	}

	/**
	 * Query by filter.
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * Will return matched rows as an array of PlayerEntity instances.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $filter array of DFC instances defining the conditions
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return PlayerEntity[]
	 */
	public static function findByFilter(PDO $db, $filter, $and=true, $sort=null) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		$sql='SELECT * FROM `player`'
		. self::buildSqlWhere($filter, $and, false, true)
		. self::buildSqlOrderBy($sort);

		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		return self::fromStatement($stmt);
	}

	/**
	 * Will execute the passed statement and return the result as an array of PlayerEntity instances
	 *
	 * @param PDOStatement $stmt
	 * @return PlayerEntity[]
	 */
	public static function fromStatement(PDOStatement $stmt) {
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * returns the result as an array of PlayerEntity instances without executing the passed statement
	 *
	 * @param PDOStatement $stmt
	 * @return PlayerEntity[]
	 */
	public static function fromExecutedStatement(PDOStatement $stmt) {
		$resultInstances=array();
		while($result=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$o=new PlayerEntity();
			$o->assignByHash($result);
			$o->notifyPristine();
			$resultInstances[]=$o;
		}
		$stmt->closeCursor();
		return $resultInstances;
	}

	/**
	 * Get sql WHERE part from filter.
	 *
	 * @param array $filter
	 * @param bool $and
	 * @param bool $fullyQualifiedNames true if field names should be qualified by table name
	 * @param bool $prependWhere true if WHERE should be prepended to conditions
	 * @return string
	 */
	public static function buildSqlWhere($filter, $and, $fullyQualifiedNames=true, $prependWhere=false) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		return $filter->buildSqlWhere(new self::$CLASS_NAME, $fullyQualifiedNames, $prependWhere);
	}

	/**
	 * get sql ORDER BY part from DSCs
	 *
	 * @param array $sort array of DSC instances
	 * @return string
	 */
	protected static function buildSqlOrderBy($sort) {
		return DSC::buildSqlOrderBy(new self::$CLASS_NAME, $sort);
	}

	/**
	 * bind values from filter to statement
	 *
	 * @param PDOStatement $stmt
	 * @param DFCInterface $filter
	 */
	public static function bindValuesForFilter(PDOStatement &$stmt, DFCInterface $filter) {
		$filter->bindValuesForFilter(new self::$CLASS_NAME, $stmt);
	}

	/**
	 * Execute select query and return matched rows as an array of PlayerEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param string $sql
	 * @return PlayerEntity[]
	 */
	public static function findBySql(PDO $db, $sql) {
		$stmt=$db->query($sql);
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * Delete rows matching the filter
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * @param PDO $db
	 * @param array $filter
	 * @param bool $and
	 * @return mixed
	 */
	public static function deleteByFilter(PDO $db, $filter, $and=true) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		if (0==count($filter)) {
			throw new InvalidArgumentException('refusing to delete without filter'); // just comment out this line if you are brave
		}
		$sql='DELETE FROM `player`'
		. self::buildSqlWhere($filter, $and, false, true);
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Assign values from array with the field id as index and the value as value
	 *
	 * @param array $array
	 */
	public function assignByArray($array) {
		$result=array();
		foreach ($array as $fieldId=>$value) {
			$result[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		$this->assignByHash($result);
	}

	/**
	 * Assign values from hash where the indexes match the tables field names
	 *
	 * @param array $result
	 */
	public function assignByHash($result) {
		$this->setId($result['id']);
		$this->setEmail($result['email']);
		$this->setPassword($result['password']);
		$this->setFirstname($result['firstname']);
		$this->setLastname($result['lastname']);
		$this->setGtalk($result['gtalk']);
		$this->setType($result['type']);
		$this->setActive($result['active']);
		$this->setCheckedIn($result['checkedIn']);
		$this->setDeleted($result['deleted']);
		$this->setLastActivity($result['lastActivity']);
		$this->setDateRegistered($result['dateRegistered']);
	}

	/**
	 * Get element instance by it's primary key(s).
	 * Will return null if no row was matched.
	 *
	 * @param PDO $db
	 * @return PlayerEntity
	 */
	public static function findById(PDO $db,$id) {
		$stmt=self::prepareStatement($db,self::SQL_SELECT_PK);
		$stmt->bindValue(1,$id);
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if(!$result) {
			return null;
		}
		$o=new PlayerEntity();
		$o->assignByHash($result);
		$o->notifyPristine();
		return $o;
	}

	/**
	 * Bind all values to statement
	 *
	 * @param PDOStatement $stmt
	 */
	protected function bindValues(PDOStatement &$stmt) {
		$stmt->bindValue(1,$this->getId());
		$stmt->bindValue(2,$this->getEmail());
		$stmt->bindValue(3,$this->getPassword());
		$stmt->bindValue(4,$this->getFirstname());
		$stmt->bindValue(5,$this->getLastname());
		$stmt->bindValue(6,$this->getGtalk());
		$stmt->bindValue(7,$this->getType(), PDO::PARAM_BOOL);
		$stmt->bindValue(8,$this->getActive(), PDO::PARAM_BOOL);
		$stmt->bindValue(9,$this->getCheckedIn(), PDO::PARAM_BOOL);
		$stmt->bindValue(10,$this->getDeleted(), PDO::PARAM_BOOL);
		$stmt->bindValue(11,$this->getLastActivity());
		$stmt->bindValue(12,$this->getDateRegistered());
	}


	/**
	 * Insert this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function insertIntoDatabase(PDO $db) {
		if (null===$this->getId()) {
			$stmt=self::prepareStatement($db,self::SQL_INSERT_AUTOINCREMENT);
			$stmt->bindValue(1,$this->getEmail());
			$stmt->bindValue(2,$this->getPassword());
			$stmt->bindValue(3,$this->getFirstname());
			$stmt->bindValue(4,$this->getLastname());
			$stmt->bindValue(5,$this->getGtalk());
			$stmt->bindValue(6,$this->getType(), PDO::PARAM_BOOL);
			$stmt->bindValue(7,$this->getActive(), PDO::PARAM_BOOL);
			$stmt->bindValue(8,$this->getCheckedIn(), PDO::PARAM_BOOL);
			$stmt->bindValue(9,$this->getDeleted(), PDO::PARAM_BOOL);
			$stmt->bindValue(10,$this->getLastActivity());
			$stmt->bindValue(11,$this->getDateRegistered());
		} else {
			$stmt=self::prepareStatement($db,self::SQL_INSERT);
			$this->bindValues($stmt);
		}
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$lastInsertId=$db->lastInsertId();
		if (false!==$lastInsertId) {
			$this->setId($lastInsertId);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Update this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateToDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_UPDATE);
		$this->bindValues($stmt);
		$stmt->bindValue(13,$this->getId());
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Delete this instance from the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function deleteFromDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_DELETE_PK);
		$stmt->bindValue(1,$this->getId());
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Fetch ApiEntity's which this PlayerEntity references.
	 * `player`.`id` -> `api`.`playerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return ApiEntity[]
	 */
	public function fetchApiEntityCollection(PDO $db, $sort=null) {
		$filter=array(ApiEntity::FIELD_PLAYERID=>$this->getId());
		return ApiEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch AuthTokenEntity's which this PlayerEntity references.
	 * `player`.`id` -> `authToken`.`playerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return AuthTokenEntity[]
	 */
	public function fetchAuthTokenEntityCollection(PDO $db, $sort=null) {
		$filter=array(AuthTokenEntity::FIELD_PLAYERID=>$this->getId());
		return AuthTokenEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch MatchEntity's which this PlayerEntity references.
	 * `player`.`id` -> `match`.`createdByPlayerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MatchEntity[]
	 */
	public function fetchMatchEntityCollection(PDO $db, $sort=null) {
		$filter=array(MatchEntity::FIELD_CREATEDBYPLAYERID=>$this->getId());
		return MatchEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch MatchEntity1's which this PlayerEntity references.
	 * `player`.`id` -> `match`.`savedByPlayerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MatchEntity1[]
	 */
	public function fetchMatchEntity1Collection(PDO $db, $sort=null) {
		$filter=array(MatchEntity1::FIELD_SAVEDBYPLAYERID=>$this->getId());
		return MatchEntity1::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch NotificationEntity's which this PlayerEntity references.
	 * `player`.`id` -> `notification`.`playerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return NotificationEntity[]
	 */
	public function fetchNotificationEntityCollection(PDO $db, $sort=null) {
		$filter=array(NotificationEntity::FIELD_PLAYERID=>$this->getId());
		return NotificationEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch PlayerAwardEntity's which this PlayerEntity references.
	 * `player`.`id` -> `playerAward`.`playerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return PlayerAwardEntity[]
	 */
	public function fetchPlayerAwardEntityCollection(PDO $db, $sort=null) {
		$filter=array(PlayerAwardEntity::FIELD_PLAYERID=>$this->getId());
		return PlayerAwardEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch PlayerMatchEntity's which this PlayerEntity references.
	 * `player`.`id` -> `playerMatch`.`playerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return PlayerMatchEntity[]
	 */
	public function fetchPlayerMatchEntityCollection(PDO $db, $sort=null) {
		$filter=array(PlayerMatchEntity::FIELD_PLAYERID=>$this->getId());
		return PlayerMatchEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch PlayerSettingsEntity's which this PlayerEntity references.
	 * `player`.`id` -> `playerSettings`.`playerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return PlayerSettingsEntity[]
	 */
	public function fetchPlayerSettingsEntityCollection(PDO $db, $sort=null) {
		$filter=array(PlayerSettingsEntity::FIELD_PLAYERID=>$this->getId());
		return PlayerSettingsEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch UserRoleEntity's which this PlayerEntity references.
	 * `player`.`id` -> `userRole`.`playerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UserRoleEntity[]
	 */
	public function fetchUserRoleEntityCollection(PDO $db, $sort=null) {
		$filter=array(UserRoleEntity::FIELD_PLAYERID=>$this->getId());
		return UserRoleEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch UserRoleEntity1's which this PlayerEntity references.
	 * `player`.`id` -> `userRole`.`givenByPlayerId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UserRoleEntity1[]
	 */
	public function fetchUserRoleEntity1Collection(PDO $db, $sort=null) {
		$filter=array(UserRoleEntity1::FIELD_GIVENBYPLAYERID=>$this->getId());
		return UserRoleEntity1::findByFilter($db, $filter, true, $sort);
	}


	/**
	 * get element as DOM Document
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		return self::hashToDomDocument($this->toHash(), 'PlayerEntity');
	}

	/**
	 * get single PlayerEntity instance from a DOMElement
	 *
	 * @param DOMElement $node
	 * @return PlayerEntity
	 */
	public static function fromDOMElement(DOMElement $node) {
		$o=new PlayerEntity();
		$o->assignByHash(self::domNodeToHash($node, self::$FIELD_NAMES, self::$DEFAULT_VALUES, self::$FIELD_TYPES));
			$o->notifyPristine();
		return $o;
	}

	/**
	 * get all instances of PlayerEntity from the passed DOMDocument
	 *
	 * @param DOMDocument $doc
	 * @return PlayerEntity[]
	 */
	public static function fromDOMDocument(DOMDocument $doc) {
		$instances=array();
		foreach ($doc->getElementsByTagName('PlayerEntity') as $node) {
			$instances[]=self::fromDOMElement($node);
		}
		return $instances;
	}

}
?>