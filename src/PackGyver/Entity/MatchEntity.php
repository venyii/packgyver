<?php

/**
 * 
 *
 * @version 1.102
 * @package entity
 */
class MatchEntity extends Db2PhpEntityBase implements Db2PhpEntityModificationTracking {
	private static $CLASS_NAME='MatchEntity';
	const SQL_IDENTIFIER_QUOTE='`';
	const SQL_TABLE_NAME='match';
	const SQL_INSERT='INSERT INTO `match` (`id`,`resultTeamYellow`,`resultTeamRed`,`random`,`dateCreated`,`dateSaved`,`createdByPlayerId`,`savedByPlayerId`,`deleted`) VALUES (?,?,?,?,?,?,?,?,?)';
	const SQL_INSERT_AUTOINCREMENT='INSERT INTO `match` (`resultTeamYellow`,`resultTeamRed`,`random`,`dateCreated`,`dateSaved`,`createdByPlayerId`,`savedByPlayerId`,`deleted`) VALUES (?,?,?,?,?,?,?,?)';
	const SQL_UPDATE='UPDATE `match` SET `id`=?,`resultTeamYellow`=?,`resultTeamRed`=?,`random`=?,`dateCreated`=?,`dateSaved`=?,`createdByPlayerId`=?,`savedByPlayerId`=?,`deleted`=? WHERE `id`=?';
	const SQL_SELECT_PK='SELECT * FROM `match` WHERE `id`=?';
	const SQL_DELETE_PK='DELETE FROM `match` WHERE `id`=?';
	const FIELD_ID=296865252;
	const FIELD_RESULTTEAMYELLOW=563587639;
	const FIELD_RESULTTEAMRED=1615004078;
	const FIELD_RANDOM=905255180;
	const FIELD_DATECREATED=-1401300815;
	const FIELD_DATESAVED=924558992;
	const FIELD_CREATEDBYPLAYERID=776857202;
	const FIELD_SAVEDBYPLAYERID=1020933393;
	const FIELD_DELETED=-1429306128;
	private static $PRIMARY_KEYS=array(self::FIELD_ID);
	private static $AUTOINCREMENT_FIELDS=array(self::FIELD_ID);
	private static $FIELD_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_RESULTTEAMYELLOW=>'resultTeamYellow',
		self::FIELD_RESULTTEAMRED=>'resultTeamRed',
		self::FIELD_RANDOM=>'random',
		self::FIELD_DATECREATED=>'dateCreated',
		self::FIELD_DATESAVED=>'dateSaved',
		self::FIELD_CREATEDBYPLAYERID=>'createdByPlayerId',
		self::FIELD_SAVEDBYPLAYERID=>'savedByPlayerId',
		self::FIELD_DELETED=>'deleted');
	private static $PROPERTY_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_RESULTTEAMYELLOW=>'resultTeamYellow',
		self::FIELD_RESULTTEAMRED=>'resultTeamRed',
		self::FIELD_RANDOM=>'random',
		self::FIELD_DATECREATED=>'dateCreated',
		self::FIELD_DATESAVED=>'dateSaved',
		self::FIELD_CREATEDBYPLAYERID=>'createdByPlayerId',
		self::FIELD_SAVEDBYPLAYERID=>'savedByPlayerId',
		self::FIELD_DELETED=>'deleted');
	private static $PROPERTY_TYPES=array(
		self::FIELD_ID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_RESULTTEAMYELLOW=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_RESULTTEAMRED=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_RANDOM=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_DATECREATED=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_DATESAVED=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_CREATEDBYPLAYERID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_SAVEDBYPLAYERID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_DELETED=>Db2PhpEntity::PHP_TYPE_BOOL);
	private static $FIELD_TYPES=array(
		self::FIELD_ID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_RESULTTEAMYELLOW=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_RESULTTEAMRED=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_RANDOM=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_DATECREATED=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,false),
		self::FIELD_DATESAVED=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,true),
		self::FIELD_CREATEDBYPLAYERID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_SAVEDBYPLAYERID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_DELETED=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false));
	private static $DEFAULT_VALUES=array(
		self::FIELD_ID=>null,
		self::FIELD_RESULTTEAMYELLOW=>0,
		self::FIELD_RESULTTEAMRED=>0,
		self::FIELD_RANDOM=>'1',
		self::FIELD_DATECREATED=>'',
		self::FIELD_DATESAVED=>null,
		self::FIELD_CREATEDBYPLAYERID=>0,
		self::FIELD_SAVEDBYPLAYERID=>0,
		self::FIELD_DELETED=>'0');
	private $id;
	private $resultTeamYellow;
	private $resultTeamRed;
	private $random;
	private $dateCreated;
	private $dateSaved;
	private $createdByPlayerId;
	private $savedByPlayerId;
	private $deleted;

	/**
	 * set value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @param mixed $id
	 * @return MatchEntity
	 */
	public function &setId($id) {
		$this->notifyChanged(self::FIELD_ID,$this->id,$id);
		$this->id=$id;
		return $this;
	}

	/**
	 * get value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * set value for resultTeamYellow 
	 *
	 * type:INT UNSIGNED,size:10,default:null
	 *
	 * @param mixed $resultTeamYellow
	 * @return MatchEntity
	 */
	public function &setResultTeamYellow($resultTeamYellow) {
		$this->notifyChanged(self::FIELD_RESULTTEAMYELLOW,$this->resultTeamYellow,$resultTeamYellow);
		$this->resultTeamYellow=$resultTeamYellow;
		return $this;
	}

	/**
	 * get value for resultTeamYellow 
	 *
	 * type:INT UNSIGNED,size:10,default:null
	 *
	 * @return mixed
	 */
	public function getResultTeamYellow() {
		return $this->resultTeamYellow;
	}

	/**
	 * set value for resultTeamRed 
	 *
	 * type:INT UNSIGNED,size:10,default:null
	 *
	 * @param mixed $resultTeamRed
	 * @return MatchEntity
	 */
	public function &setResultTeamRed($resultTeamRed) {
		$this->notifyChanged(self::FIELD_RESULTTEAMRED,$this->resultTeamRed,$resultTeamRed);
		$this->resultTeamRed=$resultTeamRed;
		return $this;
	}

	/**
	 * get value for resultTeamRed 
	 *
	 * type:INT UNSIGNED,size:10,default:null
	 *
	 * @return mixed
	 */
	public function getResultTeamRed() {
		return $this->resultTeamRed;
	}

	/**
	 * set value for random 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @param mixed $random
	 * @return MatchEntity
	 */
	public function &setRandom($random) {
		$this->notifyChanged(self::FIELD_RANDOM,$this->random,$random);
		$this->random=$random;
		return $this;
	}

	/**
	 * get value for random 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @return mixed
	 */
	public function getRandom() {
		return $this->random;
	}

	/**
	 * set value for dateCreated 
	 *
	 * type:DATETIME,size:19,default:null
	 *
	 * @param mixed $dateCreated
	 * @return MatchEntity
	 */
	public function &setDateCreated($dateCreated) {
		$this->notifyChanged(self::FIELD_DATECREATED,$this->dateCreated,$dateCreated);
		$this->dateCreated=$dateCreated;
		return $this;
	}

	/**
	 * get value for dateCreated 
	 *
	 * type:DATETIME,size:19,default:null
	 *
	 * @return mixed
	 */
	public function getDateCreated() {
		return $this->dateCreated;
	}

	/**
	 * set value for dateSaved 
	 *
	 * type:DATETIME,size:19,default:null,nullable
	 *
	 * @param mixed $dateSaved
	 * @return MatchEntity
	 */
	public function &setDateSaved($dateSaved) {
		$this->notifyChanged(self::FIELD_DATESAVED,$this->dateSaved,$dateSaved);
		$this->dateSaved=$dateSaved;
		return $this;
	}

	/**
	 * get value for dateSaved 
	 *
	 * type:DATETIME,size:19,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getDateSaved() {
		return $this->dateSaved;
	}

	/**
	 * set value for createdByPlayerId 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index
	 *
	 * @param mixed $createdByPlayerId
	 * @return MatchEntity
	 */
	public function &setCreatedByPlayerId($createdByPlayerId) {
		$this->notifyChanged(self::FIELD_CREATEDBYPLAYERID,$this->createdByPlayerId,$createdByPlayerId);
		$this->createdByPlayerId=$createdByPlayerId;
		return $this;
	}

	/**
	 * get value for createdByPlayerId 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index
	 *
	 * @return mixed
	 */
	public function getCreatedByPlayerId() {
		return $this->createdByPlayerId;
	}

	/**
	 * set value for savedByPlayerId 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index
	 *
	 * @param mixed $savedByPlayerId
	 * @return MatchEntity
	 */
	public function &setSavedByPlayerId($savedByPlayerId) {
		$this->notifyChanged(self::FIELD_SAVEDBYPLAYERID,$this->savedByPlayerId,$savedByPlayerId);
		$this->savedByPlayerId=$savedByPlayerId;
		return $this;
	}

	/**
	 * get value for savedByPlayerId 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index
	 *
	 * @return mixed
	 */
	public function getSavedByPlayerId() {
		return $this->savedByPlayerId;
	}

	/**
	 * set value for deleted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $deleted
	 * @return MatchEntity
	 */
	public function &setDeleted($deleted) {
		$this->notifyChanged(self::FIELD_DELETED,$this->deleted,$deleted);
		$this->deleted=$deleted;
		return $this;
	}

	/**
	 * get value for deleted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getDeleted() {
		return $this->deleted;
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public static function getTableName() {
		return self::SQL_TABLE_NAME;
	}

	/**
	 * Get array with field id as index and field name as value
	 *
	 * @return array
	 */
	public static function getFieldNames() {
		return self::$FIELD_NAMES;
	}

	/**
	 * Get array with field id as index and property name as value
	 *
	 * @return array
	 */
	public static function getPropertyNames() {
		return self::$PROPERTY_NAMES;
	}

	/**
	 * get the field name for the passed field id.
	 *
	 * @param int $fieldId
	 * @param bool $fullyQualifiedName true if field name should be qualified by table name
	 * @return string field name for the passed field id, null if the field doesn't exist
	 */
	public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName=true) {
		if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
			return null;
		}
		$fieldName=self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
		if ($fullyQualifiedName) {
			return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
		}
		return $fieldName;
	}

	/**
	 * Get array with field ids of identifiers
	 *
	 * @return array
	 */
	public static function getIdentifierFields() {
		return self::$PRIMARY_KEYS;
	}

	/**
	 * Get array with field ids of autoincrement fields
	 *
	 * @return array
	 */
	public static function getAutoincrementFields() {
		return self::$AUTOINCREMENT_FIELDS;
	}

	/**
	 * Get array with field id as index and property type as value
	 *
	 * @return array
	 */
	public static function getPropertyTypes() {
		return self::$PROPERTY_TYPES;
	}

	/**
	 * Get array with field id as index and field type as value
	 *
	 * @return array
	 */
	public static function getFieldTypes() {
		return self::$FIELD_TYPES;
	}

	/**
	 * Assign default values according to table
	 * 
	 */
	public function assignDefaultValues() {
		$this->assignByArray(self::$DEFAULT_VALUES);
	}


	/**
	 * return hash with the field name as index and the field value as value.
	 *
	 * @return array
	 */
	public function toHash() {
		$array=$this->toArray();
		$hash=array();
		foreach ($array as $fieldId=>$value) {
			$hash[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		return $hash;
	}

	/**
	 * return array with the field id as index and the field value as value.
	 *
	 * @return array
	 */
	public function toArray() {
		return array(
			self::FIELD_ID=>$this->getId(),
			self::FIELD_RESULTTEAMYELLOW=>$this->getResultTeamYellow(),
			self::FIELD_RESULTTEAMRED=>$this->getResultTeamRed(),
			self::FIELD_RANDOM=>$this->getRandom(),
			self::FIELD_DATECREATED=>$this->getDateCreated(),
			self::FIELD_DATESAVED=>$this->getDateSaved(),
			self::FIELD_CREATEDBYPLAYERID=>$this->getCreatedByPlayerId(),
			self::FIELD_SAVEDBYPLAYERID=>$this->getSavedByPlayerId(),
			self::FIELD_DELETED=>$this->getDeleted());
	}


	/**
	 * return array with the field id as index and the field value as value for the identifier fields.
	 *
	 * @return array
	 */
	public function getPrimaryKeyValues() {
		return array(
			self::FIELD_ID=>$this->getId());
	}

	/**
	 * cached statements
	 *
	 * @var array<string,array<string,PDOStatement>>
	 */
	private static $stmts=array();
	private static $cacheStatements=true;
	
	/**
	 * prepare passed string as statement or return cached if enabled and available
	 *
	 * @param PDO $db
	 * @param string $statement
	 * @return PDOStatement
	 */
	protected static function prepareStatement(PDO $db, $statement) {
		if(self::isCacheStatements()) {
			if (in_array($statement, array(self::SQL_INSERT, self::SQL_INSERT_AUTOINCREMENT, self::SQL_UPDATE, self::SQL_SELECT_PK, self::SQL_DELETE_PK))) {
				$dbInstanceId=spl_object_hash($db);
				if (null===self::$stmts[$statement][$dbInstanceId]) {
					self::$stmts[$statement][$dbInstanceId]=$db->prepare($statement);
				}
				return self::$stmts[$statement][$dbInstanceId];
			}
		}
		return $db->prepare($statement);
	}

	/**
	 * Enable statement cache
	 *
	 * @param bool $cache
	 */
	public static function setCacheStatements($cache) {
		self::$cacheStatements=true==$cache;
	}

	/**
	 * Check if statement cache is enabled
	 *
	 * @return bool
	 */
	public static function isCacheStatements() {
		return self::$cacheStatements;
	}

	/**
	 * Query by Example.
	 *
	 * Match by attributes of passed example instance and return matched rows as an array of MatchEntity instances
	 *
	 * @param PDO $db a PDO Database instance
	 * @param MatchEntity $example an example instance defining the conditions. All non-null properties will be considered a constraint, null values will be ignored.
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return MatchEntity[]
	 */
	public static function findByExample(PDO $db,MatchEntity $example, $and=true, $sort=null) {
		$exampleValues=$example->toArray();
		$filter=array();
		foreach ($exampleValues as $fieldId=>$value) {
			if (null!==$value) {
				$filter[$fieldId]=$value;
			}
		}
		return self::findByFilter($db, $filter, $and, $sort);
	}

	/**
	 * Query by filter.
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * Will return matched rows as an array of MatchEntity instances.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $filter array of DFC instances defining the conditions
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return MatchEntity[]
	 */
	public static function findByFilter(PDO $db, $filter, $and=true, $sort=null) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		$sql='SELECT * FROM `match`'
		. self::buildSqlWhere($filter, $and, false, true)
		. self::buildSqlOrderBy($sort);

		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		return self::fromStatement($stmt);
	}

	/**
	 * Will execute the passed statement and return the result as an array of MatchEntity instances
	 *
	 * @param PDOStatement $stmt
	 * @return MatchEntity[]
	 */
	public static function fromStatement(PDOStatement $stmt) {
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * returns the result as an array of MatchEntity instances without executing the passed statement
	 *
	 * @param PDOStatement $stmt
	 * @return MatchEntity[]
	 */
	public static function fromExecutedStatement(PDOStatement $stmt) {
		$resultInstances=array();
		while($result=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$o=new MatchEntity();
			$o->assignByHash($result);
			$o->notifyPristine();
			$resultInstances[]=$o;
		}
		$stmt->closeCursor();
		return $resultInstances;
	}

	/**
	 * Get sql WHERE part from filter.
	 *
	 * @param array $filter
	 * @param bool $and
	 * @param bool $fullyQualifiedNames true if field names should be qualified by table name
	 * @param bool $prependWhere true if WHERE should be prepended to conditions
	 * @return string
	 */
	public static function buildSqlWhere($filter, $and, $fullyQualifiedNames=true, $prependWhere=false) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		return $filter->buildSqlWhere(new self::$CLASS_NAME, $fullyQualifiedNames, $prependWhere);
	}

	/**
	 * get sql ORDER BY part from DSCs
	 *
	 * @param array $sort array of DSC instances
	 * @return string
	 */
	protected static function buildSqlOrderBy($sort) {
		return DSC::buildSqlOrderBy(new self::$CLASS_NAME, $sort);
	}

	/**
	 * bind values from filter to statement
	 *
	 * @param PDOStatement $stmt
	 * @param DFCInterface $filter
	 */
	public static function bindValuesForFilter(PDOStatement &$stmt, DFCInterface $filter) {
		$filter->bindValuesForFilter(new self::$CLASS_NAME, $stmt);
	}

	/**
	 * Execute select query and return matched rows as an array of MatchEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param string $sql
	 * @return MatchEntity[]
	 */
	public static function findBySql(PDO $db, $sql) {
		$stmt=$db->query($sql);
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * Delete rows matching the filter
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * @param PDO $db
	 * @param array $filter
	 * @param bool $and
	 * @return mixed
	 */
	public static function deleteByFilter(PDO $db, $filter, $and=true) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		if (0==count($filter)) {
			throw new InvalidArgumentException('refusing to delete without filter'); // just comment out this line if you are brave
		}
		$sql='DELETE FROM `match`'
		. self::buildSqlWhere($filter, $and, false, true);
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Assign values from array with the field id as index and the value as value
	 *
	 * @param array $array
	 */
	public function assignByArray($array) {
		$result=array();
		foreach ($array as $fieldId=>$value) {
			$result[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		$this->assignByHash($result);
	}

	/**
	 * Assign values from hash where the indexes match the tables field names
	 *
	 * @param array $result
	 */
	public function assignByHash($result) {
		$this->setId($result['id']);
		$this->setResultTeamYellow($result['resultTeamYellow']);
		$this->setResultTeamRed($result['resultTeamRed']);
		$this->setRandom($result['random']);
		$this->setDateCreated($result['dateCreated']);
		$this->setDateSaved($result['dateSaved']);
		$this->setCreatedByPlayerId($result['createdByPlayerId']);
		$this->setSavedByPlayerId($result['savedByPlayerId']);
		$this->setDeleted($result['deleted']);
	}

	/**
	 * Get element instance by it's primary key(s).
	 * Will return null if no row was matched.
	 *
	 * @param PDO $db
	 * @return MatchEntity
	 */
	public static function findById(PDO $db,$id) {
		$stmt=self::prepareStatement($db,self::SQL_SELECT_PK);
		$stmt->bindValue(1,$id);
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if(!$result) {
			return null;
		}
		$o=new MatchEntity();
		$o->assignByHash($result);
		$o->notifyPristine();
		return $o;
	}

	/**
	 * Bind all values to statement
	 *
	 * @param PDOStatement $stmt
	 */
	protected function bindValues(PDOStatement &$stmt) {
		$stmt->bindValue(1,$this->getId());
		$stmt->bindValue(2,$this->getResultTeamYellow());
		$stmt->bindValue(3,$this->getResultTeamRed());
		$stmt->bindValue(4,$this->getRandom());
		$stmt->bindValue(5,$this->getDateCreated());
		$stmt->bindValue(6,$this->getDateSaved());
		$stmt->bindValue(7,$this->getCreatedByPlayerId());
		$stmt->bindValue(8,$this->getSavedByPlayerId());
		$stmt->bindValue(9,$this->getDeleted());
	}


	/**
	 * Insert this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function insertIntoDatabase(PDO $db) {
		if (null===$this->getId()) {
			$stmt=self::prepareStatement($db,self::SQL_INSERT_AUTOINCREMENT);
			$stmt->bindValue(1,$this->getResultTeamYellow());
			$stmt->bindValue(2,$this->getResultTeamRed());
			$stmt->bindValue(3,$this->getRandom(), PDO::PARAM_BOOL);
			$stmt->bindValue(4,$this->getDateCreated());
			$stmt->bindValue(5,$this->getDateSaved());
			$stmt->bindValue(6,$this->getCreatedByPlayerId());
			$stmt->bindValue(7,$this->getSavedByPlayerId());
			$stmt->bindValue(8,$this->getDeleted(), PDO::PARAM_BOOL);
		} else {
			$stmt=self::prepareStatement($db,self::SQL_INSERT);
			$this->bindValues($stmt);
		}
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$lastInsertId=$db->lastInsertId();
		if (false!==$lastInsertId) {
			$this->setId($lastInsertId);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Update this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateToDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_UPDATE);
		$this->bindValues($stmt);
		$stmt->bindValue(10,$this->getId());
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Delete this instance from the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function deleteFromDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_DELETE_PK);
		$stmt->bindValue(1,$this->getId());
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Fetch PlayerMatchEntity's which this MatchEntity references.
	 * `match`.`id` -> `playerMatch`.`matchId`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return PlayerMatchEntity[]
	 */
	public function fetchPlayerMatchEntityCollection(PDO $db, $sort=null) {
		$filter=array(PlayerMatchEntity::FIELD_MATCHID=>$this->getId());
		return PlayerMatchEntity::findByFilter($db, $filter, true, $sort);
	}

	/**
	 * Fetch PlayerEntity which references this MatchEntity. Will return null in case reference is invalid.
	 * `match`.`createdByPlayerId` -> `player`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return PlayerEntity
	 */
	public function fetchPlayerEntity(PDO $db, $sort=null) {
		$filter=array(PlayerEntity::FIELD_ID=>$this->getCreatedByPlayerId());
		$result=PlayerEntity::findByFilter($db, $filter, true, $sort);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch PlayerEntity1 which references this MatchEntity. Will return null in case reference is invalid.
	 * `match`.`savedByPlayerId` -> `player`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return PlayerEntity1
	 */
	public function fetchPlayerEntity1(PDO $db, $sort=null) {
		$filter=array(PlayerEntity1::FIELD_ID=>$this->getSavedByPlayerId());
		$result=PlayerEntity1::findByFilter($db, $filter, true, $sort);
		return empty($result) ? null : $result[0];
	}


	/**
	 * get element as DOM Document
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		return self::hashToDomDocument($this->toHash(), 'MatchEntity');
	}

	/**
	 * get single MatchEntity instance from a DOMElement
	 *
	 * @param DOMElement $node
	 * @return MatchEntity
	 */
	public static function fromDOMElement(DOMElement $node) {
		$o=new MatchEntity();
		$o->assignByHash(self::domNodeToHash($node, self::$FIELD_NAMES, self::$DEFAULT_VALUES, self::$FIELD_TYPES));
			$o->notifyPristine();
		return $o;
	}

	/**
	 * get all instances of MatchEntity from the passed DOMDocument
	 *
	 * @param DOMDocument $doc
	 * @return MatchEntity[]
	 */
	public static function fromDOMDocument(DOMDocument $doc) {
		$instances=array();
		foreach ($doc->getElementsByTagName('MatchEntity') as $node) {
			$instances[]=self::fromDOMElement($node);
		}
		return $instances;
	}

}
?>