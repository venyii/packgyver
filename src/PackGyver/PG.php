<?php

/**
 * PackGyver
 */
final class PG {

	const DEFAULT_DATEFORMAT = 'd.m.Y';
	const DEFAULT_DATETIMEFORMAT = 'd.m.Y, H:i\h';
	const DEFAULT_DWOO_FORMAT = '%d.%m.%Y, %H:%Mh';

	/**
	 * @var PDO
	 */
	private static $database = null;

	/**
	 * CTOR
	 *
	 * - no instances allowed -
	 */
	private function __construct() {}

	/**
	 * @return bool
	 */
	public static function isDevMode() {
		return true === $GLOBALS['devMode'];
	}

	/**
	 * @return string
	 */
	public static function getSiteUrl() {
		return $GLOBALS['pgSiteUrl'];
	}

	/**
	 * @return string
	 */
	public static function getTemplateDir() {
		return self::getRootDir() . 'views';
	}

	/**
	 * @return string
	 */
	public static function getRootDir() {
		return ROOT_PATH;
	}

	/**
	 * @return string
	 */
	public static function getAppDir() {
		return APP_PATH;
	}

	/**
	 * @return string|null
	 */
	public static function getActiveCache() {
		return isset($GLOBALS['cache']) ? $GLOBALS['cache'] : null;
	}

	/**
	 * @return string
	 */
	public static function getStaticDir() {
		return DOC_ROOT . 'static';
	}

	/**
	 * @return string
	 */
	public static function getUploadDir() {
		return self::getStaticDir() . DS . 'upload';
	}

    /**
     * @return string
     */
    public static function getCacheDir() {
        return $GLOBALS['cacheDir'];
    }

    /**
     * @return string
     */
    public static function getStorageDir() {
        return $GLOBALS['storageDir'];
    }

	/**
	 * when it all started...
	 *
	 * @return string
	 */
	public static function getRecordStart() {
		return $GLOBALS['recordStart'];
	}

	/**
	 * @return bool
	 */
	public static function isGoogleAnalyticsEnabled() {
		return true === $GLOBALS['googleAnalytics'];
	}

	/**
	 * get database connection
	 *
	 * @return PDO
	 * @throws PackGyverException
	 */
    public static function getDB() {
		if (null === self::$database) {
			$dsn = $GLOBALS['database']['dsn'];
			$username = $GLOBALS['database']['username'];
			$password = $GLOBALS['database']['password'];
			$opts = array(
				PDO::ATTR_PERSISTENT => true,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_NAMED,
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci",
			);

			try {
				self::$database = new PDO($dsn, $username, $password, $opts);
			} catch (PDOException $e) {
				throw new PackGyverException('Establishing database connection failed.', $e->getCode(), $e);
			}
		}

		return self::$database;
	}

}
