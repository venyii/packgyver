<?php

/**
 * PackGyver - Season Manager
 *
 * @package season
 */
class SeasonManager {

	/**
	 *
	 * @return array
	 */
	public static function getCurrentSeasonDatesSimple() {
		return array(
			'month' => date('n'),
			'year' => date('Y')
		);
	}

	/**
	 * get array with start and end date of the current season
	 *
	 * @return array
	 */
	public static function getCurrentSeasonDates() {
		$dates = self::getCurrentSeasonDatesSimple();
		return self::getSeasonDates($dates['month'], $dates['year']);
	}

	/**
	 *
	 * @param int $month
	 * @param int $year
	 * @return array
	 */
	public static function getSeasonDates($month, $year) {
		$startMonth = mktime(00, 00, 00, $month, 01, $year);
		$endMonth = mktime(23, 59, 59, $month + 1, 00, $year);

		return array(
			'monthName' => DateUtil::getMonthNameForMonth($month),
			'month' => $month,
			'year' => $year,
			'_start' => DateUtil::formatForDB($startMonth),
			'start' => DateUtil::format($startMonth, PG::DEFAULT_DATEFORMAT),
			'_end' => DateUtil::formatForDB($endMonth),
			'end' => DateUtil::format($endMonth, PG::DEFAULT_DATEFORMAT)
		);
	}

}
