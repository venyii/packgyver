<?php

/**
 * PackGyver - Season Archive
 *
 * @package season
 */
class SeasonArchive extends ArchiveAbstract {

	const ARCHIVE_DIR = 'archive/season';

	/**
	 *
	 * @param int $month
	 * @param int $year
	 * @return string
	 */
	public function getSeasonDir($month, $year) {
		return self::getArchiveDir() . DS . $year . '_' . $month;
	}

	/**
	 * @param int $month
	 * @param int $year
	 * @return int
	 */
	public function isSeasonStored($month, $year) {
		return FileUtil::countFilesInDir($this->getSeasonDir($month, $year), true) > 0;
	}

	/**
	 * check if season month/year is between record start and last season
	 *
	 * @param int $month
	 * @param int $year
	 * @return bool
	 */
	public function isSeasonStorable($month, $year) {
		$recordStart = DateUtil::formatFromDB(PG::getRecordStart(), true);
		$start = mktime(00, 00, 00, date('n', $recordStart), 01, date('Y', $recordStart));
		$end = mktime(23, 59, 59, date('n') + 1, 00, date('Y'));
		$check = mktime(23, 59, 59, $month + 1, 00, $year);

		return ($check > $start) && ($check < $end);
	}

	/**
	 *
	 * @return string
	 */
	public static function getArchiveDir() {
		$archiveDir = PG::getStorageDir() . DS . self::ARCHIVE_DIR;

		if (!is_dir($archiveDir)) {
			mkdir($archiveDir, 0755, true);
		}

		return $archiveDir;
	}

	/**
	 *
	 * @param int $month
	 * @param int $year
	 * @return \PlayerCollection 
	 */
	public function getPlayerCollectionForSeason($month, $year) {
		$players = new PlayerCollection();

		if ($this->isSeasonStored($month, $year)) {
			$db = PG::getDB();
			$seasonDir = $this->getSeasonDir($month, $year);

			foreach (FileUtil::getFilesFromDir($seasonDir, true) as $playerXml) {
				preg_match('/\\' . DS . '([0-9]{1,})\.xml/', $playerXml, $playerId);

				if (isset($playerId[1]) && is_numeric($playerId[1])) {
					$playerEntity = PlayerEntity::findById($db, intval($playerId[1]));

					if ($playerEntity instanceof PlayerEntity) {
						$playerModel = new PlayerModel($playerEntity);
						$playerStatistics = new PlayerStatistics($playerModel, PlayerStatistics::CACHETYPE_SEASON);

						$doc = DOMUtil::newDOMDocument();
						$doc->loadXML(FileUtil::getFileContents($playerXml));

						$playerStatistics->readFromDOM($doc->getElementsByTagName(PlayerStatistics::DOM_ROOTNODE)->item(0));

						$playerModel->setStats($playerStatistics, PlayerStatistics::CACHETYPE_SEASON);
						$players->appendPlayer($playerModel);
					}
				}
			}
		}

		return $players;
	}

	/**
	 *
	 * @param PlayerModel $player
	 * @param int $month
	 * @param int $year
	 * @return \PlayerStatistics|null
	 */
	public function getSeasonForPlayer(PlayerModel $player, $month, $year) {
		$path = $this->getSeasonDir($month, $year) . DS . $player->getId() . '.xml';

		if (file_exists($path)) {
			$statistic = new PlayerStatistics($player, PlayerStatistics::CACHETYPE_SEASON);
			$doc = DOMUtil::newDOMDocument();
			$doc->loadXML(FileUtil::getFileContents($path));

			$statistic->readFromDOM($doc->getElementsByTagName(PlayerStatistics::DOM_ROOTNODE)->item(0));

			return $statistic;
		}

		return null;
	}

	/**
	 *
	 * @param int $month
	 * @param int $year
	 * @return array
	 */
	public function storeSeason($month, $year) {
		$players = array();
		$seasonDir = $this->getSeasonDir($month, $year);

		if (!is_dir($seasonDir)) {
			mkdir($seasonDir, 0755, true);
		}

		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			$playerModel = new PlayerModel($player);
			$playerStats = new PlayerStatistics($playerModel, PlayerStatistics::CACHETYPE_SEASON);
			$stats = $playerStats->calculate($month, $year)->toHash();

			if ($stats['countMatches'] > 0) {
				FileUtil::setFileContents($seasonDir . DS . $player->getId() . '.xml', $playerStats->toDOM()->saveXML());

				$players[$player->getId()] = $playerModel->toHash();
				$players[$player->getId()]['stats'] = $stats;
			}
		}

		return $players;
	}

	/**
	 * 
	 * @param int $month
	 * @param int $year
	 * @return \Season
	 */
	public function getSeason($month, $year) {
		if (is_dir($this->getSeasonDir($month, $year))) {
			$season = new Season();
			$season->setMonth($month)
					->setYear($year)
					->setPlayerCollection($this->getPlayerCollectionForSeason($month, $year));

			return $season;
		}

		return null;
	}

	/**
	 * 
	 * @return \SeasonCollection
	 */
	public function getSeasons() {
		$seasons = new SeasonCollection();
		$sortedSeasons = array();

		foreach (FileUtil::getFilesFromDir(self::getArchiveDir(), false, false) as $season) {
			list($year, $month) = explode('_', $season);
			$sortedSeasons[(int) $year][] = (int) $month;
		}

		foreach ($sortedSeasons as $year => $sortedSeason) {
			sort($sortedSeason, SORT_NUMERIC);
			$sortedSeasons[$year] = $sortedSeason;
		}

		foreach ($sortedSeasons as $year => $seasonMonths) {
			foreach ($seasonMonths as $seasonMonth) {
				$season = $this->getSeason($seasonMonth, $year);

				if ($season instanceof Season) {
					$seasons->appendSeason($season);
				}
			}
		}

		return $seasons;
	}

	/**
	 *
	 * @return array
	 */
	public static function getStoredSeasonHashes() {
		$seasons = array();
		$storedSeasons = FileUtil::getFilesFromDir(self::getArchiveDir(), false, false);

		foreach ($storedSeasons as $key => $season) {
			list($year, $month) = explode('_', $season);
			$seasons[$key]['year'] = intval($year);
			$seasons[$key]['month'] = intval($month);
			$seasons[$key]['monthName'] = DateUtil::getMonthNameForMonth(intval($month));
		}

		$sorter = new SortUtil();

		return $sorter->sort($seasons, 'year', SortUtil::TYPE_ASC);
	}

	/**
	 * get all archived seasons that the player played
	 *
	 * @param PlayerModel $player
	 * @param bool $yearHash
	 * @return array
	 */
	public static function getSeasonsForPlayer(PlayerModel $player, $yearHash = false) {
		$seasons = array();
		$storedSeasons = FileUtil::getFilesFromDir(self::getArchiveDir(), false, false);

		foreach ($storedSeasons as $key => $season) {
			if (file_exists(self::getArchiveDir() . DS . $season . DS . $player->getId() . '.xml')) {
				list($year, $month) = explode('_', $season);
				$seasons[$key]['year'] = intval($year);
				$seasons[$key]['month'] = intval($month);
				$seasons[$key]['monthName'] = DateUtil::getMonthNameForMonth(intval($month));
			}
		}

		// consider active season
		if ($player->getStats(PlayerStatistics::CACHETYPE_SEASON)->getCountMatches() > 0) {
			$dates = SeasonManager::getCurrentSeasonDatesSimple();
			$seasons[] = array(
				'year' => $dates['year'],
				'month' => $dates['month'],
				'monthName' => DateUtil::getMonthNameForMonth(intval($dates['month']))
			);
		}

		$sorter = new SortUtil();
		$sorted = $sorter->sort($seasons, 'year', SortUtil::TYPE_ASC);

		if (true === $yearHash) {
			return self::getStoredSeasonYearHash($sorted);
		}

		return $sorted;
	}

	/**
	 * @param array $seasons
	 * @return array
	 */
	public static function getStoredSeasonYearHash($seasons = null) {
		$hash = array();

		if (null === $seasons) {
			$seasons = self::getStoredSeasonHashes();
		}

		foreach ($seasons as $season) {
			$hash[$season['year']][] = $season;
		}

		$sorter = new SortUtil();
		foreach ($hash as &$seasonYear) {
			$seasonYear = $sorter->sort($seasonYear, 'month', SortUtil::TYPE_ASC);
		}

		return $hash;
	}

}
