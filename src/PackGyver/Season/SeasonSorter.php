<?php

/**
 * PackGyver - Season Sorter
 *
 * @package season
 */
class SeasonSorter extends SortUtil {

	/**
	 * for ascending order
	 *
	 * @param array $x
	 * @param array $y
	 * @return int
	 */
	private function compare($x, $y) {
		if ($x->getMonth() == $y->getMonth()) {
			return 0;
		} else if ($x->getMonth() < $y->getMonth()) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * for descending order
	 *
	 * @param array $x
	 * @param array $y
	 * @return int
	 */
	private function compareReverse($x, $y) {
		if ($x->getMonth() == $y->getMonth()) {
			return 0;
		} else if ($x->getMonth() > $y->getMonth()) {
			return -1;
		} else {
			return 1;
		}
	}

}
