<?php

/**
 * Description of AuthenticationAbstract
 */
abstract class AuthenticationProviderAbstract implements AuthenticationProviderInterface {
	/* errors */

	const ERROR_UNKNOWN = 'unknownErrorOccurred';
	const ERROR_NODATA = 0;
	const ERROR_WRONGDATA = 1;
	const ERROR_DELETED = 2;
	const ERROR_BANNED = 3;
	const ERROR_INVALID_MAIL = 4;
	const ERROR_INVALID_PASSWORD = 5;
	const ERROR_LOGIN_DISABLED = 6;

	/**
	 *
	 * @var array
	 */
	protected $availableErrors = array(
		self::ERROR_NODATA => 'Unbekannter Spieler',
		self::ERROR_WRONGDATA => 'Falsche Daten',
		self::ERROR_DELETED => 'Du bist derzeit deaktiviert',
		self::ERROR_BANNED => 'Du wurdest gebannt',
		self::ERROR_INVALID_MAIL => 'Ungültige E-Mail Adresse',
		self::ERROR_INVALID_PASSWORD => 'invalidpassword',
		self::ERROR_LOGIN_DISABLED => 'Login derzeit abgeschalten'
	);
	protected $isLoggedIn = false;
	protected $error;
	protected $credentials = array();
	protected $settings;

	public function setError($error) {
		$this->error = $error;
	}

	public function getError() {
		return $this->error;
	}

	public function getCredentials() {
		return $this->credentials;
	}

	public function setCredentials(array $credentials) {
		
	}

    /**
     * @return SettingsManager
     */
    public function getSettings() {
		if (null === $this->settings) {
			$this->settings = SettingsManager::getGroup('authentication');
		}

		return $this->settings;
	}

	public function getResponse() {
		$redirect = null;

		if (isset($_POST['redirect']) && trim($_POST['redirect']) != '') {
			$redirect = rawurldecode(trim($_POST['redirect']));
		}

		$data = array(
			'redirect' => rawurlencode($_SERVER['REQUEST_URI']),
			'data' => array(
				'success' => $this->isLoggedIn,
				'error' => $this->error,
				'credentials' => $this->getCredentials(),
				'redirect' => $redirect
			)
		);

		$loginContent = DwooProxy::getInstance()->parseTemplate('singles/login', $data);
		$loginMainContent = DwooProxy::getInstance()->parseTemplate('singles/main', array(
			'pgMainContent' => $loginContent
				));

		$header = new HttpHeader();
		$header->setMimeType(HttpHeader::MIMETYPE_HTML);
		$header->setStatusCode(200);

		$response = new Response($loginMainContent);
		$response->setHttpHeader($header);

		return $response;
	}

	public function handleAuthentication() {
		if (!$this->isAuthenticated()) {
			if (true === $this->authenticate()) {
				$this->isLoggedIn = true;
			}
		} else {
			$this->isLoggedIn = true;
		}

		return $this;
	}

	/**
	 * @return bool
	 */
	abstract public function isEnabled();
}
