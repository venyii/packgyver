<?php

/**
 * PackGyver - Authentication Provider Google OAuth
 *
 * @package auth
 */
class AuthenticationProviderGoogleOAuth extends AuthenticationProviderAbstract {

	const COOKIE_NAME = '__packgyver__goa';
	const COOKIE_SEPARATOR = "::";
	const COOKIE_EXPIRATION = 1209600; // 2 weeks

	/**
	 *
	 * @var Google_Client
	 */
	private $client;

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return 'googleoauth';
	}

	/**
	 *
	 * @return Google_Client
	 */
	private function getClient() {
		if (null === $this->client) {
			$clientId = $this->getSettings()->getValue('googleClientId');
			$clientSecret = $this->getSettings()->getValue('googleClientSecret');

			if (!$clientId || !$clientSecret) {
				$this->getSettings()->setValue('googleClientId', $clientId ? : '');
				$this->getSettings()->setValue('googleClientSecret', $clientSecret ? : '');
			}

			$this->client = new Google_Client();
			$this->client->setApplicationName("PackGyver");
			$this->client->setClientId($clientId);
			$this->client->setClientSecret($clientSecret);
			$this->client->setRedirectUri(PG::getSiteUrl() . 'authenticate/service/googleoauth');
			$this->client->setAccessType('offline');
			$this->client->setApprovalPrompt('auto');
			$this->client->setScopes(array(
				'https://www.googleapis.com/auth/userinfo.profile',
				'https://www.googleapis.com/auth/userinfo.email'
			));
		}

		return $this->client;
	}

	/**
	 *
	 * @return string
	 */
	private function getAuthFileDir() {
		$path = PG::getCacheDir() . DIRECTORY_SEPARATOR . 'Authentication' . DIRECTORY_SEPARATOR . 'GoogleOAuth';

		if (!is_dir($path)) {
			mkdir($path, 0777, true);
		}

		return $path;
	}

	/**
	 *
	 * @return bool
	 */
	public function authenticate() {
		if (isset($_GET['error'])) {

			switch ($_GET['error']) {
				case'access_denied':
					$this->setError('Der Zugriff auf das Google Konto wurde verweigert.');
					break;
				default:
					$this->setError('Ein Fehler ist aufgetreten - Bitte verständige einen Admin.');
			}

			return false;
		} else if (isset($_GET['code'])) {
			try {
				$oauth2 = new Google_Service_Oauth2($this->getClient());

				$this->getClient()->fetchAccessTokenWithAuthCode($_GET['code']);

				$userInfos = $oauth2->userinfo->get();
				$authToken = json_decode($this->getClient()->getAccessToken(), true);

				if (is_array($userInfos) && isset($userInfos['email'])) {
					$player = PlayerEntity::findByFilter(PG::getDB(), array(
								new DFC(PlayerEntity::FIELD_GTALK, $userInfos['email'], DFC::EXACT)
							));

					if ($player[0] instanceof PlayerEntity) {
						if (!$player[0]->getDeleted()) {
							AuthenticationManager::getInstance()->setPlayer($player[0]);

							if (isset($authToken['refresh_token'])) {
								AuthTokenManager::addToken(PG::getDB(), $player[0], $this->getId(), 'refresh_token', $authToken['refresh_token']);
							} else {
								$token = AuthTokenManager::getTokenForPlayer(PG::getDB(), $player[0], $this->getId(), 'refresh_token');

								if ($token instanceof AuthTokenEntity) {
									$authToken['refresh_token'] = $token->getToken();
								}
							}

							$hashedToken = hash('sha256', $authToken['access_token']);
							$path = $this->getAuthFileDir() . DS . $hashedToken . '.auth';

							if (!file_exists($path)) {
								if (false !== file_put_contents($path, json_encode($authToken))) {
									CookieUtil::write(self::COOKIE_NAME, $hashedToken, self::COOKIE_EXPIRATION);
								}
							}
						} else {
							$this->setError('Account deaktiviert.');
							Logger::get()->warn('GoogleOAuth:AuthFile:UserDisabledError', array('Email' => $userInfos['email']));

							return false;
						}
					} else {
						$this->setError('Unbekannte E-Mail Adresse');
						Logger::get()->info('GoogleOAuth:UnknownEmailError', array('Email' => $userInfos['email']));

						return false;
					}
				}
			} catch (Google_Exception $exc) {
				$this->setError($exc->getMessage());

				return false;
			}
		}

		UrlUtil::redirect('/');
	}

	/**
	 *
	 * @return string
	 */
	public function getAuthUrl() {
		return $this->getClient()->createAuthUrl();
	}

	/**
	 *
	 * @return bool
	 */
	public function isAuthenticated() {
		if (!(AuthenticationManager::getInstance()->getPlayer() instanceof PlayerEntity)) {
			$this->loginFromAuthFile();
		}

		return (AuthenticationManager::getInstance()->getPlayer() instanceof PlayerEntity);
	}

	private function loginFromAuthFile() {
		if (CookieUtil::isValid(self::COOKIE_NAME)) {
			$authToken = CookieUtil::read(self::COOKIE_NAME);
			$path = $this->getAuthFileDir() . DS . $authToken . '.auth';

			if (file_exists($path)) {
				try {
					$authFile = file_get_contents($path);

					$this->getClient()->setAccessToken($authFile);
					$oauth2 = new Google_Service_Oauth2($this->getClient());
					$userInfos = $oauth2->userinfo->get();

					if (is_array($userInfos) && isset($userInfos['email'])) {
						$player = PlayerEntity::findByFilter(PG::getDB(), array(
									new DFC(PlayerEntity::FIELD_GTALK, $userInfos['email'], DFC::EXACT)
								));

						if ($player[0] instanceof PlayerEntity) {
							if (!$player[0]->getDeleted()) {
								AuthenticationManager::getInstance()->setPlayer($player[0]);
								return true;
							} else {
								$this->setError('Account deaktiviert.');
								Logger::get()->warn('GoogleOAuth:AuthFile:UserDisabledError', array('Email' => $userInfos['email']));

								return false;
							}

						} else {
							$this->setError('Unbekannte E-Mail Adresse');
							Logger::get()->warn('GoogleOAuth:AuthFile:UnknownEmailError', array('Email' => $userInfos['email']));

							return false;
						}
					}
				} catch (Google_Exception $exc) {
					unlink($path);

					$this->setError('Es ist ein Fehler bei der Authentifizierung durch Google aufgetreten. Error-Code: ' . $exc->getCode());
					Logger::get()->error('GoogleOAuth:AuthFile:LoginError - ' . $exc->getMessage(), array('exception' => $exc));
				}
			}

			CookieUtil::kill(self::COOKIE_NAME);
		}

		return null !== $this->getClient()->getAccessToken();
	}

	/**
	 * Logout the current user
	 */
	public function logout() {
		Session::instance()->destroy();

		if (CookieUtil::isValid(self::COOKIE_NAME)) {
			$authToken = CookieUtil::read(self::COOKIE_NAME);

			$path = $this->getAuthFileDir() . DS . $authToken . '.auth';

			if (file_exists($path)) {
				unlink($path);
				$this->getClient()->revokeToken();
			}
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function isEnabled() {
		$settings = $this->getSettings();
		$enabled = $settings->getValue('isGoogleLoginEnabled');

		if (null === $enabled) {
			$settings->setValue('isGoogleLoginEnabled', '0');
			$enabled = 0;
		}

		if (!(bool) $enabled) {
			return false;
		}

		return $settings->getValue('googleClientId') && $settings->getValue('googleClientSecret');
	}
}
