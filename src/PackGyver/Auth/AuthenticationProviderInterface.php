<?php

interface AuthenticationProviderInterface {

	public function getId();

	public function setCredentials(array $credentials);

	public function getCredentials();

	public function setError($error);

	public function getError();

	public function isEnabled();

	public function isAuthenticated();

	public function authenticate();

	public function logout();

	public function getResponse();

	public function handleAuthentication();
}
