<?php

/**
 * PackGyver - APC Cache
 */
class APCCache extends Cache {

	/**
	 * {@inheritdoc}
	 */
	public function get($key) {
		return apc_fetch($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function set($key, $value, $ttl = null) {
		return apc_store($key, $value, $ttl);
	}

	/**
	 * {@inheritdoc}
	 */
	public function exists($key) {
		return apc_exists($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function delete($key) {
		return apc_delete($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function flush() {
		apc_clear_cache('user');
		apc_clear_cache();
	}

}
