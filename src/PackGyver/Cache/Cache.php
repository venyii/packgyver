<?php

/**
 * PackGyver - Cache
 *
 * @package cache
 */
abstract class Cache implements CacheHandlerInterface {

	/**
	 * @var Cache
	 */
	protected static $cache;

	/**
	 * @return Cache
	 */
	public static function getCache() {
		if (null === self::$cache) {
			self::$cache = self::getCacheForKey(PG::getActiveCache());
		}

		return self::$cache;
	}

	/**
	 * @param string $key
	 * @return \Cache
	 */
	public static function getCacheForKey($key) {
		switch ($key) {
			case 'apc':
				return new APCCache();
			case 'apcu':
				return new APCUCache();
			case 'file':
				return new FileCache();
			default:
				return new NullCache();
		}
	}

}
