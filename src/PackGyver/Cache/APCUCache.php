<?php

/**
 * PackGyver - APC Cache
 */
class APCUCache extends Cache {

	/**
	 * {@inheritdoc}
	 */
	public function get($key) {
		return apcu_fetch($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function set($key, $value, $ttl = null) {
		return apcu_store($key, $value, $ttl);
	}

	/**
	 * {@inheritdoc}
	 */
	public function exists($key) {
		return apcu_exists($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function delete($key) {
		return apcu_delete($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function flush() {
		apcu_clear_cache('user');
		apcu_clear_cache();
	}

}
