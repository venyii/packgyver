<?php

/**
 * PackGyver - NULL Cache
 */
class NullCache extends Cache {

	/**
	 * {@inheritdoc}
	 */
	public function delete($key) {}

	/**
	 * {@inheritdoc}
	 */
	public function get($key) {}

	/**
	 * {@inheritdoc}
	 */
	public function exists($key) {
		return false;
	}

	/**
	 * {@inheritdoc}
	 */
	public function set($key, $value, $ttl = null) {}

	/**
	 * {@inheritdoc}
	 */
	public function flush() {}

}
