<?php

define('DS', DIRECTORY_SEPARATOR);

define('ROOT_PATH', dirname(__DIR__) . DS);
define('APP_PATH', ROOT_PATH . 'app' . DS);
define('SRC_PATH', ROOT_PATH . 'src' . DS);
define('DOC_ROOT', ROOT_PATH . 'web' . DS);

require_once __DIR__ . '/../vendor/autoload.php';
require_once APP_PATH . 'config.inc.php';

set_exception_handler('PackGyverException::logException');

if (PG::isDevMode()) {
	ini_set('display_errors', true);
	error_reporting(E_ALL & ~E_NOTICE);
} else {
	error_reporting(0);
}

AuthenticationManager::registerProvider(new AuthenticationProviderBasic());
AuthenticationManager::registerProvider(new AuthenticationProviderGoogleOAuth());
