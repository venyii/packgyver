<?php

define('API_KEY', '334dd8e86c071e9973e881610c61c81638a5e7ff');
define('API_SECRET', '9c4096737fc88652ec38fd577e26bdbb');

define('USER_AGENT', 'PackGyverApi/0.1');

require_once 'OAuth.php';

//call this file
$endpoint = 'http://pg/api/matches/changeResult';

$postParams = array(
    'matchId' => 2335,
    'team' => 'yellow',
    'type' => '+'
);

//use oauth lib to sign request
$signature = new OAuthSignatureMethod_HMAC_SHA1();
$consumer = new OAuthConsumer(API_KEY, API_SECRET);

$request = OAuthRequest::from_consumer_and_token($consumer, null, "POST", $endpoint);
$request->sign_request($signature, $consumer, null);

//get data using signed url
$ch = curl_init($request->to_url());
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);

curl_exec($ch);
curl_close($ch);
